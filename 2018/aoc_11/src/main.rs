const SN: i64 = 7400;
const SQ_SIZE: usize = 300;

struct Cell {
    x: i64,
    y: i64
}

impl Cell {
    pub fn power_level(&self, sn: i64) -> i64 {
        let rack_id = self.x + 10;

        // Parsing the string is easier than doing the math
        let mut prod_str = (((rack_id * self.y) + sn) * rack_id).to_string();
        let hund =  {
            if prod_str.pop().is_none() ||
               prod_str.pop().is_none() {
                0
            } else if let Some(x) = prod_str.pop() {
                i64::from(x.to_digit(10).unwrap())
            } else {
                0
            }
        };

        hund - 5
    }
}

fn main() {
    // Use the given examples to test our alorithm
    assert_eq!(Cell {x: 3, y: 5}.power_level(8), 4);
    assert_eq!(Cell {x: 122, y: 79}.power_level(57i64), -5);
    assert_eq!(Cell {x: 217, y: 196}.power_level(39i64), 0);
    assert_eq!(Cell {x: 101, y: 153}.power_level(71i64), 4);

    // Part 1
    get_most_powerful_cell(SN, 3, 3);

    // Part 2
    get_most_powerful_cell(SN, 1, 300);
}

fn get_most_powerful_cell(sn: i64, min_sub_size: usize, max_sub_size: usize) -> (Cell, i64) {
    let mut fuel_cell_levels = [[0; SQ_SIZE + 1]; SQ_SIZE + 1];

    // Fill out the grid
    for x in 1..=SQ_SIZE {
        for y in 1..=SQ_SIZE {
            fuel_cell_levels[x][y]= Cell {x: x as i64, y: y as i64}.power_level(sn);
        }
    }

    // Find the 3x3 grid with the largest combined fuel cell power level
    let mut largest_3x3: (Cell, i64) = (Cell {x: 0, y: 0}, 0);
    let mut largest_sub_sq_size = 0;
    for sub_sq_size in min_sub_size..=max_sub_size {
        for x in 1..(SQ_SIZE - (sub_sq_size - 2)) {
            for y in 1..(SQ_SIZE - (sub_sq_size - 2)) {
                let mut sum = 0;
                for sub_x in 0..sub_sq_size {
                    for sub_y in 0..sub_sq_size {
                        sum += fuel_cell_levels[x + sub_x][y + sub_y];
                    }
                }

                if sum > largest_3x3.1 {
                    largest_3x3 = (Cell {x: x as i64, y: y as i64}, sum);
                    largest_sub_sq_size = sub_sq_size;
                }
            }
        }
    }

    println!("Highest fuel cell density is in {}x{} sub-grid @ {},{}: {}",
             largest_sub_sq_size, largest_sub_sq_size, largest_3x3.0.x, largest_3x3.0.y, largest_3x3.1);

    largest_3x3
}

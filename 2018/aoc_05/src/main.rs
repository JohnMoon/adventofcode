use std::io::{BufReader, BufRead};
use std::fs::File;

const INPUT: &str = "input.txt";
const ALPHABET: &str = "abcdefghijklmnopqrstuvwxyz";

fn units_react(a: char, b: char) -> bool {
    a.eq_ignore_ascii_case(&b) &&
    ((a.is_ascii_uppercase() && b.is_ascii_lowercase()) ||
    (b.is_ascii_uppercase() && a.is_ascii_lowercase()))
}

fn reaction(polymer_chain: &[char], volatile_char: Option<char>, recursion: u32) -> Vec<char> {
    let mut new_chain = Vec::new();
    let mut unit_pairs = polymer_chain.windows(2).peekable();
    let mut reaction_complete = true;
    let mut unit_pair = unit_pairs.next().unwrap();

    loop {
        // Cover the case of a volatile character which is always removed
        if let Some(v_char) = volatile_char {
            if unit_pair[0].eq_ignore_ascii_case(&v_char) {
                if unit_pairs.peek().is_none() {
                    break;
                }
                else {
                    unit_pair = unit_pairs.next().unwrap();
                    continue;
                }
            }
        }

        // If the units react, slide the window and don't add to new chain
        if units_react(unit_pair[0], unit_pair[1]) {
            reaction_complete = false;

            if unit_pairs.peek().is_none() {
                break;
            } else {
                unit_pair = unit_pairs.next().unwrap();
            }

            if unit_pairs.peek().is_none() {
                new_chain.push(unit_pair[1]);
                break;
            } else {
                unit_pair = unit_pairs.next().unwrap(); 
            }
        }

        // If the units don't react, add to the new chain
        else {
            new_chain.push(unit_pair[0]);

            // If we're on the last iteration, push both units
            if unit_pairs.peek().is_none() {
                new_chain.push(unit_pair[1]);
                break;
            } else {
                unit_pair = unit_pairs.next().unwrap();
            }
        }
    }

    // If there were no reactive polymers, return the new chain
    if reaction_complete {
        new_chain

    // Otherwise, recurse with the new chain
    } else {
        reaction(&new_chain, volatile_char, recursion + 1)
    }
}

fn main() {
    let input_f = File::open(INPUT)
        .expect("error - input not found");
    let input = BufReader::new(&input_f);

    // There's just one really long line of input
    let polymer_chain = input.lines()
        .next().unwrap().unwrap()
        .chars().collect::<Vec<char>>();

    println!("Before reaction, the polymer chain is {} units long.",
             polymer_chain.len());

    let post_reaction_chain = reaction(&polymer_chain, None, 0);

    println!("After reaction with no volatile char, the polymer chain is {} units long.",
             post_reaction_chain.len());

    let mut shortest_polymer_len = polymer_chain.len();
    for letter in ALPHABET.chars() {
        let post_reaction_chain = reaction(&polymer_chain, Some(letter), 0);

        println!("After reaction with volatile char {}, the polymer chain is {} units long.",
                 letter.to_uppercase(),
                 post_reaction_chain.len());

        if post_reaction_chain.len() < shortest_polymer_len {
            shortest_polymer_len = post_reaction_chain.len();
        }
    }

    println!("The shortest polymer is {} units long.", shortest_polymer_len);
}

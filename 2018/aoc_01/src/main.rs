use std::io::{BufReader,BufRead};
use std::fs::File;

const INPUT: &str = "input.txt";

fn main() {
    let mut sum = 0i64;
    let mut exit_loop = false;
    let mut first_loop = true;
    let mut seen_freqs = Vec::new();
    seen_freqs.push(0i64);

    /* Read in the numbers from the file, line by line */
    while !exit_loop {
        let input_f = File::open(INPUT)
            .expect("error - input not found");
        let input = BufReader::new(&input_f);
        for num_str in input.lines() {
            sum += num_str.unwrap().parse::<i64>().unwrap();
            if seen_freqs.contains(&sum) {
                exit_loop = true;
                break;
            } else {
                seen_freqs.push(sum);
            }
        }
        if first_loop {
            println!("Frequency after first processed list: {}", sum);
            first_loop = false;
        }
    }
    println!("First frequency seen twice: {}", sum);
}

use std::io::{BufReader,BufRead};
use std::fs::File;
//use std::cmp::Ordering;

//const INPUT: &str = "input.txt";
const INPUT: &str = "example.txt";

const MASK: u128 = 0x1F; // 5 bits

#[derive(Copy, Clone, Debug)]
struct Note {
    note: u8,
    result: bool
}

impl PartialEq for Note {
    fn eq(&self, other: &Self) -> bool {
        (self.note == other.note)
    }
}

impl Eq for Note { }

impl Note {
    fn from_string_res(note: &str) -> Note {
        let mut note = String::from(note);
        let result = note.pop().unwrap() == '#';
        note.pop(); note.pop(); note.pop(); note.pop();
        let rr = if note.pop().unwrap() == '#' { 1 } else { 0 };
        let r = if note.pop().unwrap() == '#' { 1 } else { 0 };
        let c = if note.pop().unwrap() == '#' { 1 } else { 0 };
        let l = if note.pop().unwrap() == '#' { 1 } else { 0 };
        let ll = if note.pop().unwrap() == '#' { 1 } else { 0 };
    
        let note: u8 = 0 | rr | r << 1 | c << 2 | l << 3 | ll << 4;
        Note {note, result}
    }
}

fn pass_generations<'a>(state: u128, notes: &Vec<Note>, num_generations: u64) -> u128 {
    // Create the new state vector and just copy the first two plants
    let mut start = state;
    let mut n = num_generations;
    let mut new_state: u128;

    loop {
        println!("{:0128b}", start);
        // Take the first two bits from the start
        new_state = 0 | (start & 0b11);

        // Slide over the state string in 5-bit windows
        for i in 0..128 {
            let note_in_state = ((start >> i) & MASK) as u8;

            // For every window, check to see if the pattern is noted
            // and push the result onto the new state vector
            let mut no_match = true;
            for check in notes {
                if check.note == note_in_state {
                    no_match = false;
                    if check.result {
                        new_state |= 1 << i;
                    } else {
                        new_state &= !(1 << i);
                    }
                    break;
                }
            }
            if no_match {
                new_state &= !(1 << i);
            }
        }

        // Copy the last two bits out of the start
        new_state |= (start & 0b11) << 126;

        n -= 1;

        if n == 0 {
            return new_state;
        } else {
            start = new_state;
        }
    }
}

fn main() {
    let input_f = File::open(INPUT)
        .expect("error - input not found");
    let input = BufReader::new(&input_f);

    // First line is different than the rest
    let mut lines = input.lines().map(Result::unwrap);
    let first_line = lines.next().unwrap();
    let mut tok_iter = first_line.split(':');
    tok_iter.next();

    let init_state_str = tok_iter.next().unwrap().trim();

    // The state will be represented as a 128-bit unsigned int - each bit is a pot
    let mut init_state: u128 = 0;
    for (i, pot) in init_state_str.chars().enumerate() {
        if pot == '#' {
            init_state |= 1 << i;
        }
    }

    println!("String: {}", init_state_str);
    println!("   Int: {:b}", init_state);

    let mut notes = Vec::new();
    for line in lines.skip(1) {
        notes.push(Note::from_string_res(&line));
    }

    // Part 1
    println!("Sum of pot values after 20 generations: {}", get_sum_after_n_gen(init_state, notes.clone(), 20));

    // Part 2
    //println!("Sum of pot values after 50 bn generations: {}", get_sum_after_n_gen(init_state, notes, 50_000_000_000));
}

fn get_sum_after_n_gen(init_state: u128, notes: Vec<Note>, num_gen: u64) -> i64 {
    let state_after_n = pass_generations(init_state, &notes, num_gen);

    println!("State after {} generations: {}", num_gen, state_after_n);
    let mut sum: i64 = 0;
    for i in 0..128 {
        if ((state_after_n >> i) & 0b1) == 1 {
            sum += i as i64;
        }
    }
    
    sum
}

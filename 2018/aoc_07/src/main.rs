use std::io::{BufReader, BufRead};
use std::fs::File;
use std::collections::HashMap;

const INPUT: &str = "input.txt";
const ALPHABET: &str = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

#[derive(Clone, Debug)]
struct Step {
    deps: Vec<char>,
    duration: u32,
    finished: bool
}
fn main() {
    let input_f = File::open(INPUT)
        .expect("error - input not found");
    let input = BufReader::new(&input_f);

    // Create a hash map of steps
    let mut steps = HashMap::new();

    for line in input.lines().map(Result::unwrap) {
        let mut tokens = line.split_whitespace();

        // The dep and the base letter for the each step is at index 1 and 5 of each line
        let dep_id = tokens.nth(1).unwrap().chars().next().unwrap();
        let base_id = tokens.nth(5).unwrap().chars().next().unwrap();

        // Add the step to the hash map if need be and add the dependency
        let step = steps.entry(base_id).or_insert( Step { 
            deps: Vec::new(),
            duration: get_step_duration(base_id),
            finished: false
        });
        step.deps.push(dep_id);
    }

    // Add base letters that weren't included as base in the file
    for letter in ALPHABET.chars() {
        if let None = steps.get_mut(&letter) {
            steps.insert(letter, Step {
                deps: Vec::new(),
                duration: get_step_duration(letter),
                finished: false
            });
        }
    }

    part_1(steps.clone());
    part_2(steps.clone(), 1);
    part_2(steps.clone(), 5);
}

fn part_1(mut steps: HashMap<char, Step>) {
    print!("The step order is: ");
    let mut done = false;
    while !done {
        done = true;
        // Each iteration goes over the whole alphabet in order
        for letter in ALPHABET.chars() {

            // If there's a step associated with the letter
            if let Some(step) = steps.get_mut(&letter) {

                // If the step has no remaining dependencies, we can complete it!
                if step.deps.is_empty() && !step.finished {
                    complete_step(letter, &mut steps);
                    print!("{}", letter);
                    done = false;
                    break;
                }
            }
        }
    }
    println!();
}

fn part_2(mut steps: HashMap<char, Step>, num_elves_available: usize) {
    let mut secs = 0;
    let mut done = false;
    let mut in_progress = Vec::new();
    
    while !done {
        done = true;
        // Each iteration goes over the whole alphabet in order
        for letter in ALPHABET.chars() {

            if let Some(step) = steps.get_mut(&letter) {
                if step.deps.is_empty() && !step.finished {
                    done = false;

                    // Maintain a list of letters in progress
                    if in_progress.len() < num_elves_available && !in_progress.contains(&letter) {
                        in_progress.push(letter);
                    }
                }
            }
        }

        // Do a sec of work on each letter in progress, retain those
        // that are not done being worked on
        if !in_progress.is_empty() {
            in_progress.retain(|c| {
               !do_a_sec_of_work(*c, &mut steps)
            });
            secs += 1;
        }
    }

    println!("Took {} seconds to complete the work with {} elves", secs, num_elves_available);
}

fn complete_step(letter: char, map: &mut HashMap<char, Step>) {
    // Remove this letter from every steps dependency chain
    for step in map.values_mut() {
        step.deps.retain(|c| *c != letter);
    }

    // Mark the letter as finished
    map.get_mut(&letter).unwrap().finished = true;
}

fn do_a_sec_of_work(letter: char, map: &mut HashMap<char, Step>) -> bool { 
    map.get_mut(&letter).unwrap().duration -= 1;
    if map.get_mut(&letter).unwrap().duration == 0 {
        complete_step(letter, map);
        true
    } else {
        false
    }
}

fn get_step_duration(letter: char) -> u32 {
    ((letter as u32) - ('A' as u32)) + 61
}

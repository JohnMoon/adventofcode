use std::time::Instant;

// Numbers from input.txt
const NUM_PLAYERS: u32 = 473;
const HIGH_MARBLE: u32 = 70_904;
const HIGH_MARBLE_100: u32 = 70_904 * 100;

#[derive(Copy, Clone)]
struct Player {
    id: u32,
    score: u32
}

#[derive(Clone)]
struct Circle {
    marbles: Vec<u32>,
    current_marble: usize
}

impl Circle {
    fn move_left(&mut self, shift: usize) {
        if !self.marbles.is_empty() {
            self.current_marble = {
                if self.current_marble >= shift  {
                    self.current_marble - shift
                } else {
                    self.marbles.len() - ((shift - self.current_marble) % self.marbles.len())
                }
            };
        }
    }

    fn move_right(&mut self, shift: usize) {
        if !self.marbles.is_empty() {
            self.current_marble = (self.current_marble + shift) % self.marbles.len();
        }
    }

    fn place_marble(&mut self, marble_val: u32) {
        self.move_right(2);
        if self.current_marble == 0 {
            self.marbles.push(marble_val);
            self.current_marble = self.marbles.len() - 1;
        } else {
            self.marbles.insert(self.current_marble, marble_val);
        }

    }

    fn remove_marble(&mut self) -> u32 {
        self.move_left(7);
        self.marbles.remove(self.current_marble)
    }
}

impl std::fmt::Display for Circle {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
	for (i, marble) in self.marbles.iter().enumerate() {
            if i == self.current_marble {
                print!("({})", marble)
            } else {
                print!(" {} ", marble)
            }
	}
        write!(f, "")
    }
}

fn main() {
    // We're given many test cases. Test before running our input.
    assert_eq!(get_high_score(9, 25), 32);
    assert_eq!(get_high_score(10, 1_618), 8_317);
    assert_eq!(get_high_score(13, 7_999), 146_373);
    assert_eq!(get_high_score(17, 1_104), 2_764);
    assert_eq!(get_high_score(21, 6_111), 54_718);
    assert_eq!(get_high_score(30, 5_807), 37_305);

    // Part 1
    get_high_score(NUM_PLAYERS, HIGH_MARBLE);

    // Part 2
    get_high_score(NUM_PLAYERS, HIGH_MARBLE_100);
}

fn get_high_score(num_players: u32, high_marble: u32) -> u32 {
    let mut circle = Circle {
        marbles: Vec::with_capacity(high_marble as usize),
        current_marble: 0
    };

    let mut players = Vec::with_capacity(num_players as usize);
    for id in 0..num_players {
        players.push(Player { id, score: 0 });
    }

    circle.place_marble(0);

    let mut now = Instant::now();
    for next_marble in 1..=high_marble {
        let player = &mut players[(next_marble % num_players) as usize];
        if next_marble % 23 != 0 {
            circle.place_marble(next_marble);
        } else {
            player.score += next_marble + circle.remove_marble();
        }

        // Print the occasional status message
        if next_marble % 65_536 == 0 {
            let dur = now.elapsed();
            println!("{}s {:3}ms to place marble {}", dur.as_secs(), dur.subsec_millis(), next_marble);
            now = Instant::now();
        }
    }

    let mut winner = Player { id: 0, score: 0 };
    for player in players {
        if player.score > winner.score {
            winner = player;
        }
    }
    println!("High score with {} players and a high marble value of {} is Player {} with {} points",
             num_players, high_marble, winner.id, winner.score);

    winner.score
}

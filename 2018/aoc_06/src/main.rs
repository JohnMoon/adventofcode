use std::io::{BufReader, BufRead};
use std::fs::File;

const INPUT: &str = "input.txt";

#[derive(Clone, Copy, Debug)]
struct Bounds {
    top: i32,
    bottom: i32,
    left: i32,
    right: i32
}

#[derive(Clone, Copy, PartialEq, Debug)]
struct Point {
    x: i32,
    y: i32
}

#[derive(Clone, Copy, PartialEq, Debug)]
struct Location {
    id: u32,
    point: Point,
    md: Option<u32>
}

fn main() {
    let input_f = File::open(INPUT)
        .expect("error - input not found");
    let input = BufReader::new(&input_f); 

    // This will help figure out which locations have an infinitely large MD
    let mut bounds = Bounds {
        top: 1000,
        bottom: 0,
        left: 1000,
        right: 0,
    };

    let mut locs = Vec::new();
    for (i, line) in input.lines().map(Result::unwrap).enumerate() {
        let coordinates: Vec<&str> = line.split(", ").collect();
        let x = coordinates[0].parse::<i32>().unwrap();
        let y = coordinates[1].parse::<i32>().unwrap();

        if x < bounds.left {
            bounds.left = x - 1;
        } else if x > bounds.right {
            bounds.right = x + 1;
        }

        if y < bounds.top {
            bounds.top = y - 1;
        } else if y > bounds.bottom {
            bounds.bottom = y + 1;
        }

        locs.push(Location {
            id: (i + 1) as u32,
            point: Point {
                x,
                y
            },
            md: None
        });
    }

    // Generate a list of all the points we have to look at (observable universe)
    let mut ou = Vec::new();
    for x in bounds.left..bounds.right {
        for y in bounds.top..bounds.bottom {
            ou.push((Point {x, y}, 0u32));
        }
    }

    // For each point, find the closest location
    for mut point in ou.iter_mut() {
        find_closest_loc(&mut point, &locs);
    }

    // Count up the number of points closest to each location
    for loc in locs.iter_mut() {
        for point in &ou {
            if point.1 == loc.id {
                // If a location has a closest point on a
                // border, its MD is infinite
                if is_on_border(point.0, bounds) {
                    loc.md = None;
                    break;
                }

                if let Some(md) = loc.md {
                    loc.md = Some(md + 1);
                } else {
                    loc.md = Some(1);
                }
            }
        }
    }

    let mut max_md = 0;
    for loc in &locs {
        if let Some(md) = loc.md {
            if md > max_md {
                max_md = md;
            }
        }
    }

    println!("Maximum non-infinte Manhattan distance is {}", max_md);


    // For part two, find the total distance to all locations
    let mut total_distance_vec: Vec<(Point, u32)> = Vec::new();
    for point in ou {
        let total_distance = find_total_distance(point.0, &locs);
        total_distance_vec.push((point.0, total_distance));
    }

    total_distance_vec.retain(|t| { t.1 < 10_000 });

    println!("Number of points with total MD < 10,000: {}", total_distance_vec.len());
}

fn is_on_border(point: Point, bounds: Bounds) -> bool {
    point.x == bounds.left ||
    point.x == bounds.right ||
    point.y == bounds.top ||
    point.y == bounds.bottom 
}

fn find_closest_loc(point: &mut (Point, u32), locs: &[Location]) {
    let mut closest_md = 10000;
    for loc in locs {
        let md = (loc.point.x - point.0.x).abs() + (loc.point.y - point.0.y).abs();
        if md < closest_md {
            point.1 = loc.id;
            closest_md = md;
        } else if md == closest_md {
            point.1 = 0; // Reset to null ID since equal distances don't count
        }
    }
}

fn find_total_distance(point: Point, locs: &[Location]) -> u32 {
    let mut ret: u32 = 0;
    for loc in locs {
        ret += ((loc.point.x - point.x).abs() + (loc.point.y - point.y).abs()) as u32;
    }
    ret
}

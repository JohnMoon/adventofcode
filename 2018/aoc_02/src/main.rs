use std::io::{BufReader,BufRead};
use std::collections::HashMap;
use std::fs::File;

const INPUT: &str = "input.txt";
const ALPHABET: &str = "abcdefghijklmnopqrstuvwxyz";

fn part_1() {
    let input_f = File::open(INPUT)
        .expect("error - input not found");
    let input = BufReader::new(&input_f);

    let mut num_2_ids = 0;
    let mut num_3_ids = 0;

    for id in input.lines() {
        let mut map = HashMap::new();
        for letter in id.unwrap().chars() {
            *map.entry(letter).or_insert(0) += 1;
        }

        let mut already_2 = false;
        let mut already_3 = false;
        for letter in ALPHABET.chars() {
            if let Some(num) = map.get(&letter) {
                if !already_2 && *num == 2 {
                    num_2_ids += 1;
                    already_2 = true;
                } else if !already_3 && *num == 3 {
                    num_3_ids += 1;
                    already_3 = true;
                }
            }
            if already_2 && already_3 {
                break;
            }
        }
    }

    println!("IDs with 2 letters: {}", num_2_ids);
    println!("IDs with 3 letters: {}", num_3_ids);
    println!("ID checksum: {}", num_2_ids * num_3_ids);
}

fn diff_by_one(id1: &str, id2: &str) -> bool {
    let mut letters_diff = 0;
    let mut letter1 = id1.chars();
    let mut letter2 = id2.chars();
    loop {
        match (letter1.next(), letter2.next()) {
            (Some(l1), Some(l2)) => {
                if l1 != l2 {
                    letters_diff += 1;
                    if letters_diff > 1 {
                        return false;
                    }
                }
            },
            _ => return letters_diff == 1,
        }
    }
}

fn part_2() {
    let input_f = File::open(INPUT)
        .expect("error - input not found");
    let input = BufReader::new(&input_f);

    let id_list_1: Vec<String> = input.lines()
        .map(Result::unwrap).
        collect();

    let id_list_2 = id_list_1.clone();

    for id1 in &id_list_1 {
        for id2 in &id_list_2 {
            if diff_by_one(&id1, &id2) {
                println!("\nBox ID {} and {} differ by only character", id1, id2);
                return;
            }
        }
    }
}

fn main() {
    part_1();
    part_2();
}

use std::io::{BufReader, BufRead, Write};
use std::fs::File;
use std::cmp::Ordering;

const INPUT: &str = "input.txt";
const DISTANCE: i64 = 2;
const START_AT: i64 = 10_600;
const END_AT: i64 = 10_650;

#[derive(Clone, Copy, Debug, PartialEq, PartialOrd, Eq)]
struct Light {
    num: u32,
    pos: (i64, i64),
    vel: (i64, i64)
}

impl Ord for Light {
    fn cmp(&self, other: &Light) -> Ordering {
        self.num.cmp(&other.num)
    }
}

impl Light {
    fn tick_sec(&mut self) {
        self.pos.0 += self.vel.0;
        self.pos.1 += self.vel.1;
    }
}

fn main() {
//    let input_f = File::open(EXAMPLE)
    let input_f = File::open(INPUT)
        .expect("error - input not found");
    let input = BufReader::new(&input_f);

    let mut points: Vec<Light> = Vec::new();

    // All of the numbers are in certain char positions
    for (i, line) in input.lines().map(Result::unwrap).enumerate() {
        let pos_x = line[10..16].trim().parse::<i64>().unwrap();
        let pos_y = line[18..24].trim().parse::<i64>().unwrap();
        let vel_x = line[36..38].trim().parse::<i64>().unwrap();
        let vel_y = line[40..42].trim().parse::<i64>().unwrap();

        points.push(Light {
            num: i as u32,
            pos: (pos_x, pos_y),
            vel: (vel_x, vel_y)
        });
    }

    for _ in 0..START_AT {
        for point in &mut points {
            point.tick_sec();
        }
    }

    let mut last_uniq: Vec<Vec<Light>> = Vec::new();
    let mut first_iter = true;
    for i in START_AT..END_AT {
        let uniq = get_unique_blocks(&points);
        if first_iter {
            first_iter = false;
            last_uniq = uniq.clone();
        }

        // We have a local minimum
        if uniq.len() > last_uniq.len() {
            let mut output_f = File::create(format!("output_{}.csv", i))
                .expect("could not create output file");

            for block in last_uniq {
                for point in block {
                    writeln!(output_f, "{},{}", point.pos.0, point.pos.1);
                }
            }
        }

        last_uniq = uniq.clone();
        println!("{}: {} unique blocks", i, uniq.len());

        // Let one second pass
        for point in &mut points {
            point.tick_sec();
        }
    }
}

fn get_unique_blocks(points: &[Light]) -> Vec<Vec<Light>> {
    let mut blocks: Vec<Vec<Light>> = Vec::new();
    let mut in_a_block: Vec<u32> = Vec::new();
    for point1 in points {
        let mut block: Vec<Light> = Vec::new();
        // If this point is already in a block somewhere, skip it
        if in_a_block.contains(&point1.num) {
            continue;

        // Otherwise, we create a new block
        } else {
            block.push(*point1);
            in_a_block.push(point1.num);
        }

        // We test every other point against this block
        let mut done = false;
        while !done {
            done = true;
            for point2 in points {
                // If it's the same point as the base, or another block contains it, skip
                if in_a_block.contains(&point2.num) {
                    continue;
                }

                let mut add_point = false;

                // If this point is connected to any point already in the block, add it to the block
                for light in &block {
                    let x_dif = (light.pos.0 - point2.pos.0).abs();
                    let y_dif = (light.pos.1 - point2.pos.1).abs();
                    if x_dif < DISTANCE &&  y_dif < DISTANCE {
                        add_point = true;
                        done = false;
                        in_a_block.push(point2.num);
                        break;
                    }
                }

                if add_point {
                    block.push(*point2);
                }
            }
        }
        blocks.push(block);
    }

    blocks
}

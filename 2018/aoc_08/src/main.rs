use std::io::{BufReader,BufRead};
use std::fs::File;

const INPUT: &str = "input.txt";

struct Node {
    children: Vec<Node>,
    meta: Vec<u32>
}

impl Node {
    fn get_metadata_sum(&self) -> u32 {
        let sum = self.children.iter()
            .fold(0, |acc, x| acc + x.get_metadata_sum());
        sum + self.meta.iter().sum::<u32>()
    }

    fn get_value(&self) -> u32 {
        let mut sum = 0;

        // If no children, add the metadata to the sum
        if self.children.is_empty() {
            sum += self.meta.iter().sum::<u32>();

        // Otherwise, add the value of the children indexed by the metadata
        } else {
            for num in &self.meta {
                if *num == 0 {
                    continue;
                }

                if let Some(child) = self.children.get(*num as usize - 1) {
                    sum += child.get_value();
                }
            }
        }

        sum
    }
}

fn main() {
    let input_f = File::open(INPUT)
        .expect("error - input not found");
    let input = BufReader::new(&input_f);

    let mut input_n = Vec::new();
    for line in input.lines() {
        for num in line.unwrap().split_whitespace() {
            input_n.push(num.parse::<u32>().unwrap());
        }
    }

    // Generate a tree from the input
    let tree = generate_tree(&input_n).0;

    // Part 1
    println!("Sum of metadata is {}", tree.get_metadata_sum());

    // Part 2
    println!("Value of the tree is {}", tree.get_value());
}

// Recursive function that generates a tree based on the input
fn generate_tree(input: &[u32]) -> (Node, usize) {
    let num_children = &input[0];
    let num_meta = &input[1];

    let mut index = 2;
    let mut new_node = Node {
        children: Vec::with_capacity(*num_children as usize),
        meta: Vec::with_capacity(*num_meta as usize)
    };

    for _ in 0..*num_children {
        let (child, indicies) = generate_tree(&input.split_at(index).1);
        index += indicies as usize;
        new_node.children.push(child);
    }

    // Add all metadata to the sum
    for i in 0..*num_meta {
        new_node.meta.push(input[index + (i as usize)]);
    }

    (new_node, index + (*num_meta as usize))
}

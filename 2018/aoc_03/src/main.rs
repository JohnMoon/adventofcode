use std::io::{BufReader, BufRead};
use std::fs::File;

const INPUT: &str = "input.txt";
const SQUARE_SIZE: i32 = 1_000;

#[derive(Debug, Copy, Clone)]
struct Claim {
    id: u32, // Claim ID
    origin: Point, // Origin (top left of rectangle)
    width: u32, // Width (inches)
    height: u32, // Height (inches)
    overlap: bool // if it overlaps
}

#[derive(Debug, Copy, Clone)]
struct Point {
    x: i32,
    y: i32,
}

impl Claim {
    fn contains(&self, point: Point) -> bool {
        point.x >= self.origin.x &&
        point.x < self.origin.x + (self.width as i32) &&
        point.y >= self.origin.y &&
        point.y < self.origin.y + (self.height as i32)
    }
}

fn main() {
    let input_f = File::open(INPUT)
        .expect("error - input not found");
    let input = BufReader::new(&input_f);

    // Create a vector of Claims
    let mut claims = Vec::new();
    for line in input.lines().map(Result::unwrap) {
        // Example line: #1233 @ 923,156: 16x21
        // Trim the '#', then split on whitespace
        let mut elems = line
            .trim_start_matches('#')
            .split_whitespace();

        let id = elems.next().unwrap().parse::<u32>().unwrap();
        elems.next(); // Skip the '@'

        let mut xy = elems.next().unwrap().split(',');
        let x = xy.next().unwrap().parse::<i32>().unwrap();
        let y = xy.next().unwrap().trim_end_matches(':').parse::<i32>().unwrap();
        let origin = Point {x, y};

        let mut wh = elems.next().unwrap().split('x');
        let width = wh.next().unwrap().parse::<u32>().unwrap();
        let height = wh.next().unwrap().parse::<u32>().unwrap();

        // Add a new claim to our vector
        claims.push(Claim {id, origin, width, height, overlap: false});
    }

    let mut sum_overlapping_inches = 0;
    // Iterate over every point in the fabric
    for x in 0..SQUARE_SIZE {
        for y in 0..SQUARE_SIZE {
            let point = Point {x, y};

            // Check if more than one claim contains the point
            let mut point_contained = false;
            let mut overlap_counted = false;
            let mut save_index = 0;
            for (i, claim) in claims.iter_mut().enumerate() {
                if claim.contains(point) {
                    if !point_contained {
                        point_contained = true;
                        save_index = i;
                    } else {
                        if !overlap_counted {
                            sum_overlapping_inches += 1;
                            overlap_counted = true;
                        }
                        claim.overlap = true;
                    }
                }
            }
            if overlap_counted {
                claims[save_index].overlap = true;
            }
        }
    }

    println!("Total overlapping area: {}", sum_overlapping_inches);
    for claim in claims.iter().filter(|x| !x.overlap) {
        println!("Claim with no overlap: {}", claim.id);
    }
}

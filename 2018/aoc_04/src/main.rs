use std::io::{BufReader, BufRead};
use std::fs::File;
use std::collections::HashMap;

const INPUT: &str = "input.txt";

#[derive(PartialOrd, PartialEq, Ord, Eq, Debug, Copy, Clone)]
struct Time {
    year: i32,
    month: u32,
    day: u32,
    hour: u32,
    minute: u32,
}

impl Time {
    fn sub(&self, y: &Time) -> Time {
        let x = self;
        Time {
            year: x.year - y.year,
            month: sub_mod(x.month, y.month, 12),
            day: sub_mod(x.day, y.day, 7),
            hour: sub_mod(x.hour, y.hour, 24),
            minute: sub_mod(x.minute, y.minute, 60)
        }
    }

    fn as_minutes(&self) -> u32 {
        self.minute + (self.hour * 60) + (self.day * 24 * 60)
    }
}

#[derive(Clone, Copy)]
struct Guard {
    id: u32,
    total_min_asleep: u32,
    minutes_caught_sleeping: [u32; 60]
}

fn sub_mod(x: u32, y: u32, modulo: u32) -> u32 {
    if x >= y {
        x - y
    } else {
        modulo - (y - x)
    }
}

fn main() {

    let input_f = File::open(INPUT)
        .expect("error - input not found");
    let input = BufReader::new(&input_f);

    let mut times: Vec<(Time, String)> = Vec::new();
    
    for line in input.lines() {
        // The date string is in a known format, so just split on chars
        let line = line.unwrap();
        let year = line[1..5].parse::<i32>().unwrap();
        let month = line[6..8].parse::<u32>().unwrap();
        let day = line[9..11].parse::<u32>().unwrap();
        let hour = line[12..14].parse::<u32>().unwrap();
        let minute = line[15..17].parse::<u32>().unwrap();

        let time_and_string = (Time {year, month, day, hour, minute}, line);

        // Create a sorted vector
        let pos = times
            .binary_search(&time_and_string)
            .unwrap_or_else(|e| e);
        times.insert(pos, time_and_string);
    }

    let mut guards: HashMap<u32, Guard> = HashMap::new();
    let mut entry_iter = times.iter_mut();
    let mut entry = entry_iter.next().unwrap();
    loop {
        let mut new_guard = Guard {id: 0, total_min_asleep: 0, minutes_caught_sleeping: [0; 60]};
        let mut strings = entry.1.split_whitespace();
        let time_asleep;
        let time_awake;
        let det = strings.nth(3).unwrap();

        // New guard on duty
        if det.starts_with('#') {
            new_guard.id = det.chars().skip(1).collect::<String>().parse::<u32>().unwrap();

            if guards.contains_key(&new_guard.id) {
                new_guard = guards.remove(&new_guard.id).unwrap();
            }

            entry = entry_iter.next().unwrap();
            let action = entry.1.split_whitespace().nth(3).unwrap();

            // Guard fell asleep
            if action == "asleep" {

                time_asleep = entry.0;
                entry = entry_iter.next().unwrap();
                let action = entry.1.split_whitespace().nth(3).unwrap();

                // Guard woke up
                if action == "up" {
                    time_awake = entry.0;
                    new_guard.total_min_asleep += time_awake.sub(&time_asleep).as_minutes();
                    // Simple case, no wrap at 60 minutes
                    if time_awake.minute > time_asleep.minute {
                        for minute in time_asleep.minute..time_awake.minute {
                            new_guard.minutes_caught_sleeping[minute as usize] += 1;
                        }
                    } else {
                        for minute in time_asleep.minute..60 {
                            new_guard.minutes_caught_sleeping[minute as usize] += 1;
                        }
                        for minute in 0..time_awake.minute {
                            new_guard.minutes_caught_sleeping[minute as usize] += 1;
                        }
                    }

                    guards.insert(new_guard.id, new_guard);
                } 
            }

            // Guard never went to sleep and new guard is on duty...
            else {
                continue;
            }
        }

        match entry_iter.next() {
            Some(x) => entry = x,
            None => break,
        }
    }

    let mut max = Guard {id: 0, total_min_asleep: 0, minutes_caught_sleeping: [0; 60]};
    for guard in &guards {
        if guard.1.total_min_asleep > max.total_min_asleep {
            max = *guard.1;
        }
    }

    // Max minute shows the guard who was asleep most often at the same minute
    let mut max_minute = (0, 0, 0);
    for guard in &guards {
        for i in 0..60 {
            if guard.1.minutes_caught_sleeping[i] > max_minute.2 {
                max_minute = (guard.1.id, i, guard.1.minutes_caught_sleeping[i]);
            }
        }
    }

    let mut specific_max_minute = (0, 0);
    for i in 0..60 {
        if max.minutes_caught_sleeping[i] > specific_max_minute.1 {
            specific_max_minute = (i, max.minutes_caught_sleeping[i]);
        }
    }

    println!("Part 1: Guard {} slept {} minutes total. Most often, they're asleep at minute {}",
             max.id, max.total_min_asleep, specific_max_minute.0);
    println!("The product of ID and minute is {}", max.id * max_minute.0 as u32);

    println!("\nPart 2: Guard {} slept the most at the same minute ({})",
             max_minute.0, max_minute.1);
    println!("The product of ID and minute is {}", max_minute.0 * max_minute.1 as u32);
}

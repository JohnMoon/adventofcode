const INPUT: &str = include_str!("input.txt");
const SAMPLE: &str = include_str!("sample.txt");

fn median(positions: &[i32]) -> i32 {
    let mut sorted_positions = positions.to_owned();
    sorted_positions.sort_unstable();

    let mid_index = sorted_positions.len() / 2;
    if sorted_positions.len() % 2 == 0 {
        (sorted_positions[mid_index - 1] + sorted_positions[mid_index]) / 2
    } else {
        sorted_positions[mid_index]
    }
}

fn mean(positions: &[i32]) -> i32 {
    let len = positions.len() as i32;
    (positions.iter().sum::<i32>() + len - 1) / len
}

fn get_fuel_for_alignment_p1(positions: &[i32]) -> i32 {
    let pos = median(positions);
    positions.iter().fold(0, |acc, x| {
        acc + (x - pos).abs()
    })
}

fn get_fuel_for_alignment_p2(positions: &[i32]) -> i32 {
    let pos = mean(positions);
    let mut last_fuel = 2_000_000_000;
    // Check the values _near_ the mean for the least fuel consumption
    for spread in 0..6 {
        for i in &[pos + spread, pos - spread] {
            let mut fuel = 0;
            for x in positions {
                let mut delta = (x - i).abs();
                while delta > 0 {
                    fuel += delta;
                    delta -= 1;
                }
            }
            if fuel < last_fuel {
                last_fuel = fuel;
            }
        }
    }
    last_fuel
}

fn main() {
    let sample_positions: Vec<i32> = SAMPLE.trim().split(',').map(|x| x.parse::<i32>().unwrap()).collect();
    assert_eq!(get_fuel_for_alignment_p1(&sample_positions), 37);
    assert_eq!(get_fuel_for_alignment_p2(&sample_positions), 168);

    let input_positions: Vec<i32> = INPUT.trim().split(',').map(|x| x.parse::<i32>().unwrap()).collect();
    println!("Fuel required for alignment (P1): {}", get_fuel_for_alignment_p1(&input_positions));
    println!("Fuel required for alignment (P2): {}", get_fuel_for_alignment_p2(&input_positions));
}
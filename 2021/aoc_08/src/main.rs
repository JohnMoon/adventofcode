use std::collections::HashMap;

const INPUT: &str = include_str!("input.txt");
const SAMPLE: &str = include_str!("sample.txt");

struct SignalMap {
    signals: Vec<String>,
    output: Vec<String>
}

impl SignalMap {
    fn from_line(input: &str) -> SignalMap {
        let split: Vec<&str> = input.trim().split(" | ").collect();
        SignalMap {
            signals: split[0].split_whitespace().map(|x| x.to_string()).collect(),
            output: split[1].split_whitespace().map(|x| x.to_string()).collect()
        }
    }

    fn count_unique_in_output(&self) -> usize {
        self.output.iter().filter(|x| {
            let len = x.len();
            vec![2, 3, 4, 7].contains(&len)
        })
        .count()
    }

    fn get_unscrambled_output(&self) -> u32 {
        let digits = self.get_digits();
        let mut output_vec = Vec::new();

        for digit_str in &self.output {
            let mut sorted_str = digit_str.chars().collect::<Vec<char>>();
            sorted_str.sort_by(|a, b| b.cmp(a));
            for (digit, s) in &digits {
                let mut sorted_s = s.chars().collect::<Vec<char>>();
                sorted_s.sort_by(|a, b| b.cmp(a));
                if sorted_str == sorted_s {
                    output_vec.push(*digit);
                }
            }
        }

        let out = format!("{}{}{}{}", output_vec[0], output_vec[1], output_vec[2], output_vec[3])
            .parse::<u32>().unwrap();
        out
    }

    fn get_digits(&self) -> HashMap<u8, String> {
        let mut known_digits: HashMap<u8, String> = HashMap::new();

        // Push all our trivial segments into the known map
        for s in &self.signals {
            match s.len() {
                2 => known_digits.insert(1, s.to_string()),
                4 => known_digits.insert(4, s.to_string()),
                3 => known_digits.insert(7, s.to_string()),
                7 => known_digits.insert(8, s.to_string()),
                _ => None
            };
            if known_digits.len() == 4 { break; }
        }

        // Work out which one is number 6 within the digits with 6 segments (0, 6, and 9)
        let six_segments: Vec<String> = self.signals.clone().into_iter().filter(|x| x.len() == 6).collect();
        let one = known_digits.get(&1).unwrap().clone();
        let four = known_digits.get(&4).unwrap().clone();
        for segment in &six_segments {
            // If this digit does not have each segment we find in 1, then it must be 6
            let mut found_six = false;
            for c in one.chars() {
                if ! segment.contains(c) {
                    known_digits.insert(6, segment.clone());
                    found_six = true;
                    break;
                }
            }
            if found_six { continue; }

            // At this point, it's either a 0 or a 9
            let in_common_with_four = segment.chars().filter(|c| {
                four.contains(*c)
            }).count();

            // If this digit has 3 in common with 4, it must be 0
            if in_common_with_four == 3 {
                known_digits.insert(0, segment.clone());
                continue;
            }

            // If this digit has 4 in common with 4, it must be 9
            if in_common_with_four == 4 {
                known_digits.insert(9, segment.clone());
                continue;
            }

            unreachable!();
        }

        let six = known_digits.get(&6).unwrap().clone();

        // Work out 2, 3, and 5
        let five_segments: Vec<String> = self.signals.clone().into_iter().filter(|x| x.len() == 5).collect();
        for i in 0..five_segments.len() {
            let mut common_segments = 0;
            for j in 0..five_segments.len() {
                if j != i {
                    common_segments += five_segments[i].chars().filter(|c| {
                        five_segments[j].contains(*c)
                    }).count();
                }
            }
            // If this digit has 4 in common with each of the other two, it must be 3
            if common_segments == 8 {
                known_digits.insert(3, five_segments[i].clone());
                continue;
            }

            // At this point, it's either a 2 or a 5
            let in_common_with_six = five_segments[i].chars().filter(|c| {
                six.contains(*c)
            }).count();

            // If this digit has 4 in common with 6, it must be 2
            if in_common_with_six == 4 {
                known_digits.insert(2, five_segments[i].clone());
                continue;
            }

            // If this digit has 5 in common with 6, it must be 5
            if in_common_with_six == 5 {
                known_digits.insert(5, five_segments[i].clone());
                continue;
            }

            unreachable!();
        }

        known_digits
    }
}

fn main() {
    let sample_notes: Vec<SignalMap> = SAMPLE.lines().map(|l| SignalMap::from_line(l)).collect();
    let sample_uniques = sample_notes.iter().fold(0, |acc, m| {
        acc + m.count_unique_in_output()
    });
    let sample_output_sum = sample_notes.iter().fold(0, |acc, m| {
        acc + m.get_unscrambled_output()
    });
    assert_eq!(sample_uniques, 26);
    assert_eq!(sample_output_sum, 61229);

    let input_notes: Vec<SignalMap> = INPUT.lines().map(|l| SignalMap::from_line(l)).collect();
    let input_uniques = input_notes.iter().fold(0, |acc, m| {
        acc + m.count_unique_in_output()
    });
    let input_output_sum = input_notes.iter().fold(0, |acc, m| {
        acc + m.get_unscrambled_output()
    });

    println!("Input has {} unique values in the output values", input_uniques);
    println!("Input output values sum to {}", input_output_sum);
}

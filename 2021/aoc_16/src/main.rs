const INPUT: &str = include_str!("input.txt");
const SAMPLE: &str = include_str!("sample.txt");

#[derive(Debug,PartialEq,Eq)]
enum TypeId {
    Sum,
    Prod,
    Min,
    Max,
    Literal,
    Gt,
    Lt,
    Eq
}

#[derive(Debug)]
struct Header {
    version: u64,   // 3 bits
    type_id: TypeId // 3 bits
}

struct Packet {
    header: Header,

    // Operator only
    length_type_id: u64,

    // Literal only
    bit_groups: Vec<u64>, // Each representing a 5-bit chunk

    // Operator only
    sub_packets: Vec<Packet>
}

impl Packet {
    fn from_ascii_bin_input(bits: &mut String) -> Packet {
        let header = get_header(bits);

        // Trim the header
        *bits = bits.as_str()[6..].to_string();

        if header.type_id == TypeId::Literal {
            let mut bit_groups: Vec<u64> = Vec::new();
            loop {
                let chunk = &bits[0..5];
                let val = u64::from_str_radix(chunk, 2).unwrap();
                bit_groups.push(val & 0b1111);

                *bits = bits.as_str()[5..].to_string();

                if val & 0b1_0000 == 0 {
                    break;
                }
            }

            Packet {
                header,
                length_type_id: 0,
                bit_groups,
                sub_packets: Vec::new()
            }
        } else {
            let (length_type_id, sub_packets) = get_sub_packets(bits);

            Packet {
                header,
                length_type_id,
                bit_groups: Vec::new(),
                sub_packets
            }
        }
    }

    fn from_input(input: &str) -> Packet {
        let mut bits = "".to_string();
        for c in input.trim().chars() {
            let bit_s = format!("{:04b}", c.to_digit(16).unwrap());
            bits.push_str(&bit_s);
        }

        Packet::from_ascii_bin_input(&mut bits)
    }

    fn get_value(&self) -> u64 {
        match self.header.type_id {
            TypeId::Literal => {
                let mut value = 0;
                for (i, chunk) in self.bit_groups.iter().rev().enumerate() {
                    value |= chunk << (i * 4);
                }
                value
            },
            TypeId::Sum => {
                self.sub_packets.iter()
                    .fold(0, |acc, p| { acc + p.get_value() })
            },
            TypeId::Prod => {
                self.sub_packets.iter()
                    .fold(1, |acc, p| { acc * p.get_value() })
            },
            TypeId::Min => {
                self.sub_packets.iter()
                    .min_by(|a, b| {
                        a.get_value().cmp(&b.get_value())
                    }).unwrap().get_value()
            },
            TypeId::Max => {
                self.sub_packets.iter()
                    .max_by(|a, b| {
                        a.get_value().cmp(&b.get_value())
                    }).unwrap().get_value()
            },
            TypeId::Gt => {
                assert_eq!(self.sub_packets.len(), 2);
                (self.sub_packets[0].get_value() > self.sub_packets[1].get_value()) as u64
            },
            TypeId::Lt => {
                assert_eq!(self.sub_packets.len(), 2);
                (self.sub_packets[0].get_value() < self.sub_packets[1].get_value()) as u64
            },
            TypeId::Eq => {
                assert_eq!(self.sub_packets.len(), 2);
                (self.sub_packets[0].get_value() == self.sub_packets[1].get_value()) as u64
            }
        }
    }

    fn get_packet_len_bits(&self) -> usize {
        if self.header.type_id == TypeId::Literal {
            6 + (self.bit_groups.len() * 5)
        } else {
            let header_len = 7 + if self.length_type_id == 0 { 15 } else { 11 };
            self.sub_packets.iter().fold(header_len, |acc, p| { acc + p.get_packet_len_bits() })
        }
    }

    fn get_version_sum(&self) -> u64 {
        self.sub_packets.iter().fold(self.header.version, |acc, p| { acc + p.get_version_sum() })
    }
}

fn get_header(input: &str) -> Header {
    let version = u64::from_str_radix(&input[0..3], 2).unwrap();
    let type_id = match u64::from_str_radix(&input[3..6], 2).unwrap() {
        0 => TypeId::Sum,
        1 => TypeId::Prod,
        2 => TypeId::Min,
        3 => TypeId::Max,
        4 => TypeId::Literal,
        5 => TypeId::Gt,
        6 => TypeId::Lt,
        7 => TypeId::Eq,
        _ => panic!("Invalid operator")
    };

    Header { version, type_id }
}

fn get_sub_packets(bits: &mut String) -> (u64, Vec<Packet>) {
    // Get the length type and trim the bit
    let length_type_id = bits.chars().next().unwrap().to_digit(2).unwrap() as u64;
    *bits = bits.as_str()[1..].to_string();

    let mut sub_packets = Vec::new();

    // Process the length and trim the bits
    if length_type_id == 0 {
        let sub_p_len_bits = usize::from_str_radix(&bits.as_str()[0..15], 2).unwrap();
        *bits = bits.as_str()[15..].to_string();

        while sub_packets.iter().fold(0, |acc, p: &Packet| acc + p.get_packet_len_bits()) < sub_p_len_bits {
            sub_packets.push(Packet::from_ascii_bin_input(bits));
        }
    } else {
        let num_sub_packets = usize::from_str_radix(&bits.as_str()[0..11], 2).unwrap();
        *bits = bits.as_str()[11..].to_string();

        while sub_packets.len() < num_sub_packets {
            sub_packets.push(Packet::from_ascii_bin_input(bits));
        }
    }

    (length_type_id, sub_packets)
}

fn main() {
    let mut sample_lines = SAMPLE.lines();
    // Part 1 samples
    assert_eq!(Packet::from_input(sample_lines.next().unwrap()).get_version_sum(), 6);
    assert_eq!(Packet::from_input(sample_lines.next().unwrap()).get_version_sum(), 14);
    assert_eq!(Packet::from_input(sample_lines.next().unwrap()).get_version_sum(), 16);
    assert_eq!(Packet::from_input(sample_lines.next().unwrap()).get_version_sum(), 12);
    assert_eq!(Packet::from_input(sample_lines.next().unwrap()).get_version_sum(), 23);
    assert_eq!(Packet::from_input(sample_lines.next().unwrap()).get_version_sum(), 31);

    // Part 2 samples
    assert_eq!(Packet::from_input(sample_lines.next().unwrap()).get_value(), 3);
    assert_eq!(Packet::from_input(sample_lines.next().unwrap()).get_value(), 54);
    assert_eq!(Packet::from_input(sample_lines.next().unwrap()).get_value(), 7);
    assert_eq!(Packet::from_input(sample_lines.next().unwrap()).get_value(), 9);
    assert_eq!(Packet::from_input(sample_lines.next().unwrap()).get_value(), 1);
    assert_eq!(Packet::from_input(sample_lines.next().unwrap()).get_value(), 0);
    assert_eq!(Packet::from_input(sample_lines.next().unwrap()).get_value(), 0);
    assert_eq!(Packet::from_input(sample_lines.next().unwrap()).get_value(), 1);

    let packet = Packet::from_input(INPUT);
    println!("Version sum: {}", packet.get_version_sum());
    println!("Value: {}", packet.get_value());
}
use std::collections::HashMap;

const INPUT: &str = include_str!("input.txt");
const SAMPLE: &str = include_str!("sample.txt");
const SAMPLE2: &str = include_str!("sample2.txt");
const SAMPLE3: &str = include_str!("sample3.txt");

struct Cave {
    connections: Vec<String>
}

struct System {
    caves: HashMap<String, Cave>
}

impl System {
    fn from_input(input: &str) -> System {
        let mut caves: HashMap<String, Cave> = HashMap::new();

        for line in input.lines() {
            // Processing A-B
            let mut tokens = line.split('-');
            let a = tokens.next().unwrap().to_string();
            let b = tokens.next().unwrap().to_string();

            // Cover  A -> B
            let mut entry = caves.entry(a.clone()).or_insert(Cave {connections: Vec::new()});
            if !entry.connections.contains(&b) {
                entry.connections.push(b.clone());
            }

            // Cover B -> A
            entry = caves.entry(b.clone()).or_insert(Cave {connections: Vec::new()});
            if !entry.connections.contains(&a) {
                entry.connections.push(a);
            }
        }

        System {
            caves
        }
    }

    fn get_paths(&self, start: &str, end: &str, previous: Vec<String>, part: &str) -> Vec<Vec<String>> {
        let mut paths = Vec::new();
        let root = self.caves.get(start).unwrap();
        let mut small_cave_revisited = false;

        // Filter out any small caves we can't jump to
        for conn in &root.connections {
            if conn == "start" { continue; }
            if &conn.to_lowercase() == conn {
                if part == "p2" && !small_cave_revisited {
                    // If any small cave in this path has been revisited, set boolean
                    let mut small_caves: Vec<&String> = previous.iter()
                                                            .filter(|cave| &cave.to_lowercase() == *cave)
                                                            .collect();
                    let current = start.to_string();
                    if start.to_lowercase() == start {
                        small_caves.push(&current);
                    }

                    for cave in &small_caves {
                        if small_caves.iter().filter(|c| *c == cave).count() >= 2 {
                            small_cave_revisited = true;
                        }
                    }
                }
                if part == "p1" || small_cave_revisited {
                    if previous.contains(conn) {
                        continue;
                    }
                }
            }

            let mut new_previous = previous.clone();
            new_previous.push(start.to_string());

            // Base case - the path ends
            if conn == end {
                new_previous.push(end.to_string());
                paths.push(new_previous);
                continue;
            }

            // Recursive call
            paths.append(&mut self.get_paths(conn, end, new_previous, part));
        }

        paths
    }
}

fn main() {
    for (sample, expected_paths_p1, expected_paths_p2) in [(SAMPLE, 10, 36), (SAMPLE2, 19, 103), (SAMPLE3, 226, 3509)] {
        let sample_caves = System::from_input(sample);
        assert_eq!(sample_caves.get_paths("start", "end", Vec::new(), "p1").len(), expected_paths_p1);
        assert_eq!(sample_caves.get_paths("start", "end", Vec::new(), "p2").len(), expected_paths_p2);
    }

    let input_caves = System::from_input(INPUT);
    let input_paths_p1 = input_caves.get_paths("start", "end", Vec::new(), "p1");
    println!("Part 1: input has {} possible paths through system", input_paths_p1.len());

    let input_paths_p2 = input_caves.get_paths("start", "end", Vec::new(), "p2");
    println!("Part 2: input has {} possible paths through system", input_paths_p2.len());
}

const INPUT: &str = include_str!("input.txt");
const SAMPLE: &str = include_str!("sample.txt");

fn get_gamma_rate(report: &[u64], bit_count: u32) -> u64 {
    let mut gamma_rate = 0;
    for i in 0..bit_count {
        let mask = 2_u64.pow(i);
        let num_set = report.iter().filter(|x| {(*x & mask) > 0}).count();
        if num_set >= (report.len() - num_set) {
            gamma_rate |= mask;
        }
    }
    gamma_rate
}

fn get_power_consumption(report: &[u64], bit_count: u32) -> u64 {
    let gamma = get_gamma_rate(report, bit_count);
    println!("Gamma: {}", gamma);
    let epsilon = !gamma & (2_u64.pow(bit_count) - 1);
    println!("Epsilon: {}", epsilon);
    gamma * epsilon
}

fn get_rating(report: &[u64], bit_count: u32, gas: &str) -> u64 {
    let mut report = report.to_vec();
    for i in (0..bit_count).rev() {
        let mask = 2_u64.pow(i);
        let gamma_rate = get_gamma_rate(&report, bit_count);
        println!("Gamma rate: {:b}", gamma_rate);
        let gamma_bit = match gas {
            "o2" => gamma_rate & mask,
            "co2" => (!gamma_rate) & mask,
            _ => panic!("Invalid gas")
        };
        println!("Gamma bit: {:b}", gamma_bit);
        let mut x = 0;
        while x < report.len() {
            if report.len() == 1 { break; }
            if (report[x] & mask) != gamma_bit {
                report.remove(x);
            } else {
                x += 1;
            }
        }
        print!("Keeping: ");
        for r in &report {
            print!("{:b} ", r);
        }
        println!();

        if report.len() == 1 {
            break;
        }
    }
    println!("Got {} rating: {}\n", gas, report[0]);
    report[0]
}

fn get_life_support_rating(report: &[u64], bit_count: u32) -> u64 {
    get_rating(report, bit_count, "o2") * get_rating(report, bit_count, "co2")
}

fn main() {
    let sample_data: Vec<u64> = SAMPLE.lines()
                                      .map(|l| u64::from_str_radix(l, 2)
                                      .unwrap())
                                      .collect();

    assert_eq!(get_power_consumption(&sample_data, 5), 198);
    assert_eq!(get_life_support_rating(&sample_data, 5), 230);

    let input_data: Vec<u64> = INPUT.lines()
                                    .map(|l| u64::from_str_radix(l, 2)
                                    .unwrap())
                                    .collect();

    println!("The input power consumption is {}", get_power_consumption(&input_data, 12));
    println!("The input life support rating is is {}", get_life_support_rating(&input_data, 12));

}
use std::{collections::HashMap, io::Write, hash::Hash};

const INPUT: &str = include_str!("input.txt");
const SAMPLE: &str = include_str!("sample.txt");

#[derive(Debug)]
struct Polymer {
    polymer: Vec<char>,
    rules: HashMap<String, char>
}

impl Polymer {
    fn from_input(input: &str) -> Polymer {
        let (polymer_str, rule_lines) = input.split_once("\n\n").unwrap();

        let mut rules: HashMap<String, char> = HashMap::new();
        for line in rule_lines.lines() {
            let (pair, insert) = line.split_once(" -> ").unwrap();
            rules.insert(pair[0..2].to_string(), insert.chars().nth(0).unwrap());
        }

        Polymer {
            polymer: polymer_str.chars().collect(),
            rules
        }
    }

    fn count_char_occurance_for_step(&self, chars: &[char], n_steps: usize) -> (String, HashMap<char, usize>) {
        let mut new_p: Vec<char> = Vec::with_capacity(chars.len() * 2);
        for (i, pair) in chars.windows(2).enumerate() {
            let new_char = *self.rules.get(&pair.iter().collect::<String>()).unwrap();
            // Only add the first char if it's the first iteration, otherwise you'll double-count
            if i == 0 {
                new_p.push(pair[0]);
            }
            new_p.push(new_char);
            new_p.push(pair[1]);
        }

        // Base case
        if n_steps == 1 {
            let polymer = new_p.iter().collect();
            (polymer, self.get_count_map(new_p.iter().collect()))
        } else {
            self.count_char_occurance_for_step(&new_p, n_steps - 1)
        }
    }

    fn compute_counts(&self, step_count: usize) -> (String, HashMap<String, HashMap<char, usize>>) {
        let mut polymer = "".to_string();
        let mut known_counts: HashMap<String, HashMap<char, usize>> = HashMap::new();
        for (i, pair) in self.polymer.windows(2).enumerate() {
            let pair_string: String = pair.iter().collect();
            let (new_polymer, mut map) = self.count_char_occurance_for_step(pair, step_count);
            println!("We now know that after {} steps, {} will produce in {:?}", step_count, pair_string, map);
            if i > 0 {
                println!("Not first item - decrease count of first letter by 1");
                *map.get_mut(&pair[0]).unwrap() -= 1;
            }

            known_counts.insert(pair_string, map);
            polymer.push_str(&new_polymer);
        }

        (polymer, known_counts)
    }

    fn get_total_count_p1(&self, step_count: usize) -> HashMap<char, usize> {
        let (polymer, mut known_counts) = self.compute_counts(step_count);

        let mut ret: HashMap<char, usize> = HashMap::new();
        for pair in self.polymer.windows(2).map(|pair| pair.iter().collect::<String>()) {
            let map = known_counts.entry(pair.clone()).or_insert_with(|| {
                let pair_c = pair.chars().collect::<Vec<char>>();
                let count = self.count_char_occurance_for_step(&pair_c, step_count).1;
                println!("Missed pair {} - had to calculate: {:?}", pair, count);
                count
            });

            for (c, count) in map {
                let e = ret.entry(*c).or_insert(0);
                *e += *count;
            }
        }

        println!("Total count after {} steps: {:?}", step_count, ret);
        ret
    }

    fn get_total_count_p2(&self, step_count: usize) -> HashMap<char, usize> {
        // Only do half the work
        let (halfway_polymer, mut known_counts_halfway) = self.compute_counts(step_count / 2);

        let mut ret: HashMap<char, usize> = HashMap::new();
        for (i, pair) in halfway_polymer.chars().collect::<Vec<char>>().windows(2).map(|p| p.iter().collect::<String>()).enumerate() {
            let known_count = known_counts_halfway.entry(pair.clone()).or_insert_with(|| {
                // IN THE CASE WHERE YOU MISS A PAIR, CALCULATE FROM THE STEP IT APPEARS, NOT THE TOP LEVEL
                let pair_c = pair.chars().collect::<Vec<char>>();
                let count = self.count_char_occurance_for_step(&pair_c, step_count / 2).1;
                println!("Missed pair {} - had to calculate: {:?}", pair, count);
                count
            });

            for (c, count) in known_count.iter() {
                let e = ret.entry(*c).or_insert(0);
                *e += *count;
            }

            //if i >= halfway_polymer.len() - 2 {
            //    //println!("Last item - do nothing!")
            //} else {
            //    //println!("Not last item - decrease count of second letter by 1");
            //    *known_count.get_mut(&pair.chars().nth(1).unwrap()).unwrap() -= 1;
            //}
        }

        println!("Total count after {} steps: {:?}", step_count, ret);
        ret
    }

    fn get_count_map(&self, p: String) -> HashMap<char, usize> {
        let mut count_map: HashMap<char, usize> = HashMap::new();

        for c in p.chars() {
            let e = count_map.entry(c).or_insert(0);
            *e += 1;
        }

        count_map
    }

    fn get_val(&self, n_steps: usize) -> usize {
        let count_map = if n_steps <= 40 {
            self.get_total_count_p1(n_steps)
        } else {
            self.get_total_count_p2(n_steps)
        };

        let (min_char, _) = count_map.iter().min_by(|a, b| {
            a.1.cmp(b.1)
        }).unwrap();

        let (max_char, _) = count_map.iter().max_by(|a, b| {
            a.1.cmp(b.1)
        }).unwrap();

        count_map.get(max_char).unwrap() - count_map.get(min_char).unwrap()
    }
}

fn main() {
    let sample_polymer = Polymer::from_input(SAMPLE);
    let input_polymer = Polymer::from_input(INPUT);

    assert_eq!(sample_polymer.get_val(10), 1588);
    println!("Part 1 value is {}", input_polymer.get_val(10));

    assert_eq!(sample_polymer.get_val(40), 2_188_189_693_529);
    println!("Part 2 value is {}", input_polymer.get_val(40));
}

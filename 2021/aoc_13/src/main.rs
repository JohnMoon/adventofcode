use std::collections::HashMap;
use std::fmt;

const INPUT: &str = include_str!("input.txt");
const SAMPLE: &str = include_str!("sample.txt");

struct Fold {
    horizontal: bool,
    along: usize
}

struct Page {
    grid: HashMap<(usize, usize), bool>,
    folds: Vec<Fold>
}

impl fmt::Display for Page {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let x_max = self.x_max();
        let y_max = self.y_max();
        println!("{}x{} grid", x_max, y_max);

        for y in 0..y_max {
            for x in 0..x_max {
                match self.grid.get(&(x, y)) {
                    Some(b) => if *b { write!(f, "#")? } else { write!(f, ".")? },
                    None => write!(f, ".")?
                }
            }
            writeln!(f)?;
        }
        Ok(())
    }
}

impl Page {
    fn from_input(input: &str) -> Page {
        let mut grid = HashMap::new();
        let mut lines = input.lines();
        loop {
            let line = lines.next().unwrap();
            if line.is_empty() { break; }

            let mut tokens = line.split(',');
            let point = (tokens.next().unwrap().parse::<usize>().unwrap(),
                                      tokens.next().unwrap().parse::<usize>().unwrap());

            let e = grid.entry(point).or_default();
            *e = true;
        }

        let mut folds = Vec::new();
        for line in lines {
            let trimmed_line = line.replace("fold along ", "");
            let mut tokens = trimmed_line.split('=');
            let horizontal = tokens.next().unwrap() == "y";
            let along = tokens.next().unwrap().parse::<usize>().unwrap();
            folds.push(Fold { horizontal, along });
        }

        Page {
            grid,
            folds
        }
    }

    fn run_folds(&mut self, n_folds: usize) {
        let mut n = 0;
        for fold in self.folds.iter().take(n_folds) {
            n += 1;
            if fold.horizontal {
                for y in (fold.along + 1)..self.y_max() {
                    for x in 0..self.x_max() {
                        if self.grid.remove(&(x, y)).is_some() {
                            let new_y = fold.along - (y - fold.along);
                            let e = self.grid.entry((x, new_y)).or_default();
                            *e = true;
                        }
                    }
                }
            } else {
                for x in (fold.along + 1)..self.x_max() {
                    for y in 0..self.y_max() {
                        if self.grid.remove(&(x, y)).is_some() {
                            let new_x = fold.along - (x - fold.along);
                            let e = self.grid.entry((new_x, y)).or_default();
                            *e = true;
                        }
                    }
                }
            }
            println!("After fold {}, {} dots", n, self.count_dots());
        }
    }

    fn x_max(&self) -> usize {
        self.grid.keys().map(|k| { k.0 }).max().unwrap() + 1
    }

    fn y_max(&self) -> usize {
        self.grid.keys().map(|k| { k.1 }).max().unwrap() + 1
    }

    fn count_dots(&self) -> usize {
        self.grid.values().count()
    }
}

fn main() {
    let mut sample_page = Page::from_input(SAMPLE);
    sample_page.run_folds(sample_page.folds.len());
    assert_eq!(sample_page.count_dots(), 16);
    println!("Sample page:\n{}", sample_page);

    let mut input_page = Page::from_input(INPUT);
    input_page.run_folds(input_page.folds.len());
    println!("{}", input_page);
}
use std::collections::HashMap;

const INPUT: &str = include_str!("input.txt");
const SAMPLE: &str = include_str!("sample.txt");

#[derive(Clone)]
struct Grid {
    grid: Vec<Vec<u32>>
}

impl Grid {
    fn from_input(input: &str) -> Grid {
        let grid: Vec<Vec<u32>> = input.lines().map(|line| {
            line.chars().map(|c| c.to_digit(10).unwrap()).collect()
        }).collect();

        Grid { grid }
    }

    fn from_input_p2(input: &str) -> Grid {
        let orig_grid = Grid::from_input(input);
        let mut new_grid = orig_grid.clone();
        println!("originally: {:?}", new_grid.get_terminal_point());

        // Start by going down
        for i in 1..5 {
            for row in &orig_grid.grid {
                new_grid.grid.push({
                    row.iter().map(|n| {
                        let new_val = n + i;
                        if new_val > 9 {
                            (new_val % 10) + 1
                        } else {
                            new_val
                        }
                    }).collect()
                });
            }
        }

        println!("after expanding down: {:?}", new_grid.get_terminal_point());

        // Then, append to the sides
        let orig_row_len = orig_grid.grid[0].len();
        for i in 1..5 {
            for row in new_grid.grid.iter_mut() {
                row.append(
                    &mut row.iter().take(orig_row_len).map(|n| {
                        let new_val = n + i as u32;
                        if new_val > 9 {
                            (new_val % 10) + 1
                        } else {
                            new_val
                        }
                    }).collect()
                );
            }
        }

        println!("after expanding to the sides: {:?}", new_grid.get_terminal_point());

        new_grid
    }


    fn get_terminal_point(&self) -> (usize, usize) {
        (self.grid[0].len() - 1, self.grid.len() - 1)
    }

    fn get_adjacent_points(&self, current_point: (usize, usize)) -> Vec<(usize, usize)> {
        let terminal_point = self.get_terminal_point();
        let mut adjacent_points = Vec::new();
        if current_point.0 < terminal_point.0 {
            adjacent_points.push((current_point.0 + 1, current_point.1));
        }
        if current_point.1 < terminal_point.1 {
            adjacent_points.push((current_point.0, current_point.1 + 1));
        }
        adjacent_points
    }

    fn get_lowest_risk_level_rec(&self, current_point: (usize, usize),
                                        known_points: &mut HashMap<(usize, usize), u32>) -> u32 {
        if let Some(risk_level) = known_points.get(&current_point) {
            return *risk_level;
        }

        let terminal_point = self.get_terminal_point();

        // Base case
        if current_point == terminal_point {
            println!("Reached path end!");
            known_points.insert(terminal_point, 0);
            return 0;
        }

        let paths_to_try = self.get_adjacent_points(current_point);

        let min_risk = paths_to_try.iter().map(|p| {
            self.get_lowest_risk_level_rec(*p, known_points) + self.grid[p.1][p.0]
        }).min().unwrap();

        known_points.insert(current_point, min_risk);

        min_risk
    }

    // Helper function for triggering the recursion
    fn get_lowest_risk_level(&self) -> u32 {
        self.get_lowest_risk_level_rec((0, 0), &mut HashMap::new())
    }
}

fn main() {
    let sample = Grid::from_input(SAMPLE);
    assert_eq!(sample.get_lowest_risk_level(), 40);
    let sample_p2 = Grid::from_input_p2(SAMPLE);
    assert_eq!(sample_p2.get_lowest_risk_level(), 315);

    let input = Grid::from_input(INPUT);
    println!("P1: Lowest risk path total: {}", input.get_lowest_risk_level());

    let input_p2 = Grid::from_input_p2(INPUT);
    println!("P2: Lowest risk path total: {} (should be 2778 - come back later to fix)", input_p2.get_lowest_risk_level());
}

const INPUT: &str = include_str!("input.txt");
const SAMPLE: &str = include_str!("sample.txt");

#[derive(Debug,Clone)]
struct Board {
    grid: Vec<Vec<u32>>,
    grid_t: Vec<Vec<u32>>,
    bingo: bool,
    called_nums: Vec<u32>
}

impl Board {
    fn parse_from_lines(lines: &[&str]) -> Board {
        let mut grid: Vec<Vec<u32>> = Vec::new();
        for line in lines {
            let mut row: Vec<u32> = Vec::new();
            for val in line.split_whitespace() {
                row.push(val.parse::<u32>().unwrap());
            }
            grid.push(row);
        }

        // Transpose the grid for easier column processing
        let n_cols = grid[0].len();
        let mut iters: Vec<_> = grid.clone().into_iter().map(|n| n.into_iter()).collect();

        let grid_t = (0..n_cols)
            .map(|_| {
                iters.iter_mut()
                     .map(|n| n.next().unwrap())
                     .collect::<Vec<u32>>()
            })
            .collect();

        Board { grid, grid_t, bingo: false, called_nums: Vec::new() }
    }

    fn mark_num(&mut self, num: u32) {
        if !self.bingo {
            self.called_nums.push(num);
            if self.has_bingo_row() {
                println!("BINGO");
                self.bingo = true;
            }
        }
    }

    fn has_bingo_row(&self) -> bool {
        println!("Called numbers {:?}", self.called_nums);

        self.grid.iter().any(|row| {
            println!("Row: {:?}", row);
            row.iter().all(|x| self.called_nums.contains(x))
        })
        ||
        self.grid_t.iter().any(|col| {
            println!("Col: {:?}", col);
            col.iter().all(|x| self.called_nums.contains(x))
        })
    }

    fn get_val(&self) -> u32 {
        let mut unmarked_sum: u32 = 0;
        for row in &self.grid {
            for val in row {
                if !self.called_nums.contains(val) { unmarked_sum += val }
            }
        }
        println!("Unmarked sum: {}", unmarked_sum);
        println!("Last num: {}", self.called_nums.last().unwrap());
        unmarked_sum * self.called_nums.last().unwrap()
    }
}

#[derive(Debug)]
struct Game {
    called_nums: Vec<u32>,
    boards: Vec<Board>
}

impl Game {
    fn from_input(input: &str) -> Game {
        let called_nums: Vec<u32> = input.lines()
            .next()
            .unwrap()
            .split(',')
            .map(|x| x.parse::<u32>().unwrap())
            .collect();
        let mut board_lines = Vec::new();
        let mut boards = Vec::new();
        for line in input.lines().skip(2) {
            println!("Line: {}", line);
            if line.is_empty() && !board_lines.is_empty() {
                boards.push(Board::parse_from_lines(&board_lines));
                board_lines.clear();
            } else {
                board_lines.push(line);
            }
        }
        boards.push(Board::parse_from_lines(&board_lines));

        Game {
            called_nums,
            boards
        }
    }

    // Run the game, return the winning board
    fn run_game(&mut self) -> Option<Board> {
        for num in &self.called_nums {
            println!("\n\nCalling {}!", num);
            for board in self.boards.iter_mut() {
                board.mark_num(*num);
            }

            let winning_boards: Vec<&Board> = self.boards
                .iter()
                .filter(|x| x.bingo)
                .collect();
            if !winning_boards.is_empty() {
                println!("{} boards have bingo!", winning_boards.len());
                return Some((*(winning_boards[0])).clone());
            }
        }
        None
    }

    fn run_game_p2(&mut self) -> Option<Board> {
        let mut i = 0;
        for num in &self.called_nums {
            i += 1;
            println!("\n\nCalling {}!", num);

            for board in self.boards.iter_mut() {
                board.mark_num(*num);
            }

            let winning_boards: Vec<&Board> = self.boards
                .iter()
                .filter(|x| x.bingo)
                .collect();
            println!("After {} rounds, we have {} winners", i, winning_boards.len());
            if winning_boards.len() == self.boards.len() {
                println!("All boards have won");
                let mut last_winner = winning_boards[0];
                for board in winning_boards.iter().skip(1) {
                    if board.called_nums.len() > last_winner.called_nums.len() {
                        last_winner = board;
                    }
                }
                println!("Last winning board:");
                for r in &last_winner.grid {
                    println!("{:?}", r);
                }
                return Some(last_winner.clone());
            }
        }
        None
    }
}

fn main() {
    let mut sample_game = Game::from_input(SAMPLE);
    assert_eq!(sample_game.run_game().unwrap().get_val(), 4512);
    sample_game = Game::from_input(SAMPLE);
    assert_eq!(sample_game.run_game_p2().unwrap().get_val(), 1924);

    let mut input_game = Game::from_input(INPUT);
    if let Some(winning_board) = input_game.run_game() {
        println!("Winning bingo has value: {}", winning_board.get_val());
    }

    input_game = Game::from_input(INPUT);
    if let Some(winning_board) = input_game.run_game_p2() {
        println!("Final winning bingo has value: {}", winning_board.get_val());
    }
}
use std::collections::HashMap;

const INPUT: &str = include_str!("input.txt");
const SAMPLE: &str = include_str!("sample.txt");

#[derive(Copy,Clone,Debug,PartialEq,Eq,Hash)]
struct Point {
    x: usize,
    y: usize
}

#[derive(Copy,Clone,Debug)]
struct Line {
    start: Point,
    end: Point
}

impl Line {
    fn from_str(input: &str) -> Line {
        let tokens: Vec<usize> = input.replace(" -> ", ",")
            .split(',')
            .map(|s| s.parse::<usize>().unwrap())
            .collect();
        Line {
            start: Point {
                x: tokens[0],
                y: tokens[1]
            },
            end: Point {
                x: tokens[2],
                y: tokens[3]
            }
        }
    }

    fn get_points(&self) -> Vec<Point> {
        let mut points = vec![self.start];
        let mut traverse = self.start;

        while traverse != self.end {
            if self.start.x != self.end.x {
                match self.end.x > traverse.x {
                    true => traverse.x += 1,
                    false => traverse.x -= 1
                };
            }

            if self.start.y != self.end.y {
                match self.end.y > traverse.y {
                    true => traverse.y += 1,
                    false => traverse.y -= 1
                };
            }

            points.push(traverse);
        }

        points
    }
}

fn count_overlaps(input: &[Line], include_diag: bool) -> usize {
    let mut all_points: Vec<Vec<Point>> =  input.iter().map(|l| l.get_points()).collect();
    if !include_diag {
        all_points = all_points.into_iter().filter(|l| {
            let first = l.first().unwrap();
            let last = l.last().unwrap();
            first.x == last.x || first.y == last.y
        })
        .collect();
    }
    let mut point_count = HashMap::new();
    for line in all_points {
        for point in line {
            if let Some(count) = point_count.get_mut(&point) {
                *count += 1_usize;
            } else {
                point_count.insert(point, 1_usize);

            }
        }
    }
    point_count.iter().filter(|(_, count)| *count > &1).count()
}

fn main() {
    let sample_lines: Vec<Line> = SAMPLE.lines().map(|l| Line::from_str(l)).collect();
    assert_eq!(count_overlaps(&sample_lines, false), 5);
    assert_eq!(count_overlaps(&sample_lines, true), 12);

    let input_lines: Vec<Line> = INPUT.lines().map(|l| Line::from_str(l)).collect();
    println!("{} points in straight lines overlap", count_overlaps(&input_lines, false));
    println!("{} points in all lines overlap", count_overlaps(&input_lines, true));
}
const INPUT: &str = include_str!("input.txt");
const SAMPLE: &str = include_str!("sample.txt");

fn count_increases(list: &[u64]) -> usize {
    list.windows(2)
        .filter(|pair| pair[1] > pair[0])
        .count()
}

fn count_increases_p2(list: &[u64]) -> usize {
    list.windows(3)
        .collect::<Vec<&[u64]>>()
        .windows(2)
        .filter(|group| {
            group[1].iter().sum::<u64>() > group[0].iter().sum::<u64>()
        })
        .count()
}

fn main() {
    let sample_data: Vec<u64> = SAMPLE.lines()
                                      .map(|l| l.parse::<u64>()
                                      .unwrap())
                                      .collect();
    assert_eq!(count_increases(&sample_data), 7);
    assert_eq!(count_increases_p2(&sample_data), 5);

    let input: Vec<u64> = INPUT.lines()
                               .map(|l| l.parse::<u64>()
                               .unwrap())
                               .collect();

    println!("Part 1: Input list has {} increases", count_increases(&input));
    println!("Part 2: Input list has {} increases", count_increases_p2(&input));
}
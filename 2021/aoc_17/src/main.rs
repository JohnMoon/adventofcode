use std::ops::RangeInclusive;

const INPUT: &str = include_str!("input.txt");
const SAMPLE: &str = include_str!("sample.txt");

struct Target {
    target_x_range: RangeInclusive<i64>,
    target_y_range: RangeInclusive<i64>
}

impl Target {
    fn from_input(input: &str) -> Target {
        // Expecting manually processed input CSV
        // "target area: x=20..30, y=-10..-5" -> "20,30,-10,-5"
        let mut tokens = input.trim().split(',');
        let target_x_start = tokens.next().unwrap().parse::<i64>().unwrap();
        let target_x_end = tokens.next().unwrap().parse::<i64>().unwrap();
        let target_y_start = tokens.next().unwrap().parse::<i64>().unwrap();
        let target_y_end = tokens.next().unwrap().parse::<i64>().unwrap();

        Target {
            target_x_range: target_x_start..=target_x_end,
            target_y_range: target_y_start..=target_y_end
        }
    }

    fn triangle_expansion_in_x_range(&self, start_velocity: i64) -> Vec<usize> {
        let range = &self.target_x_range;
        let mut v = start_velocity;
        let mut x = 0;
        let mut steps_where_v_is_in_range = Vec::new();
        let mut step  = 0;
        while v >= 0 && x <= *range.end() {
            x += v;
            v -= 1;
            step += 1;

            if range.contains(&x) {
                steps_where_v_is_in_range.push(step);

                // Special condition - when v == 0, all remaining possible steps
                // are possible. Note this by adding a 0
                if v == 0 {
                    steps_where_v_is_in_range.push(0);
                    break;
                }
            }
        }
        steps_where_v_is_in_range
    }

    fn triangle_expansion_in_y_range(&self, start_velocity: i64) -> Vec<usize> {
        let range = &self.target_y_range;
        let mut y = 0;
        let mut steps_where_v_is_in_range = Vec::new();

        // When falling back down, the velocity will be 1 greater than
        // the starting velocity when traveling back across the origin.
        // Also, add on the steps it takes to travel up and back down
        let (mut step, mut v) = {
            if start_velocity >= 0 {
                ((start_velocity * 2 + 1) as usize,
                -(start_velocity + 1))
            } else {
                (0, start_velocity)
            }
        };

        while y >= *range.start() {
            y += v;
            v -= 1;
            step += 1;

            if range.contains(&y) {
                steps_where_v_is_in_range.push(step);
            }
        }
        steps_where_v_is_in_range
    }

    fn get_possible_x_velocities(&self) -> Vec<(i64, Vec<usize>)> {
        let mut possible_x_v = Vec::new();

        for x_v in 1..=*self.target_x_range.end() {
            let working_steps = self.triangle_expansion_in_x_range(x_v);
            if !working_steps.is_empty() {
                possible_x_v.push((x_v, working_steps));
            }
        }
        possible_x_v
    }

    fn get_possible_y_velocities(&self) -> Vec<(i64, Vec<usize>)> {
        let mut possible_y_v = Vec::new();

        let bottom_magnitude = i64::abs(*self.target_y_range.start());
        for y_v in -bottom_magnitude..=bottom_magnitude {
            let working_steps = self.triangle_expansion_in_y_range(y_v);
            if !working_steps.is_empty() {
                possible_y_v.push((y_v, working_steps));
            }
        }
        possible_y_v
    }

    fn get_working_shots(&self) -> Vec<(i64, i64)> {
        let possible_x_velocities = self.get_possible_x_velocities();
        let possible_y_velocities = self.get_possible_y_velocities();
        let mut working_shots = Vec::new();
        for x in &possible_x_velocities {
            let mut possible_x_steps = x.1.clone();
            let mut x_stalls_at = None;
            if possible_x_steps.last().unwrap() == &0 {
                possible_x_steps.pop();
                x_stalls_at = Some(possible_x_steps.last().unwrap());
            }
            for y in &possible_y_velocities {
                if let Some(stall_step) = x_stalls_at {
                    if y.1.iter().any(|s| s >= stall_step) {
                        working_shots.push((x.0, y.0));
                        continue;
                    }
                }

                if possible_x_steps.iter().any(|s| y.1.contains(s)) {
                    working_shots.push((x.0, y.0));
                }
            }
        }

        working_shots
    }

    fn get_highest_y(&self) -> i64 {
        let working_shots = self.get_working_shots();
        let max_y_v = working_shots.iter().max_by(|a, b| {
            a.1.cmp(&b.1)
        }).unwrap();

        let mut v = max_y_v.1;
        let mut p = 0;
        while v > 0 {
            p += v;
            v -= 1;
        }
        p
    }
}

fn main() {
    let sample_target = Target::from_input(SAMPLE);
    assert_eq!(sample_target.get_highest_y(), 45);
    assert_eq!(sample_target.get_working_shots().len(), 112);

    let target = Target::from_input(INPUT);
    println!("Highest y position is {}", target.get_highest_y());
    println!("Number of working shots {}", target.get_working_shots().len());
}
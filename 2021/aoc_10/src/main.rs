const INPUT: &str = include_str!("input.txt");
const SAMPLE: &str = include_str!("sample.txt");

fn open_to_close(c: &char) -> char {
    match c {
        '(' => ')',
        '[' => ']',
        '{' => '}',
        '<' => '>',
        _ => unreachable!()
    }
}

fn from_chars(chars: &[char]) -> Result<usize, char> {
    let mut n_chunks = 0;
    let mut search_stack = vec![*chars.first().unwrap()];

    for c in chars.iter().skip(1) {
        if let Some(current_open_char) = search_stack.last() {
            let current_close_char = open_to_close(current_open_char);

            if c == &current_close_char {
                search_stack.pop();
                n_chunks += 1;
            } else if ['(', '[', '{', '<'].contains(c) {
                search_stack.push(*c);
            } else {
                return Err(*c);
            }
        }
    }
    Ok(n_chunks)
}

fn get_illegal_syntax_score(input: &str) -> u64 {
    input.lines().fold(0_u64, |acc, line| {
        acc + match from_chars(&line.chars().collect::<Vec<char>>()) {
            Ok(_) => 0,
            Err(e) => match e {
                ')' => 3,
                ']' => 57,
                '}' => 1197,
                '>' => 25137,
                _ => unreachable!()
            }
        }
    })
}

fn get_incomplete_lines(input: &str) -> Vec<&str> {
    input.lines().filter(|line| {
        from_chars(&line.chars().collect::<Vec<char>>()).is_ok()
    }).collect()
}

fn get_completion_strings(input: &str) -> Vec<String> {
    let incomplete_lines = get_incomplete_lines(input);
    println!("{} incomplete lines", incomplete_lines.len());
    let mut completion_strings = Vec::new();
    for inc_line in incomplete_lines {
        let chars = inc_line.chars().collect::<Vec<char>>();
        let mut search_stack = vec![*chars.first().unwrap()];

        for c in chars.iter().skip(1) {
            if let Some(current_open_char) = search_stack.last() {
                let current_close_char = open_to_close(current_open_char);

                if c == &current_close_char {
                    search_stack.pop();
                } else if ['(', '[', '{', '<'].contains(c) {
                    search_stack.push(*c);
                } else {
                    unreachable!();
                }
            } else {
                search_stack.push(*c);
            }
        }

        let completion_str = search_stack.iter().rev().map(|c| {
            open_to_close(c)
        }).collect();

        completion_strings.push(completion_str);
    }
    completion_strings
}

fn get_middle_completion_string_score(input: &str) -> u64 {
    let mut completion_string_scores: Vec<u64> = get_completion_strings(input).iter().map(|cs| {
        cs.chars().fold(0_u64, |acc, c| {
            (acc * 5) + match c {
                ')' => 1,
                ']' => 2,
                '}' => 3,
                '>' => 4,
                _ => unreachable!()
            }
        })
    }).collect();

    completion_string_scores.sort_unstable();
    completion_string_scores[completion_string_scores.len() / 2]
}

fn main() {
    assert_eq!(get_illegal_syntax_score(SAMPLE), 26397);
    assert_eq!(get_middle_completion_string_score(SAMPLE), 288957);
    println!("Input syntax error score: {}", get_illegal_syntax_score(INPUT));
    println!("Input middle completion score: {}", get_middle_completion_string_score(INPUT));
}
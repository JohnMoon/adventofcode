const INPUT: &str = include_str!("input.txt");
const SAMPLE: &str = include_str!("sample.txt");

const QUANTUM_ROLLS: [u64; 7] = [3, 4, 5, 6, 7, 8, 9];
//const QUANTUM_ROLLS: [u64; 2] = [3, 9];

struct Game {
    p1_pos: u64,
    p2_pos: u64,
    p1_score: u64,
    p2_score: u64,
    die_rolls: Vec<u64>
}

impl Game {
    fn from_input(input: &str) -> Game {
        let mut lines = input.trim().lines();

        // Subtract one from inputs to make the position zero-indexed
        let p1_pos = lines.next().unwrap().split_whitespace().nth(4).unwrap().parse::<u64>().unwrap() - 1;
        let p2_pos = lines.next().unwrap().split_whitespace().nth(4).unwrap().parse::<u64>().unwrap() - 1;

        Game { p1_pos, p2_pos, p1_score: 0, p2_score: 0, die_rolls: Vec::new() }
    }

    fn roll(&mut self) -> u64 {
        // Deterministic dice
        let new_roll = {
            if let Some(last_roll) = self.die_rolls.last() {
                if *last_roll == 100 {
                    1
                } else {
                    last_roll + 1
                }
            } else {
                1
            }
        };
        self.die_rolls.push(new_roll);
        new_roll
    }

    fn roll_thrice(&mut self) -> u64 {
        let mut rolls = Vec::new();
        for _ in 0..3 {
            rolls.push(self.roll());
        }
        rolls.iter().sum()
    }

    fn play_round(&mut self, winning_score: u64) {
        let p1_round_roll = self.roll_thrice();
        self.p1_pos = (self.p1_pos + p1_round_roll) % 10;
        self.p1_score += self.p1_pos + 1;

        if self.p1_score >= winning_score {
            return;
        }

        let p2_round_roll = self.roll_thrice();
        self.p2_pos = (self.p2_pos + p2_round_roll) % 10;
        self.p2_score += self.p2_pos + 1;
    }

    fn play_to(&mut self, winning_score: u64) {
        while self.p1_score < winning_score && self.p2_score < winning_score {
            self.play_round(winning_score);
        }
    }

    // Recursive function
    fn calc_wins_r(&self, p1_pos: u64, p2_pos: u64, p1_score: u64, p2_score: u64) -> (u64, u64) {
        //println!("p1_pos: {}, p2_pos: {}, p1_score: {}, p2_score: {}", p1_pos, p2_pos, p1_score, p2_score);
        let mut p1_wins = 0;
        let mut p2_wins = 0;

        // Try each possible roll combo
        for p1_roll in QUANTUM_ROLLS {
            let p1_new_pos = (p1_pos + p1_roll) % 10;
            let p1_new_score = p1_score + p1_new_pos + 1;
            let p1_win_inc = match p1_roll {
                3|9 => 1,
                4|8 => 3,
                5|7 => 6,
                    6 => 7,
                _ => unreachable!()
            };
            if p1_new_score >= 21 {
                p1_wins += p1_win_inc;
                continue;
            }

            for p2_roll in QUANTUM_ROLLS {
                let p2_new_pos = (p2_pos + p2_roll) % 10;
                let p2_new_score = p2_score + p2_new_pos + 1;
                let p2_win_inc = match p2_roll {
                    3|9 => 1,
                    4|8 => 3,
                    5|7 => 6,
                        6 => 7,
                    _ => unreachable!()
                };
                if p2_new_score >= 21 {
                    p2_wins += p2_win_inc;
                    continue;
                }

                // If neither of these rolls produces a winner, recurse
                let next_round_win_count = self.calc_wins_r(p1_new_pos, p2_new_pos, p1_new_score, p2_new_score);
                p1_wins += next_round_win_count.0 + p1_win_inc;
                p2_wins += next_round_win_count.1 + p2_win_inc;
            }
        }

        //println!("Win count: {} vs {}", p1_wins, p2_wins);
        (p1_wins, p2_wins)
    }

    // Recusive starter
    fn calc_wins(&self) -> (u64, u64) {
        self.calc_wins_r(self.p1_pos, self.p2_pos, self.p1_score, self.p2_score)
    }

    fn get_loser_score_times_num_rolls(&self) -> u64 {
        let loser_score = self.p1_score.min(self.p2_score);
        loser_score * (self.die_rolls.len() as u64)
    }
}

fn main() {
    let mut sample = Game::from_input(SAMPLE);
    sample.play_to(1000);
    assert_eq!(sample.get_loser_score_times_num_rolls(), 739_785);
    sample = Game::from_input(SAMPLE);
    assert_eq!(sample.calc_wins(), (444356092776315, 341960390180808));

    let mut input = Game::from_input(INPUT);
    input.play_to(1000);
    println!("Deterministic die: {}", input.get_loser_score_times_num_rolls());

}

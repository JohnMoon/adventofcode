const INPUT: &str = include_str!("input.txt");
const SAMPLE: &str = include_str!("sample.txt");

struct Grid {
    grid: Vec<Vec<u32>>
}

impl Grid {
    fn from_input(input: &str) -> Grid {
        Grid {
            grid: input.lines().map(|row| {
                row.chars().map(|c| c.to_digit(10).unwrap()).collect()
            }).collect()
        }
    }

    fn x_max(&self) -> usize {
        self.grid[0].len()
    }

    fn y_max(&self) -> usize {
        self.grid.len()
    }

    fn get_adjacent_points(&self, point: (usize, usize)) -> Vec<(usize, usize)> {
        let mut adjacent_points = Vec::new();

        // In this case, adjacent does not include diagonals
        if point.0 > 0 {
            adjacent_points.push((point.0 - 1, point.1));
        }
        if point.0 < self.x_max() - 1 {
            adjacent_points.push((point.0 + 1, point.1));
        }
        if point.1 > 0 {
            adjacent_points.push((point.0, point.1 - 1));
        }
        if point.1 < self.y_max() - 1 {
            adjacent_points.push((point.0, point.1 + 1));
        }

        adjacent_points
    }

    fn get_low_points(&self) -> Vec<(usize, usize)> {
        let mut low_points: Vec<(usize, usize)> = Vec::new();
        for x in 0..self.x_max() {
            for y in 0..self.y_max() {
                let point_val = self.grid[y][x];
                let adjacent_points = self.get_adjacent_points((x, y));

                // If all adjacent points are greater than this one, it's a low point
                if adjacent_points.iter().all(|p| {
                    self.grid[p.1][p.0] > point_val
                }) {
                    low_points.push((x, y));
                }
            }
        }
        low_points
    }

    fn get_low_point_risk_level(&self) -> u32 {
        let low_points = self.get_low_points();
        low_points.iter().fold(0_u32, |acc, p| {
            acc + (1 + self.grid[p.1][p.0])
        })
    }

    // Recursive function that steps out from the original low point, summing up the basin
    fn get_basin_size(&self, low_point: (usize, usize), seen_points: &mut Vec<(usize, usize)>) -> usize {
        let adjacent_points = self.get_adjacent_points(low_point);

        // If an adjacent point has not been seen before in this basin, and is less than nine, add to the basin
        let basin_points: Vec<(usize, usize)> = adjacent_points.into_iter().filter(|p| {
           (! seen_points.contains(p)) && self.grid[p.1][p.0] < 9
        }).collect();

        seen_points.append(&mut basin_points.clone());

        // For each new point in the basin, call this function recursively (updating seen_points as we go)
        for point in basin_points {
            self.get_basin_size(point, seen_points);
        }

        seen_points.len()
    }

    fn get_product_of_three_largest_basins(&self) -> usize {
        let mut basin_sizes: Vec<usize> = self.get_low_points().iter().map(|p| {
            self.get_basin_size(*p, &mut vec![*p])
        }).collect();

        basin_sizes.sort_unstable();
        basin_sizes.iter().rev().take(3).product()
    }
}

fn main() {
    let sample_grid = Grid::from_input(SAMPLE);
    assert_eq!(sample_grid.get_low_point_risk_level(), 15);
    assert_eq!(sample_grid.get_product_of_three_largest_basins(), 1134);

    let input_grid = Grid::from_input(INPUT);
    println!("Sum of risk levels: {}", input_grid.get_low_point_risk_level());
    println!("Product of three largest basin sizes: {}", input_grid.get_product_of_three_largest_basins());
}
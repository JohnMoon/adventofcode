const INPUT: &str = include_str!("input.txt");
const SAMPLE: &str = include_str!("sample.txt");

fn calc_position(moves: &[&str]) -> i64 {
    let mut depth = 0;
    let mut forward = 0;

    for m in moves {
        let mut split = m.split_whitespace();
        let dir = split.next().unwrap();
        let val = split.next().unwrap().parse::<i64>().unwrap();

        match dir {
            "forward" => forward += val,
            "down" => depth += val,
            "up" => depth -= val,
            _ => panic!("Invalid input!")
        }
    }

    depth * forward
}

fn calc_position_p2(moves: &[&str]) -> i64 {
    let mut depth = 0;
    let mut forward = 0;
    let mut aim = 0;

    for m in moves {
        let mut split = m.split_whitespace();
        let dir = split.next().unwrap();
        let val = split.next().unwrap().parse::<i64>().unwrap();

        match dir {
            "forward" => {
                forward += val;
                depth += aim * val;
            },
            "down" => aim += val,
            "up" => aim -= val,
            _ => panic!("Invalid input!")
        }
    }

    depth * forward
}

fn main() {
    let sample_moves: Vec<&str> = SAMPLE.lines().collect();
    assert_eq!(calc_position(&sample_moves), 150);
    assert_eq!(calc_position_p2(&sample_moves), 900);

    let input_moves: Vec<&str> = INPUT.lines().collect();

    println!("Part 1: position product is {}", calc_position(&input_moves));
    println!("Part 2: position product is {}", calc_position_p2(&input_moves));
}
use std::collections::HashSet;
use std::fmt;

const INPUT: &str = include_str!("input.txt");
const SAMPLE: &str = include_str!("sample.txt");

#[derive(Clone)]
struct Grid {
    algo: Vec<char>,
    grid: HashSet<(i64, i64)>,
    outer_layer_lit: bool
}

impl fmt::Display for Grid {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let x_range = self.get_x_min()..=self.get_x_max();
        let y_range = self.get_y_min()..=self.get_y_max();

        for y in y_range {
            for x in x_range.clone() {
                if self.grid.contains(&(x, y)) {
                    write!(f, "#")?;
                } else {
                    write!(f, ".")?;
                }
            }
            writeln!(f)?;
        }
        Ok(())
    }
}

impl Grid {
    fn from_input(input: &str) -> Grid {
        let split = input.trim().split_once("\n\n").unwrap();
        let algo: Vec<char> = split.0.chars().collect();
        let input_grid = split.1;
        let mut grid = HashSet::new();
        for (y, line) in input_grid.lines().enumerate() {
            for (x, c) in line.chars().enumerate() {
                if c == '#' {
                    grid.insert((x as i64, y as i64));
                }
            }
        }

        println!("{:?}", grid);

        println!("Algo is {} chars long", algo.len());

        Grid { algo, grid, outer_layer_lit: false}
    }

    fn get_x_min(&self) -> i64 {
        self.grid.iter().min_by(|a, b| a.0.cmp(&b.0)).unwrap().0
    }

    fn get_x_max(&self) -> i64 {
        self.grid.iter().max_by(|a, b| a.0.cmp(&b.0)).unwrap().0
    }

    fn get_y_min(&self) -> i64 {
        self.grid.iter().min_by(|a, b| a.1.cmp(&b.1)).unwrap().1
    }

    fn get_y_max(&self) -> i64 {
        self.grid.iter().max_by(|a, b| a.1.cmp(&b.1)).unwrap().1
    }

    fn get_3x3_grid(&self, center: (i64, i64)) -> Vec<(i64, i64)> {
        // Return grid elements in normal reading order
        vec![
            (center.0 - 1, center.1 - 1),
            (center.0, center.1 - 1),
            (center.0 + 1, center.1 - 1),
            (center.0 - 1, center.1),
            center,
            (center.0 + 1, center.1),
            (center.0 - 1, center.1 + 1),
            (center.0, center.1 + 1),
            (center.0 + 1, center.1 + 1)
        ]
    }

    fn point_to_digit(&self, point: (i64, i64)) -> usize {
        let small_grid = self.get_3x3_grid(point);
        let min_x = self.get_x_min();
        let min_y = self.get_y_min();
        let max_x = self.get_x_max();
        let max_y = self.get_y_max();
        //println!("Point {:?} -> {:?}", point, small_grid);

        let mut bin_str = "".to_string();
        for p in small_grid.iter() {
            let in_bounds = p.0 >= min_x && p.0 <= max_x && p.1 <= max_y && p.1 >= min_y;
            if (in_bounds && self.grid.contains(p)) || (!in_bounds && self.outer_layer_lit) {
                //println!("Point {:?}: in bounds ({}) and 1", p, in_bounds);
                bin_str.push('1')
            } else {
                //println!("Point {:?}: in bounds ({}) and 0", p, in_bounds);
                bin_str.push('0')
            }
        }
        //println!("Bit str: {}", bin_str);
        usize::from_str_radix(&bin_str, 2).unwrap()
    }

    fn enhance_pixel(&self, point: (i64, i64)) -> bool {
        let digit = self.point_to_digit(point);
        let c = self.algo.get(digit).unwrap();
        //println!("{:?} -> {} -> {}\n\n", point, digit, c);
        c == &'#'
    }

    fn enhance_pixels(&self, steps: usize) -> Grid {
        let mut current_grid = self.clone();

        println!("Initial:\n{}", current_grid);
        for step in 0..steps {
            let mut next_grid = current_grid.clone();
            let x_range = (next_grid.get_x_min() - 1)..=(next_grid.get_x_max() + 1);
            let y_range = (next_grid.get_y_min() - 1)..=(next_grid.get_y_max() + 1);

            for x in x_range {
                for y in y_range.clone() {
                    let p = (x, y);
                    if current_grid.enhance_pixel(p) {
                        next_grid.grid.insert(p);
                    } else {
                        next_grid.grid.remove(&p);
                    }
                }
            }

            if self.need_to_toggle_outer_layer() {
                println!("Toggling outer layer");
                next_grid.outer_layer_lit = !next_grid.outer_layer_lit;
            }

            current_grid = next_grid.to_owned();
            println!("After {} steps:\n{}", step + 1, current_grid);
        }

        current_grid
    }

    fn need_to_toggle_outer_layer(&self) -> bool {
        self.algo.first().unwrap() == &'#'
    }

    fn get_num_lit(&self) -> usize {
        self.grid.len()
    }
}

fn main() {
    let sample = Grid::from_input(SAMPLE);
    assert_eq!(sample.enhance_pixels(2).get_num_lit(), 35);
    assert_eq!(sample.enhance_pixels(50).get_num_lit(), 3351);

    let input = Grid::from_input(INPUT);
    println!("After 2 steps, {} pixels are lit", input.enhance_pixels(2).get_num_lit());
    println!("After 50 steps, {} pixels are lit", input.enhance_pixels(50).get_num_lit());
}
use std::fmt;

const INPUT: &str = include_str!("input.txt");
const SAMPLE: &str = include_str!("sample.txt");

struct Grid {
    grid: Vec<Vec<u32>>,
    flash_count: u64
}

impl fmt::Display for Grid {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        for row in &self.grid {
            for d in row {
                write!(f, "{}", d)?;
            }
            writeln!(f)?;
        }
        Ok(())
    }
}

impl Grid {
    fn from_input(input: &str) -> Grid {
        Grid {
            grid: input.lines().map(|line| {
                line.chars().map(|c| c.to_digit(10).unwrap()).collect()
            }).collect::<Vec<Vec<u32>>>(),
            flash_count: 0
        }
    }

    fn x_max(&self) -> usize {
        self.grid[0].len()
    }

    fn y_max(&self) -> usize {
        self.grid.len()
    }

    fn get_adjacent_points(&self, point: (usize, usize)) -> Vec<(usize, usize)> {
        let mut adjacent_points = Vec::new();

        if point.0 > 0 {
            adjacent_points.push((point.0 - 1, point.1));
        }
        if point.0 < self.x_max() - 1 {
            adjacent_points.push((point.0 + 1, point.1));
        }
        if point.1 > 0 {
            adjacent_points.push((point.0, point.1 - 1));
        }
        if point.1 < self.y_max() - 1 {
            adjacent_points.push((point.0, point.1 + 1));
        }
        if point.0 > 0 && point.1 > 0 {
            adjacent_points.push((point.0 - 1, point.1 - 1));
        }
        if point.0 < self.x_max() - 1 && point.1 < self.y_max() - 1 {
            adjacent_points.push((point.0 + 1, point.1 + 1));
        }
        if point.0 > 0 && point.1 < self.y_max() - 1 {
            adjacent_points.push((point.0 - 1, point.1 + 1));
        }
        if point.0 < self.x_max() - 1 && point.1 > 0 {
            adjacent_points.push((point.0 + 1, point.1 - 1));
        }

        adjacent_points
    }

    fn step(&mut self) {
        // Increase the energy level of each octupus
        for row in self.grid.iter_mut() {
            for d in row.iter_mut() {
                *d += 1;
            }
        }

        loop {
            let mut blast_radius = Vec::new();

            for x in 0..self.x_max() {
                for y in 0..self.y_max() {
                    if self.grid[y][x] > 9 {
                        self.grid[y][x] = 0;
                        self.flash_count += 1;
                        blast_radius.append(&mut self.get_adjacent_points((x, y)));
                    }
                }
            }

            if blast_radius.is_empty() {
                break;
            }

            for point in blast_radius {
                let p = &mut self.grid[point.1][point.0];
                // Ignore points that have already flashed this round
                if *p != 0 { *p += 1; };
            }
        }
    }

    fn count_flashes_after_n_steps(&self, n_steps: usize) -> u64 {
        let mut mut_grid = Grid {
            grid: self.grid.clone(),
            flash_count: 0
        };

        println!("Before any steps:\n{}\n", mut_grid);

        for step in 0..n_steps {
            mut_grid.step();

            println!("After step {}:\n{}\n", step + 1, mut_grid);
        }

        mut_grid.flash_count
    }

    fn get_sync_step(&self) -> usize {
        let mut mut_grid = Grid {
            grid: self.grid.clone(),
            flash_count: 0
        };

        println!("Before any steps:\n{}", mut_grid);

        let num_octopuses: u64 = (mut_grid.x_max() * mut_grid.y_max()) as u64;
        let mut step = 0;
        loop {
            let pre_step_flash_count = mut_grid.flash_count;
            mut_grid.step();
            step += 1;
            let post_step_flash_count = mut_grid.flash_count;

            println!("After step {}:\n{}", step, mut_grid);

            // If all of the octopuses flashed, return the step number
            if (post_step_flash_count - pre_step_flash_count) == num_octopuses {
                return step;
            }

        }
    }
}

fn main() {
    let sample_grid = Grid::from_input(SAMPLE);
    assert_eq!(sample_grid.count_flashes_after_n_steps(100), 1656);
    assert_eq!(sample_grid.get_sync_step(), 195);

    let input_grid = Grid::from_input(INPUT);
    println!("After 100 steps, we see {} total flashes", input_grid.count_flashes_after_n_steps(100));
    println!("It takes {} steps for the dumbos to sync", input_grid.get_sync_step());
}
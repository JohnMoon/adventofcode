const INPUT: &str = include_str!("input.txt");
const SAMPLE: &str = include_str!("sample.txt");

struct Cups {
    circle: Vec<usize>,
    cur_cup_i: usize,
    dest_cup_i: usize
}

impl Cups {
    fn from_input(input: &str) -> Cups {
        let mut circle = Vec::new();
        let mut max_label = 0;
        for c in input.chars() {
            if let Some(label) = c.to_digit(10) {
                let ulabel = label as usize;
                if ulabel > max_label {
                    max_label = ulabel;
                }
                circle.push(ulabel);
            }
        }

        //for x in (max_label + 1)..=1_000_000 {
        //    circle.push(x);
        //}

        //assert_eq!(circle.len(), 1_000_000);

        Cups {
            circle,
            cur_cup_i: 0,
            dest_cup_i: 0
        }
    }

    fn pull_cups(&mut self) -> Vec<usize> {
        let mut ret = Vec::new();
        for i in 1..=3 {
            let i_mod = (self.cur_cup_i + i) % self.circle.len();
            ret.push(self.circle.remove(i_mod));
        }
        ret
    }

    fn play(&mut self, n_rounds: u64) {
        for _ in 0..n_rounds {
            self.play_round();
        }
    }

    fn play_round(&mut self) {
        //let orig_order = self.circle.clone();
        let orig_cur_label = self.circle[self.cur_cup_i];
        //print!("cups: ");
        //for (i, c) in orig_order.iter().enumerate() {
        //    if i == self.cur_cup_i {
        //        print!(" ({}) ", c);
        //    } else {
        //        print!(" {} ", c);
        //    }
        //}
        //println!();


        let pulled_cups = self.pull_cups();
        //print!("pick up: ");
        //for c in &pulled_cups {
        //    print!("{}, ", c);
        //}
        //println!();

        self.cur_cup_i = self.circle.iter().position(|&x| x == orig_cur_label).unwrap();
        let mut dest_label = self.circle[self.cur_cup_i];
        loop {
            dest_label -= 1;
            if dest_label < self.get_min_label() {
                dest_label = self.get_max_label();
            }

            if let Some(index) = self.circle.iter().position(|&x| x == dest_label) {
                self.dest_cup_i = index + 1;
                //println!("destination: {} ({})", dest_label, self.dest_cup_i);
                break;
            }
        }

        for (i, cup) in pulled_cups.iter().enumerate() {
            self.circle.insert(self.dest_cup_i + i, *cup);
        }

        self.cur_cup_i = self.circle.iter().position(|&x| x == orig_cur_label).unwrap();
        self.cur_cup_i = (self.cur_cup_i + 1) % self.circle.len();

        //println!("{:?} -> {:?}", orig_order, self.circle);
        //println!();
    }

    fn get_max_label(&self) -> usize {
        let mut ret = 0;
        for c in &self.circle {
            if *c > ret {
                ret = *c;
            }
        }
        ret
    }

    fn get_min_label(&self) -> usize {
        let mut ret = 10000000;
        for c in &self.circle {
            if *c < ret {
                ret = *c;
            }
        }
        ret
    }
}

fn main() {
    let mut s_cups = Cups::from_input(SAMPLE);
    s_cups.play(100);
    for cup in s_cups.circle {
        print!("{}", cup);
    }

    println!();

    let mut cups = Cups::from_input(INPUT);
    cups.play(100);
    for cup in cups.circle {
        print!("{}", cup);
    }
    println!();
}

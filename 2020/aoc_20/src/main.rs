const INPUT: &str = include_str!("input.txt");
const SAMPLE: &str = include_str!("sample.txt");
const DIM: usize = 10;

use std::collections::HashMap;

#[derive (Debug, Clone, Copy)]
struct Tile {
    id: u64,
    data: [[bool; DIM]; DIM]
}

impl Tile {
    fn new() -> Tile {
        Tile {
            id: 0,
            data: [[false; DIM]; DIM]
        }
    }

    fn get_edges(&self) -> [[bool; DIM]; 4] {
        let mut ret = [[false; DIM]; 4];
        ret[0] = self.data[0];
        ret[1] = self.data[DIM - 1];
        for i in 0..DIM {
            ret[2][i] = self.data[i][0];
            ret[3][i] = self.data[i][DIM - 1];
        }

        //println!("Tile {} has these edges:", self.id);
        //for i in 0..4 {
        //    for j in 0..DIM {
        //        if ret[i][j] {
        //            print!("#");
        //        } else {
        //            print!(".");
        //        }
        //    }
        //    println!()
        //}
        ret
    }
}

#[derive (Debug, Clone)]
struct CamArray {
    arr: Vec<Vec<Tile>>,
    arr_dim: usize
}

impl CamArray {
    fn from_input(input: &str) -> CamArray {
        let num_tiles = input.lines().filter(|s| s.starts_with("Tile")).count();
        let arr_dimension: usize = (num_tiles as f64).sqrt() as usize;

        let mut arr: Vec<Vec<Tile>> = Vec::new();
        let mut arr_row: Vec<Tile> = Vec::new();
        let mut tile = Tile::new();
        let mut row_ctr = 0;
        for line in input.lines() {
            if line.starts_with("Tile") {
                tile = Tile::new();
                row_ctr = 0;
                tile.id = line.split_whitespace().collect::<Vec<&str>>()[1].replace(":", "").parse::<u64>().unwrap();
                continue;
            }

            if arr_row.len() == arr_dimension {
                arr.push(arr_row.clone());
                arr_row.clear();
            }

            if line.is_empty() {
                arr_row.push(tile);
                continue;
            }

            for (i, c) in line.chars().enumerate() {
                tile.data[row_ctr][i] = c == '#';
            }

            row_ctr += 1;
        }
        arr.push(arr_row);

        println!("Working with a {}x{} camera tile array", arr[0].len(), arr.len());

        CamArray {
            arr,
            arr_dim: arr_dimension
        }
    }

    fn get_corner_pieces(&self) -> Vec<u64> {
        let mut corners = Vec::new();

        let mut edges: Vec<(u64, [[bool; DIM]; 4])> = Vec::new();
        for arr_row in 0..self.arr_dim {
            for arr_col in 0..self.arr_dim {
                let t = self.arr[arr_row][arr_col];
                edges.push((t.id, t.get_edges()));
            }
        }

        //println!("edges.len(): {}", edges.len());

        let mut common_counter: HashMap<u64, u64> = HashMap::new();
        for e in &edges {
            common_counter.insert(e.0, 0);
        }

        let mut found_common = false;
        for tile_i in 0..edges.len() {
            for tile_j in (tile_i + 1)..edges.len() {

                for tile_i_edge in edges[tile_i].1.iter() {
                    for tile_j_edge in edges[tile_j].1.iter() {
                        let mut rev_tile_j_edge = *tile_j_edge;
                        rev_tile_j_edge.reverse();
                        if tile_i_edge == tile_j_edge || *tile_i_edge == rev_tile_j_edge {
                            let i_ctr = common_counter.get_mut(&edges[tile_i].0).unwrap();
                            *i_ctr += 1;

                            let j_ctr = common_counter.get_mut(&edges[tile_j].0).unwrap();
                            *j_ctr += 1;

                            found_common = true;
                            break;
                        }
                    }
                    if found_common {
                        found_common = false;
                        break;
                    }
                }

            }
        }

        for (k, v) in common_counter {
            if v == 2 {
                corners.push(k);
            }
        }

        assert_eq!(corners.len(), 4);

        corners
    }
}

fn main() {
    let s_cam_arr = CamArray::from_input(SAMPLE);
    let s_corners = s_cam_arr.get_corner_pieces();
    let mut s_prod = 1;
    for c in s_corners {
        s_prod *= c;
    }
    assert_eq!(s_prod, 20899048083289);

    let cam_arr = CamArray::from_input(INPUT);
    let corners = cam_arr.get_corner_pieces();
    let mut prod = 1;
    for c in corners {
        prod *= c;
        println!("Corner ID: {}", c);
    }
    println!("P1 - multiplying the corner tile IDs yields: {}", prod);
}

const INPUT: &str = include_str!("input.txt");

struct PwNode {
    letter: char,
    n0: usize,
    n1: usize,
    password: String
}

impl PwNode {
    fn from_string(input_str: &str) -> PwNode {
        let tokens: Vec<&str> = input_str.split_whitespace().collect();
        let minmax: Vec<&str> = tokens[0].split('-').collect(); // "1-3"
        let n0 = minmax[0].parse::<usize>().unwrap();
        let n1 = minmax[1].parse::<usize>().unwrap();

        let letter = tokens[1].chars().next().unwrap(); // "a:"

        let password = tokens[2].to_string(); // "asdf"

        PwNode {
            letter,
            n0,
            n1,
            password
        }
    }

    fn is_valid_p1(&self) -> bool {
        let letter_count = self.password.matches(self.letter).count();
        letter_count >= self.n0 && letter_count <= self.n1
    }

    fn is_valid_p2(&self) -> bool {
        let chars: Vec<char> = self.password.chars().collect();
        let c1 = chars.get(self.n0 - 1).unwrap_or(&' ');
        let c2 = chars.get(self.n1 - 1).unwrap_or(&' ');

        // Logical XOR
        (*c1 == self.letter) ^ (*c2 == self.letter)
    }
}

fn main() {
    let input: Vec<PwNode> = INPUT.lines().map(|i| {
        PwNode::from_string(i)
    }).collect();

    let valid_count_p1 = input.iter().filter(|node| {
        node.is_valid_p1()
    }).count();

    println!("There are {} valid passwords (for part 1 criteria)", valid_count_p1);

    let valid_count_p2 = input.iter().filter(|node| {
        node.is_valid_p2()
    }).count();

    println!("There are {} valid passwords (for part 2 criteria)", valid_count_p2);
}

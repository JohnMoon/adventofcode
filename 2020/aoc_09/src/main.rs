const INPUT: &str = include_str!("input.txt");
const SAMPLE: &str = include_str!("sample.txt");

struct XMAS {
    stream: Vec<u64>,
    p_len: usize
}


impl XMAS {
    fn first_invalid_val(&self) -> u64 {
        let mut shift = 0;
        loop {
            let preamble: Vec<u64> = self.stream.iter()
                .skip(shift)
                .take(self.p_len)
                .copied()
                .collect();
            let val = self.stream.get(self.p_len + shift).unwrap();

            if ! XMAS::val_is_valid(&preamble, *val) {
                return *val;
            }

            shift += 1;
        }
    }

    fn val_is_valid(preamble: &[u64], value: u64) -> bool {
        for i in 0..preamble.len() {
            for j in (i + 1)..preamble.len() {
                if preamble[i] + preamble[j] == value {
                    return true;
                }
            }
        }
        false
    }

    fn find_weakness(&self, invalid_n: u64) -> u64 {
        let mut shift = 0;
        let mut chunk_size = 2;
        loop {
            let chunk: Vec<u64> = self.stream.iter()
                .skip(shift)
                .take(chunk_size)
                .copied()
                .collect();

            let sum: u64 = chunk.iter().sum();

            if sum == invalid_n {
                return chunk.iter().max().unwrap() + chunk.iter().min().unwrap();
            }

            chunk_size += 1;

            if sum > invalid_n || chunk_size + shift > self.stream.len() {
                shift += 1;
                chunk_size = 2;
            }
        }
    }
}

fn main() {
    let xmas_s = XMAS {
        stream: SAMPLE.lines().map(|l| l.parse::<u64>().unwrap()).collect(),
        p_len: 5
    };

    assert_eq!(xmas_s.first_invalid_val(), 127);
    assert_eq!(xmas_s.find_weakness(127), 62);

    let xmas = XMAS {
        stream: INPUT.lines().map(|l| l.parse::<u64>().unwrap()).collect(),
        p_len: 25
    };

    let first_invalid = xmas.first_invalid_val();
    println!("The first invalid value is {}", first_invalid);
    println!("The weakness is is {}", xmas.find_weakness(first_invalid));
}

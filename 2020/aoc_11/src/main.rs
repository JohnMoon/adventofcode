const INPUT: &str = include_str!("input.txt");
const SAMPLE: &str = include_str!("sample.txt");

#[derive (Eq, PartialEq, Clone, Copy)]
struct Seat {
    occupied: Option<bool>
}

impl Seat {
    fn from_char(s_char: char) -> Seat {
        match s_char {
            'L' => Seat { occupied: Some(false) },
            '#' => Seat { occupied: Some(true) },
            '.' => Seat { occupied: None },
            _ => panic!("Invalid char!")
        }
    }

    fn is_occupied(&self) -> bool {
        if let Some(o) = self.occupied {
            return o;
        }
        false
    }

    fn is_floor(&self) -> bool {
        self.occupied.is_none()
    }
}

#[derive (Eq, PartialEq, Clone)]
struct Seatmap {
    map: Vec<Vec<Seat>>,

}

impl Seatmap {
    fn from_input(input: &str) -> Seatmap {
        let mut ret = Vec::new();
        for line in input.lines() {
            let mut row = Vec::new();
            for seat in line.chars() {
                row.push(Seat::from_char(seat));
            }
            ret.push(row);
        }
        Seatmap {
            map: ret
        }
    }

    fn row_count(&self) -> usize {
        self.map.len()
    }

    fn col_count(&self) -> usize {
        self.map[0].len()
    }

    fn run_to_steady_state(&mut self) {
        loop {
            let orig = self.clone();
            self.run_round();
            if *self == orig {
                break;
            }
        }
    }

    fn run_to_steady_state_p2(&mut self) {
        loop {
            let orig = self.clone();
            self.run_round_p2();
            if *self == orig {
                break;
            }
        }
    }

    fn num_occupied_adj_seats(&self, i_row: usize, i_col: usize) -> usize {
        let mut sum = 0;

        let min_row = if i_row > 1 { i_row - 1 } else { 0 };
        let max_row = if i_row == self.row_count() - 1 { i_row } else { i_row + 1 };

        let min_col = if i_col > 1 { i_col - 1 } else { 0 };
        let max_col = if i_col == self.col_count() - 1 { i_col } else { i_col + 1 };

        for row in min_row..=max_row {
            for col in min_col..=max_col {
                if row == i_row && col == i_col {
                    continue;
                }

                if self.map[row][col].is_occupied() {
                    sum += 1;
                }
            }
        }

        sum
    }

    fn num_vis_occupied(&self, i_row: usize, i_col: usize) -> usize {
        let mut sum = 0;

        //O 1 2
        //3 X 4
        //5 6 7
        let mut blocked = vec![false, false, false, false, false, false, false, false];
        
        let mut rad = 0;

        loop {
            rad += 1;
            let min_row = if i_row > rad { i_row - rad } else { 0 };
            let max_row = if i_row + rad < self.row_count() { i_row + rad } else { self.row_count() - 1 };

            let min_col = if i_col > rad { i_col - rad } else { 0 };
            let max_col = if i_col + rad < self.col_count() { i_col + rad } else { self.col_count() - 1 };

            for row in min_row..=max_row {
                for col in min_col..=max_col {
                    if (row != i_row && col != i_col) && ((i_row as i32 - row as i32).abs() != (i_col as i32 - col as i32).abs()) {
                        //println!("{},{}: nah 1", row, col);
                        continue;
                    }

                    if (row == i_row && col == i_col) || self.map[row][col].is_floor() {
                        //println!("{},{}: nah 2", row, col);
                        continue;
                    }

                    if row < i_row {
                        if col < i_col {
                            if blocked[0] { continue };
                            blocked[0] = true;
                        } else if col == i_col {
                            if blocked[1] { continue };
                            blocked[1] = true;
                        } else {
                            if blocked[2] { continue };
                            blocked[2] = true;
                        }
                    } else if row == i_row {
                        if col < i_col {
                            if blocked[3] { continue };
                            blocked[3] = true;
                        } else {
                            if blocked[4] { continue };
                            blocked[4] = true;
                        }
                    } else if col < i_col {
                        if blocked[5] { continue };
                        blocked[5] = true;
                    } else if col == i_col {
                        if blocked[6] { continue };
                        blocked[6] = true;
                    } else {
                        if blocked[7] { continue };
                        blocked[7] = true;
                    }

                    if self.map[row][col].is_occupied() {
                        sum += 1;
                    }
                }
                if ! blocked.iter().any(|&x| !x) {
                    break;
                }
            }
            if min_row == 0 && max_row == self.row_count() - 1 && min_col == 0 && max_col == self.col_count() - 1 {
                break;
            }
        }

        sum
    }



    fn run_round(&mut self) {
        let start_map = self.clone();

        for row in 0..self.row_count() {
            for col in 0..self.col_count() {
                let target_seat = start_map.map[row][col];
                if target_seat.is_floor() {
                    continue;
                }

                let num_adjacent_occupied = start_map.num_occupied_adj_seats(row, col);

                if target_seat.is_occupied() {
                    // If a seat is occupied and 4 or more adjacent seats are occupied,
                    // leave the seat
                    if num_adjacent_occupied >= 4 {
                        self.map[row][col] = Seat { occupied: Some(false) };
                    }

                } else {
                    // If a seat is empty and no seats adjacent are occupied, take the seat
                    if num_adjacent_occupied == 0 {
                        self.map[row][col] = Seat { occupied: Some(true) };
                    }
                }
            }
        }
    }

    fn run_round_p2(&mut self) {
        let start_map = self.clone();

        for row in 0..self.row_count() {
            for col in 0..self.col_count() {
                let target_seat = start_map.map[row][col];
                if target_seat.is_floor() {
                    continue;
                }

                let num_vis_occupied = start_map.num_vis_occupied(row, col);

                if target_seat.is_occupied() {
                    // If a seat is occupied and 5 or more adjacent seats are occupied,
                    // leave the seat
                    if num_vis_occupied >= 5 {
                        self.map[row][col] = Seat { occupied: Some(false) };
                    }

                } else {
                    // If a seat is empty and no seats adjacent are occupied, take the seat
                    if num_vis_occupied == 0 {
                        self.map[row][col] = Seat { occupied: Some(true) };
                    }
                }
            }
        }
    }

    fn num_occupied(&self) -> usize {
        let mut ret = 0;
        for row in &self.map {
            for s in row {
                if s.is_occupied() {
                    ret += 1;
                }
            }
        }
        ret
    }
}

fn main() {
    let mut sample_seatmap = Seatmap::from_input(SAMPLE);
    sample_seatmap.run_to_steady_state();
    assert_eq!(sample_seatmap.num_occupied(), 37);

    sample_seatmap = Seatmap::from_input(SAMPLE);
    sample_seatmap.run_to_steady_state_p2();
    assert_eq!(sample_seatmap.num_occupied(), 26);

    let mut seatmap = Seatmap::from_input(INPUT);
    seatmap.run_to_steady_state();
    println!("At steady state, {} seats are occupied (P1)", seatmap.num_occupied());

    let mut seatmap = Seatmap::from_input(INPUT);
    seatmap.run_to_steady_state_p2();
    println!("At steady state, {} seats are occupied (P2)", seatmap.num_occupied());
}

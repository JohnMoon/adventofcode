const INPUT: &str = include_str!("input.txt");

#[derive (Debug,Clone)]
struct Bag {
    color: String,
    contains: Vec<(usize, String)>
}

impl PartialEq for Bag {
    fn eq(&self, other: &Self) -> bool {
        self.color == other.color
    }
}

impl Bag {
    fn from_string(input: &str) -> Bag {
        let tokens: Vec<&str> = input.split_whitespace().collect();
        let color = format!("{} {}", tokens[0], tokens[1]);

        let mut contains: Vec<(usize, String)> = Vec::new();
        let mut tok_iter = tokens.iter().skip(4);
        while let Some(tok) = tok_iter.next() {
            // If the token is a number, the next two tokens are the color
            if tok.trim().parse::<usize>().is_ok() {
                let count = tok.trim().parse::<usize>().unwrap();
                let color = format!("{} {}", tok_iter.next().unwrap(), tok_iter.next().unwrap());
                contains.push((count, color));
            }
        }

        Bag {
            color,
            contains
        }
    }

    fn contains(&self, color: &str) -> bool {
        self.contains.iter().any(|x| x.1 == color)
    }
}


#[derive (Debug)]
struct BagSystem {
    bags: Vec<Bag>,
}

impl BagSystem {
    fn from_input(input: &str) -> BagSystem {
        BagSystem {
            bags: input.lines().map(|l| Bag::from_string(l)).collect()
        }
    }

    fn can_directly_contain(&self, input_color: String) -> Vec<Bag> {
        let mut can_dir_contain = Vec::new();
        for bag in self.bags.iter().filter(|b| b.contains(&input_color)) {
            can_dir_contain.push(bag.clone());
        }
        can_dir_contain
    }

    fn can_contain(&self, input_color: String) -> Vec<Bag> {
        let mut can_dir_contain = self.can_directly_contain(input_color);
        let mut can_indir_contain = Vec::new();
        for bag in &can_dir_contain {
            can_indir_contain.append(&mut self.can_contain(bag.color.clone()));
        }

        can_dir_contain.append(&mut can_indir_contain);
        can_dir_contain.sort_by_key(|k| k.color.clone());
        can_dir_contain.dedup();
        can_dir_contain
    }

    fn must_contain(&self, input_color: String) -> usize {
        let root = self.bags.iter().find(|b| b.color == input_color).unwrap();

        let mut sum = 1;
        for sub_bags in &root.contains {
            sum += sub_bags.0 * self.must_contain(sub_bags.1.clone());
        }

        sum
    }
}


fn main() {
    let system = BagSystem::from_input(INPUT);

    println!("Shiny gold can be contained by {} bags",
             system.can_contain("shiny gold".to_string()).len());

    println!("Shiny gold must contain {} other bags",
             system.must_contain("shiny gold".to_string()) - 1);
}

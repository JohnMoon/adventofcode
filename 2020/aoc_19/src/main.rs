const INPUT: &str = include_str!("input.txt");
const SAMPLE: &str = include_str!("sample.txt");

use std::collections::HashMap;

#[derive (Debug, Clone)]
struct Rules {
    rules: HashMap<u64, Vec<Vec<u64>>>,
    terminals: HashMap<u64, Vec<String>>
}

#[derive (Debug, Clone)]
struct Collapsed {
    rules: HashMap<u64, Vec<String>>,
}

impl Rules {
    fn from_input(input: &str) -> Rules {
        let mut rules: HashMap<u64, Vec<Vec<u64>>> = HashMap::new();
        let mut terminals = HashMap::new();
        let mut found_terminal = false;
        for line in input.lines() {
            if line.is_empty() {
                break;
            }

            let rule_n = line.split(':').collect::<Vec<&str>>()[0].parse::<u64>().unwrap();
            let mut rule_chain: Vec<u64> = Vec::new();

            for s in line.split_whitespace().skip(1) {

                if s == "|" {
                    if rules.contains_key(&rule_n) {
                        rules.get_mut(&rule_n).unwrap().push(rule_chain.clone());
                    } else {
                        rules.insert(rule_n, vec![rule_chain.clone()]);
                    }
                    rule_chain.clear();
                    continue;
                }

                let s_trimmed = s.replace("\"", "");

                if s_trimmed.chars().next().unwrap().is_alphabetic() {
                    terminals.insert(rule_n, vec![s_trimmed]);
                    found_terminal = true;
                    break;
                }

                rule_chain.push(s_trimmed.parse::<u64>().unwrap());
            }

            if found_terminal {
                found_terminal = false;
                continue;
            }

            if rules.contains_key(&rule_n) {
                rules.get_mut(&rule_n).unwrap().push(rule_chain);
            } else {
                rules.insert(rule_n, vec![rule_chain]);
            }
        }

        Rules {
            rules,
            terminals
        }
    }

    fn get_collapsed(&self) -> Collapsed {
        let mut rules = self.terminals.clone();

        for k in self.terminals.keys() {
            for rule in self.rules {
            }
        }

        Collapsed {
            rules
        }
    }
}

fn main() {
    let rules = Rules::from_input(SAMPLE);
    println!("{:?}", rules);
}

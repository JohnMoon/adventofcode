const INPUT: &str = include_str!("input.txt");
const SAMPLE: &str = include_str!("sample.txt");

use std::collections::HashMap;

struct Ticket {
    fields: HashMap<String, Vec<(u64, u64)>>
}

impl Ticket {
    fn from_input(input: &str) -> Ticket {
        // Process just the field validity
        let mut fields = HashMap::new();
        for line in input.lines() {
            if line.is_empty() {
                break;
            }

            let key = line.split(":").collect::<Vec<&str>>()[0];
            let mut ranges = Vec::new();
            for chunk in line.split_whitespace() {
                if chunk.chars().any(char::is_numeric) {
                    let chunk_split = chunk.split('-').collect::<Vec<&str>>();
                    let n1 = chunk_split[0].parse::<u64>().unwrap();
                    let n2 = chunk_split[1].parse::<u64>().unwrap();
                    ranges.push((n1, n2));
                }
            }

            fields.insert(key.to_string(), ranges);
        }

        Ticket {
            fields
        }
    }

    fn get_fitting_fields(&self, value: u64) -> HashMap<String, Vec<(u64, u64)>> {
        let mut fitting_fields = self.fields.clone();
        fitting_fields.retain(|_, v| {
            for (start, stop) in v {
                if value >= *start && value <= *stop {
                    return true;
                }
            }
            return false;
        });
        fitting_fields
    }

    fn ticket_is_valid(&self, ticket: &str) -> bool {
        for val_str in ticket.split(',') {
            let val = val_str.parse::<u64>().unwrap();
            let fitting_fields = self.get_fitting_fields(val);
            if fitting_fields.is_empty() {
                return false;
            }
        }

        true
    }

    fn invalid_ticket_values(&self, ticket: &str) -> Vec<u64> {
        let mut bad_vals = Vec::new();
        for val_str in ticket.split(',') {
            let val = val_str.parse::<u64>().unwrap();
            let fitting_fields = self.get_fitting_fields(val);
            if fitting_fields.is_empty() {
                bad_vals.push(val);
            }
        }
        bad_vals
    }
}

fn get_nearby_tickets(input: &str) -> Vec<&str> {
    let mut ret = Vec::new();
    let mut nearby_section = false;
    for line in input.lines() {
        if line.starts_with("nearby") {
            nearby_section = true;
            continue;
        }

        if ! nearby_section {
            continue;
        }
        ret.push(line);
    }
    ret
}

fn get_my_ticket(input: &str) -> &str {
    let mut ret = "";
    let mut my_section = false;
    for line in input.lines() {
        if line.starts_with("your") {
            my_section = true;
            continue;
        }

        if ! my_section {
            continue;
        }

        ret = line;
        break;
    }
    ret
}

fn main() {
    let sample_ticket = Ticket::from_input(SAMPLE);

    let mut sample_bad_ticket_vals: Vec<u64> = Vec::new();
    for t in get_nearby_tickets(SAMPLE) {
        let bad_vals = sample_ticket.invalid_ticket_values(t);
        for v in bad_vals {
            sample_bad_ticket_vals.push(v);
        }
    }

    assert_eq!(sample_bad_ticket_vals.iter().sum::<u64>(), 71);

    let ticket = Ticket::from_input(INPUT);

    let mut bad_ticket_vals: Vec<u64> = Vec::new();
    for t in get_nearby_tickets(INPUT) {
        let bad_vals = ticket.invalid_ticket_values(t);
        for v in bad_vals {
            bad_ticket_vals.push(v);
        }
    }

    println!("Sum of the bad ticket values: {}", bad_ticket_vals.iter().sum::<u64>());

    let mut valid_tickets = Vec::new();
    let my_ticket = get_my_ticket(INPUT);
    if ticket.ticket_is_valid(my_ticket) {
        println!("My ticket is valid!");
        valid_tickets.push(my_ticket);
    }

    for t in get_nearby_tickets(INPUT) {
        if ticket.ticket_is_valid(t) {
            valid_tickets.push(t);
        }
    }
}

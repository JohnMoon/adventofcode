const INPUT: &str = include_str!("input.txt");

#[derive (Debug)]
struct BoardingPass {
    row: usize,
    col: usize,
    seat_id: usize
}

impl BoardingPass {
    fn from_string(input: &str) -> BoardingPass {
        let mut min_row = 0;
        let mut max_row = 127;
        let mut min_col = 0;
        let mut max_col = 7;

        for c in input.chars().enumerate() {
            if c.0 < 7 {
                let step = (max_row - min_row) / 2;
                if c.1 == 'F' {
                    max_row -= step + 1;
                } else if c.1 == 'B' {
                    min_row += step + 1;
                } else {
                    panic!("Bad string input");
                }
            } else {
                let step = (max_col - min_col) / 2;
                if c.1 == 'L' {
                    max_col -= step + 1;
                } else if c.1 == 'R' {
                    min_col += step + 1;
                } else {
                    panic!("Bad string input");
                }
            }
        }

        // Sanity check
        assert!(min_row == max_row && min_col == max_col);

        let seat_id = min_row * 8 + min_col;

        BoardingPass {
            row: min_row,
            col: min_col,
            seat_id
        }
    }
}

fn main() {
    let passes: Vec<BoardingPass> = INPUT.lines()
        .map(|l| BoardingPass::from_string(l)).collect();

    let max_seat_id = passes.iter().max_by_key(|p| p.seat_id).unwrap().seat_id;

    println!("Largest seat ID is {}", max_seat_id);

    // Find any empty seats
    let mut empty_seats = Vec::new();
    for row in 1..127 {
        for col in 0..=7 {
            if ! passes.iter().any(|p| p.row == row && p.col == col) {
                let seat_id = row * 8 + col;
                empty_seats.push(BoardingPass {
                    row,
                    col,
                    seat_id
                });
            }
        }
    }

    // Find the one empty seat with ID +1 and -1 _not_ empty
    let mut my_seat_id = 0;
    let mut last_id = empty_seats.first().unwrap().seat_id;
    for seat in empty_seats.iter().skip(1) {
        if seat.seat_id != last_id + 1 {
            my_seat_id = seat.seat_id;
            break;
        }
        last_id = seat.seat_id;
    }

    println!("My seat ID is {}", my_seat_id);
}

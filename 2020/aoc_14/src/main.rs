const INPUT: &str = include_str!("input.txt");
const SAMPLE: &str = include_str!("sample.txt");
const SAMPLE2: &str = include_str!("sample2.txt");
const MASK_36: u64 = 0xF_FFFF_FFFF;

use std::collections::HashMap;

struct Mem {
    mem: HashMap<usize, u64>
}

impl Mem {
    fn from_input(input: &str) -> Mem {
        let mut cmask = 0; // Actual bitmask
        let mut cmask_val = 0; // Value to mask in
        let mut mem = HashMap::new();
        for line in input.lines() {
            if line.starts_with("mask") {
                let mask_str = line.split_whitespace().collect::<Vec<&str>>()[2];
                cmask = u64::from_str_radix(&mask_str.replace("1", "0").replace("X", "1"), 2).unwrap();
                cmask_val = u64::from_str_radix(&mask_str.replace("X", "0"), 2).unwrap();
                continue;
            }

            let addr = line.split('[').collect::<Vec<&str>>()[1].split(']').collect::<Vec<&str>>()[0].parse::<usize>().unwrap();
            let value = line.split_whitespace().collect::<Vec<&str>>()[2].parse::<u64>().unwrap();

            let masked_val = ((value & cmask) + cmask_val) & MASK_36;

            mem.insert(addr, masked_val);
        }

        Mem {
            mem
        }
    }

    fn from_input_p2(input: &str) -> Mem {
        let mut addr_mask = 0; // All X's are 0's
        let mut floating_mask = 0; // All X's are 1
        let mut floating_masks = Vec::new();
        let mut mem = HashMap::new();
        for line in input.lines() {
            if line.starts_with("mask") {
                let mask_str = line.split_whitespace().collect::<Vec<&str>>()[2];
                addr_mask = u64::from_str_radix(&mask_str.replace("X", "0"), 2).unwrap();
                floating_mask = u64::from_str_radix(&mask_str.replace("1", "0").replace("X", "1"), 2).unwrap();
                let mut orig_floating_bits = Vec::new();
                for i in 0..=36 {
                    if ((1 << i) & floating_mask) > 0 {
                        orig_floating_bits.push(1 << i);
                    }
                }

                let bit_count = orig_floating_bits.len();
                floating_masks = get_mask_permutations(orig_floating_bits);
                floating_masks.push(0);

                assert_eq!(floating_masks.len(), 2usize.pow(bit_count as u32));

                continue;
            }

            let orig_addr = line.split('[').collect::<Vec<&str>>()[1].split(']').collect::<Vec<&str>>()[0].parse::<u64>().unwrap();
            let value = line.split_whitespace().collect::<Vec<&str>>()[2].parse::<u64>().unwrap();

            for f in &floating_masks {
                let addr = (((orig_addr & !floating_mask) | addr_mask) + f) & MASK_36;
                mem.insert(addr as usize, value);
            }
        }

        Mem {
            mem
        }
    }


    fn mem_sum(&self) -> u64 {
        let mut sum = 0;
        for v in self.mem.values() {
            sum += v;
        }
        sum
    }
}

fn get_mask_permutations(floating: Vec<u64>) -> Vec<u64> {
    let mut ret = floating.clone();
    for i in 0..floating.len() {
        for j in i..floating.len() {
            let val = floating[i] | floating[j];
            if ! ret.contains(&val) {
                ret.push(val);
            }
        }
    }

    if ret.len() > floating.len() {
        get_mask_permutations(ret)
    } else {
        ret
    }
}

fn main() {
    let sample_mem = Mem::from_input(SAMPLE);
    assert_eq!(sample_mem.mem_sum(), 165);

    let sample_mem = Mem::from_input_p2(SAMPLE2);
    assert_eq!(sample_mem.mem_sum(), 208);

    let mem = Mem::from_input(INPUT);
    println!("P1 memory sum: {}", mem.mem_sum());

    let mem = Mem::from_input_p2(INPUT);
    println!("P2 memory sum: {}", mem.mem_sum());
}

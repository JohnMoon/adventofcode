const INPUT: &str = include_str!("input.txt");

struct Forest {
    tree_matrix: Vec<Vec<bool>>,
}

impl Forest {
    fn from_file(path: &str) -> Forest {
        let input: Vec<&str> = path.lines().collect();
        let mut tree_matrix = Vec::new();
        for line in input {
            let mut tree_row = Vec::new();
            for point in line.chars() {
                tree_row.push(point == '#');
            }
            tree_matrix.push(tree_row);
        }
        Forest {
            tree_matrix
        }
    }

    fn contains_tree_at(&self, x: usize, y: usize) -> Option<&bool> {
        // The forest extends to the right forever,
        // so we can just take the modulus of the input x
        // to find the actual position.
        let mod_x = x % self.tree_matrix[0].len();

        let row = self.tree_matrix.get(y)?;
        row.get(mod_x)
    }
}


fn main() {
    let forest = Forest::from_file(INPUT);

    let slopes_to_try = vec![(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)];
    let mut tree_ctr_product = 1;

    for slope in slopes_to_try {
        let mut x: usize = 0;
        let mut y: usize = 0;

        let mut tree_ctr = 0;
        while let Some(hit) = forest.contains_tree_at(x, y) {
            if *hit {
                tree_ctr += 1;
            }

            x += slope.0;
            y += slope.1;
        }

        println!("With slope {}, {}, we hit {} trees!", slope.0, slope.1, tree_ctr);
        tree_ctr_product *= tree_ctr;
    }
    println!("The product of all the slope tree hits is {}!", tree_ctr_product);
}

const INPUT: &str = include_str!("input.txt");

#[derive (Debug, Clone)]
struct Op {
    op: String,
    arg: i64
}

struct HGC {
    acc: i64,
    pc: usize,
    program: Vec<Op>
}

impl HGC {
    fn new(input: &str) -> HGC {
        let mut program = Vec::new();
        for line in input.lines() {
            let tokens: Vec<&str> = line.split_whitespace().collect();
            program.push(Op {
                op: tokens[0].to_string(),
                arg: tokens[1].parse::<i64>().unwrap()
            });
        }

        HGC {
            acc: 0,
            pc: 0,
            program
        }
    }

    fn mod_until_termination_acc(&mut self) -> i64 {
        let mut mod_index = 0;
        loop {
            let orig_op = self.program[mod_index].clone();

            if orig_op.op == *"acc" {
                mod_index += 1;
                continue;
            }

            let new_op = Op {
                op: if orig_op.op == *"nop" { "jmp".to_string() } else { "nop".to_string() },
                arg: orig_op.arg
            };

            self.acc = 0;
            self.pc = 0;
            self.program[mod_index] = new_op;

            if self.run_terminates() {
                break;
            } else {
                // Restore original op
                self.program[mod_index] = orig_op;
                mod_index += 1;
            }
        }
        self.acc
    }

    fn run_terminates(&mut self) -> bool {
        let mut visited_ops = vec![self.pc];
        while self.step() {
            if visited_ops.contains(&self.pc) {
                return false;
            }
            visited_ops.push(self.pc);
        }
        true
    }

    fn get_pre_loop_acc_val(&mut self) -> i64 {
        let mut visited_ops = vec![self.pc];
        let mut pre_step_acc = self.acc;
        while self.step() {
            if visited_ops.contains(&self.pc) {
                break;
            } else {
                visited_ops.push(self.pc);
                pre_step_acc = self.acc;
            }
        }
        pre_step_acc
    }

    fn step(&mut self) -> bool {
        if let Some(op) = self.program.clone().get(self.pc) {
            match op.op.as_str() {
                "acc" => self.acc(op.arg),
                "jmp" => self.jmp(op.arg),
                "nop" => self.nop(),
                _ => panic!("Invalid instruction")
            }
            return true;
        }
        false
    }

    fn acc(&mut self, arg: i64) {
        self.acc += arg;
        self.pc += 1;
    }

    fn jmp(&mut self, arg: i64) {
        self.pc = ((self.pc as i64) + arg) as usize;
    }

    fn nop(&mut self) {
        self.pc += 1;
    }

}

fn main() {
    let mut hgc = HGC::new(INPUT);

    println!("The acc value before loop is {}", hgc.get_pre_loop_acc_val());
    println!("After termination, the acc value is {}", hgc.mod_until_termination_acc());
}

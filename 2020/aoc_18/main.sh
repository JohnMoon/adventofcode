#!/bin/bash
set -o errexit

main_p1() {
    groups=./groups
    input="$1"
    work="${input}.work"
    cp "$input" "$work"

	while true; do
		# Get all the lowest-nested groups for eval
		if ! grep -n -E -o '\([0-9*+ ]+\)' "$work" > "$groups"; then
			# If no parens groups, get just lines with operations
			if ! grep -n '[*+]' "$work" > "$groups"; then
				printf "All lines evaluated!\n"
				break
			fi
		fi

		# Evaluate them
		while read -r line; do
            line_n=$(echo "$line" | cut -d ':' -f 1)
            line=$(echo "$line" | cut -d ':' -f 2)

			n_tok=$(echo "$line" | sed 's/[^+*]//g' | awk '{ print length }')
			if [ "$n_tok" -eq 0 ]; then
				printf "error\n"
				return 1
			fi

			p_line=$(printf "%s%s" "$(printf "%0.s(" $(seq 1 "$n_tok"))" "$line" | sed -e 's/[*+] [0-9]*/&\)/g')
			val=$((p_line))
			esc_line=$(printf '%s' "$line" | sed -e 's/[]\/$*.^[]/\\&/g')
			sed -i -e "${line_n}s/$esc_line/$val/" "$work"
		done < "$groups"
	done

	sum=0
	while read -r val; do
		sum=$((sum + val))
	done < "$work"
	printf "P1 sum of all expressions is: %d\n" "$sum"

	rm -f "$work" "${work}.bak" "$groups"
}

main_p2() {
    input="$1"
    work="${input}.work"
    sum=0
    rm -f "$work"
    while read -r line; do
        original_line="$line"
        printf "=======================\n"
        while true; do
            printf -- "-> %s\n" "$line"
            # Get all the lowest-nested groups for eval
            group=$(echo "$line" | grep -E -o '\([0-9*+ ]+\)' | head -n 1)
            if [ -z "$group" ]; then
                group=$(echo "$line" | grep -E '[*+]' | head -n 1)
                if [ -z "$group" ]; then
                    break
                fi
            fi 

            orig_group=$(printf '%s' "$group" | sed -e 's/[]\/$*.^[]/\\&/g')
            group=$(echo "$group" | tr -d '()')

		    # Process addition until there is none
		    while echo "$group" | grep -E -q '\+'; do
                exp=$(echo "$group" | grep -E -o '[0-9]+ \+ [0-9]+' | head -n 1)
                val=$((exp))
                esc_exp=$(printf '%s' "$exp" | sed -e 's/[]\/$*.^[]/\\&/g')
                group=$(echo "$group" | sed -e "0,/$esc_exp/s//$val/")
		    done

            # Process multiplication until there is none
		    while echo "$group" | grep -E -q '\*'; do
                exp=$(echo "$group" | grep -E -o '[0-9]+ \* [0-9]+' | head -n 1)
                val=$((exp))
                esc_exp=$(printf '%s' "$exp" | sed -e 's/[]\/$*.^[]/\\&/g')
                group=$(echo "$group" | sed -e "0,/$esc_exp/s//$val/")
		    done

		    line="${line//$orig_group/$group}"
        done

        printf "Output: %s\n\n" "$line"
        printf "=======================\n"
        printf "%s = %s\n" "$original_line" "$line" >> "$work"
        sum=$((sum + line))
    done < "$input"

	printf "P2 sum of all expressions is: %d\n" "$sum"
}

#main_p1 "$@"
main_p2 "$@"

const INPUT: &str = include_str!("input.txt");

fn main() {
    let input: Vec<&str> = INPUT.lines().collect();
    for entry1 in input.iter().enumerate() {
        let e1_int = entry1.1.parse::<i32>().unwrap();
        for entry2 in input.iter().skip(entry1.0).enumerate() {
            let e2_int = entry2.1.parse::<i32>().unwrap();
            for entry3 in input.iter().skip(entry1.0 + entry2.0) {
                let e3_int = entry3.parse::<i32>().unwrap();

                if e1_int + e2_int + e3_int == 2020 {
                    println!("{}, {}, and {} sum to 2020", e1_int, e2_int, e3_int);
                    println!("Their product is {}", e1_int * e2_int * e3_int)
                }
            }
        }
    }
}

const INPUT: &str = include_str!("input.txt");
const SAMPLE: &str = include_str!("sample.txt");

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum Heading {
    NORTH,
    SOUTH,
    EAST,
    WEST
}

fn new_heading(from: Heading, degrees: i64, dir: ActionKey) -> Heading {
    let headings_r = vec![Heading::NORTH, Heading::EAST, Heading::SOUTH, Heading::WEST];
    let headings_l = vec![Heading::NORTH, Heading::WEST, Heading::SOUTH, Heading::EAST];

    let new_index;
    if dir == ActionKey::RIGHT {
        let start_index = headings_r.iter().position(|&h| h == from).unwrap();
        new_index = ((start_index as i64) + (degrees / 90)) % 4;
        headings_r[new_index as usize]
    } else {
        let start_index = headings_l.iter().position(|&h| h == from).unwrap();
        new_index = ((start_index as i64) + (degrees / 90)) % 4;
        headings_l[new_index as usize]
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum ActionKey {
    NORTH,
    SOUTH,
    EAST,
    WEST,
    LEFT,
    RIGHT,
    FORWARD
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
struct Action {
    action: ActionKey,
    value: i64
}

impl Action {
    fn from_line(line: &str) -> Action {
        let action = match line.chars().next().unwrap() {
            'N' => ActionKey::NORTH,
            'S' => ActionKey::SOUTH,
            'E' => ActionKey::EAST,
            'W' => ActionKey::WEST,
            'L' => ActionKey::LEFT,
            'R' => ActionKey::RIGHT,
            'F' => ActionKey::FORWARD,
            _ => panic!("Invalid action!")
        };

        let value = line[1..].parse::<i64>().unwrap();

        Action {
            action,
            value
        }
    }

}

#[derive(Debug, Clone, Copy)]
struct Point {
    x: i64,
    y: i64,
    h: Heading,
    d: i64 // Manhattan distance from start
}

impl PartialEq for Point {
    fn eq(&self, other: &Point) -> bool {
        self.x == other.x && self.y == other.y
    }
}

struct Track {
    actions: Vec<Action>,
    points: Vec<Point>,
    waypoint: Point
}

impl Track {
    fn from_input(input: &str) -> Track {
        let mut actions = Vec::new();
        let points = vec![Point {x: 0, y: 0, h: Heading::EAST, d: 0}];
        for line in input.lines() {
            actions.push(Action::from_line(line));
        }

        Track {
            actions,
            points,
            waypoint: Point {x: 10, y: 1, h: Heading::EAST, d: 0}
        }
    }

    fn reset_waypoint(orig_waypoint: Point, degrees: i64, dir: ActionKey, ship: Point) -> Point {
        let x_delta = orig_waypoint.x - ship.x;
        let y_delta = orig_waypoint.y - ship.y;
        let headings_r = vec![Heading::NORTH, Heading::EAST, Heading::SOUTH, Heading::WEST];
        let headings_l = vec![Heading::NORTH, Heading::WEST, Heading::SOUTH, Heading::EAST];

        let new_heading;
        if dir == ActionKey::RIGHT {
            let start_index = headings_r.iter().position(|&h| h == orig_waypoint.h).unwrap();
            let new_index = ((start_index as i64) + (degrees / 90)) % 4;
            new_heading = headings_r[new_index as usize];
        } else {
            let start_index = headings_l.iter().position(|&h| h == orig_waypoint.h).unwrap();
            let new_index = ((start_index as i64) + (degrees / 90)) % 4;
            new_heading = headings_l[new_index as usize]
        }

        let mut new_waypoint = orig_waypoint;
        match orig_waypoint.h {
            Heading::NORTH => {
                match new_heading {
                    Heading::EAST => {
                        new_waypoint.x = ship.x + y_delta;
                        new_waypoint.y = ship.y - x_delta;
                    }
                    Heading::WEST => {
                        new_waypoint.x = ship.x - y_delta;
                        new_waypoint.y = ship.y + x_delta;
                    }
                    Heading::SOUTH => {
                        new_waypoint.x = ship.x - x_delta;
                        new_waypoint.y = ship.y - y_delta;
                    }
                    _ => panic!("Bad dir from north")
                }
            }
            Heading::SOUTH => {
                match new_heading {
                    Heading::EAST => {
                        new_waypoint.x = ship.x - y_delta;
                        new_waypoint.y = ship.y + x_delta;
                    }
                    Heading::WEST => {
                        new_waypoint.x = ship.x + y_delta;
                        new_waypoint.y = ship.y - x_delta;
                    }
                    Heading::NORTH => {
                        new_waypoint.x = ship.x - x_delta;
                        new_waypoint.y = ship.y - y_delta;
                    }
                    _ => panic!("Bad dir from south")
                }
            }
            Heading::EAST => {
                match new_heading {
                    Heading::NORTH => {
                        new_waypoint.y = ship.y + x_delta;
                        new_waypoint.x = ship.x - y_delta;
                    }
                    Heading::SOUTH => {
                        new_waypoint.y = ship.y - x_delta;
                        new_waypoint.x = ship.x + y_delta;
                    }
                    Heading::WEST => {
                        new_waypoint.y = ship.y - y_delta;
                        new_waypoint.x = ship.x - x_delta;
                    }
                    _ => panic!("Bad dir from south")
                }
            }
            Heading::WEST => {
                match new_heading {
                    Heading::NORTH => {
                        new_waypoint.y = ship.y - x_delta;
                        new_waypoint.x = ship.x + y_delta;
                    }
                    Heading::SOUTH => {
                        new_waypoint.y = ship.y + x_delta;
                        new_waypoint.x = ship.x - y_delta;
                    }
                    Heading::EAST => {
                        new_waypoint.y = ship.y - y_delta;
                        new_waypoint.x = ship.x - x_delta;
                    }
                    _ => panic!("Bad dir from south")
                }
            }
        }
        new_waypoint.h = new_heading;
        new_waypoint
    }

    fn run(&mut self) {
        let mut last_point = *self.points.last().unwrap();

        for act in &self.actions {
            let mut new_point = last_point;
            match act.action {
                ActionKey::NORTH => new_point.y += act.value,
                ActionKey::SOUTH => new_point.y -= act.value,
                ActionKey::EAST => new_point.x += act.value,
                ActionKey::WEST => new_point.x -= act.value,
                ActionKey::LEFT => new_point.h = new_heading(last_point.h, act.value, ActionKey::LEFT),
                ActionKey::RIGHT => new_point.h = new_heading(last_point.h, act.value, ActionKey::RIGHT),
                ActionKey::FORWARD => {
                    match last_point.h {
                        Heading::NORTH => new_point.y += act.value,
                        Heading::SOUTH => new_point.y -= act.value,
                        Heading::EAST => new_point.x += act.value,
                        Heading::WEST => new_point.x -= act.value,
                    }
                }
            }

            new_point.d += get_md(last_point, new_point);
            self.points.push(new_point);
            last_point = new_point;
        }
    }

    fn run_p2(&mut self) {
        let mut last_point = *self.points.last().unwrap();

        for act in &self.actions {
            let x_delta = self.waypoint.x - last_point.x;
            let y_delta = self.waypoint.y - last_point.y;
            let mut new_point = last_point;

            match act.action {
                ActionKey::NORTH => self.waypoint.y += act.value,
                ActionKey::SOUTH => self.waypoint.y -= act.value,
                ActionKey::EAST => self.waypoint.x += act.value,
                ActionKey::WEST => self.waypoint.x -= act.value,
                ActionKey::LEFT => {
                    self.waypoint = Track::reset_waypoint(self.waypoint, act.value, ActionKey::LEFT, last_point);
                },
                ActionKey::RIGHT => {
                    self.waypoint = Track::reset_waypoint(self.waypoint, act.value, ActionKey::RIGHT, last_point);
                },
                ActionKey::FORWARD => {
                    for _ in 0..act.value {
                        new_point.x += x_delta;
                        new_point.y += y_delta;
                        self.waypoint.x += x_delta;
                        self.waypoint.y += y_delta;
                    }
                }
            }

            new_point.d += get_md(last_point, new_point);
            self.points.push(new_point);
            last_point = new_point;
        }
    }

}

fn get_md(a: Point, b: Point) -> i64 {
    (a.x - b.x).abs() + (a.y - b.y).abs()
}

fn main() {
    let mut sample_track = Track::from_input(SAMPLE);
    sample_track.run();
    let sample_last_point = sample_track.points.last().unwrap();
    assert_eq!(sample_last_point.x.abs() + sample_last_point.y.abs(), 25);

    sample_track = Track::from_input(SAMPLE);
    sample_track.run_p2();
    let sample_last_point = sample_track.points.last().unwrap();
    assert_eq!(sample_last_point.x.abs() + sample_last_point.y.abs(), 286);

    let mut track = Track::from_input(INPUT);
    track.run();
    let last_point = track.points.last().unwrap();
    println!("P1 - Manhattan distance to the last point is {}", last_point.x.abs() + last_point.y.abs());

    track = Track::from_input(INPUT);
    track.run_p2();
    let last_point = track.points.last().unwrap();
    println!("P2 - Manhattan distance to the last point is {}", last_point.x.abs() + last_point.y.abs());

}

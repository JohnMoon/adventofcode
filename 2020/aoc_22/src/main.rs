const INPUT: &str = include_str!("input.txt");
const SAMPLE: &str = include_str!("sample.txt");

struct Cards {
    p1_cards: Vec<u64>, // [0] is the top
    p2_cards: Vec<u64> // [0] is the top
}

impl Cards {
    fn from_input(input: &str) -> Cards {
        let mut p1_cards = Vec::new();
        let mut p2_cards = Vec::new();
        let mut p1_deck_read = true;

        for line in input.lines() {
            if line.starts_with("Player 1") {
                continue;
            }

            if line.starts_with("Player 2") {
                p1_deck_read = false;
                continue;
            }

            if line.is_empty() {
                continue;
            }

            let card = line.parse::<u64>().unwrap();
            if p1_deck_read {
                p1_cards.push(card);
            } else {
                p2_cards.push(card);
            }
        }

        Cards {
            p1_cards,
            p2_cards
        }
    }

    fn play(&mut self) {
        loop {
            if self.p1_cards.is_empty() {
                println!("Player 2 wins!");
                break;
            } else if self.p2_cards.is_empty() {
                println!("Player 1 wins!");
                break;
            } else {
                self.play_round();
            }
        }
    }

    fn play_round(&mut self) {
        let p1_card = self.p1_cards[0];
        let p2_card = self.p2_cards[0];
        self.p1_cards.remove(0);
        self.p2_cards.remove(0);
        if p1_card > p2_card {
            self.p1_cards.push(p1_card);
            self.p1_cards.push(p2_card);
        } else if p2_card > p1_card {
            self.p2_cards.push(p2_card);
            self.p2_cards.push(p1_card);
        } else {
            panic!("Equal cards!");
        }
    }

    fn get_score(&self) -> usize {
        let winner_cards;
        if self.p1_cards.is_empty() {
            winner_cards = &self.p2_cards;
        } else if self.p2_cards.is_empty() {
            winner_cards = &self.p1_cards;
        } else {
            panic!("No winner!");
        }

        let mut score = 0;
        for (i, card) in winner_cards.iter().enumerate() {
            let scalar = winner_cards.len() - i;
            score += (*card as usize) * scalar;
        }
        score
    }
}

fn main() {
    let mut s_cards = Cards::from_input(SAMPLE);
    s_cards.play();
    assert_eq!(s_cards.get_score(), 306);

    let mut cards = Cards::from_input(INPUT);
    cards.play();
    println!("The winning score is {}", cards.get_score());
}

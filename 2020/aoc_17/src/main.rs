const INPUT: &str = include_str!("input.txt");
const SAMPLE: &str = include_str!("sample.txt");

use std::collections::HashMap;

#[derive (Hash, Eq, PartialEq, Clone, Copy, Debug)]
struct Point {
    x: i64,
    y: i64,
    z: i64,
    w: i64
}

impl Point {
    fn new(x: i64, y: i64, z: i64, w: i64) -> Point {
        Point {
            x,
            y,
            z,
            w
        }
    }
}

#[derive (Clone)]
struct PocketDim {
    space: HashMap<Point, bool>
}

impl PocketDim {
    fn from_input(input: &str) -> PocketDim {
        // Loading just the z = 0 layer
        let mut space = HashMap::new();
        let mut x_count = 0;
        let mut y_count = 0;
        for (x, line) in input.lines().enumerate() {
            x_count += 1;
            y_count = 0;
            for (y, c) in line.chars().enumerate() {
                y_count += 1;
                let active = c == '#';
                space.insert(
                    Point::new(x as i64, y as i64, 0, 0),
                    active);
            }
        }

        println!("Starting with a {}x{} grid", x_count, y_count);

        // Add a buffer gap around the plane
        for x in -1..=x_count + 1 {
            for y in -1..=y_count + 1 {
                for z in -1..=1 {
                    for w in -1..=1 {
                        let p = Point::new(x, y, z, w);
                        space.entry(p).or_insert(false);
                    }
                }

            }
        }

        PocketDim {
            space
        }
    }

    fn get_neighbors(&self, p: &Point) -> HashMap<Point, bool> {
        let mut ret = HashMap::new();

        let mut test_point = Point::new(0, 0, 0, 0);
        for x in p.x - 1..=p.x + 1 {
            test_point.x = x;
            for y in p.y - 1..=p.y + 1 {
                test_point.y = y;
                for z in p.z - 1..=p.z + 1 {
                    test_point.z = z;
                    for w in p.w - 1..=p.w + 1 {
                        test_point.w = w;
                        if test_point == *p {
                            continue;
                        }

                        match self.space.get(&test_point) {
                            Some(a) => {
                                ret.insert(test_point, *a);
                            },
                            None => {
                                ret.insert(test_point, false);
                            }
                        };
                    }
                }
            }
        }

        // Should always have 80 neighbors
        assert_eq!(ret.len(), 80);
        ret
    }

    fn cycle(&mut self) {
        let mut activate = Vec::new();
        let mut deactivate = Vec::new();
        let mut neighbors_to_add = HashMap::new();
        for (p, a) in &self.space {
            let neighbors = self.get_neighbors(p);
            let mut active_neighbor_count = 0;
            for v in neighbors.values() {
                if *v {
                    active_neighbor_count += 1;
                }
            }
            //println!("{} neighbors are active", active_neighbor_count);

            if *a {
                // If a cube is active and exactly 2 or 3 of its neighbors are also active,
                // the cube remains active. Otherwise, the cube becomes inactive.
                if active_neighbor_count == 2 || active_neighbor_count == 3 {
                    activate.push(*p);
                } else {
                    deactivate.push(*p);
                }
            } else {
                // If a cube is inactive but exactly 3 of its neighbors are active, the cube
                // becomes active. Otherwise, the cube remains inactive.
                if active_neighbor_count == 3 {
                    activate.push(*p);
		} else {
                    deactivate.push(*p);
                }
            }

            for (n, v) in &neighbors {
                neighbors_to_add.insert(*n, *v);
            }
        }

        for (n, v) in &neighbors_to_add {
            self.space.insert(*n, *v);
        }

        for p in deactivate {
            self.space.insert(p, false);
        }

        for p in activate {
            self.space.insert(p, true);
        }

    }

    fn count_active(&self) -> usize {
        let mut sum = 0;
        for a in self.space.values() {
            if *a {
                sum += 1;
            }
        }
        sum
    }

    //fn print_z(&self, z: i64) {
    //    let mut layer = self.space.clone();
    //    layer.retain(|p, _| { p.z == z });

    //    let mut min_x = 1000;
    //    let mut max_x = 0;
    //    let mut min_y = 1000;
    //    let mut max_y = 0;
    //    for p in layer.keys() {
    //        if p.x > max_x {
    //            max_x = p.x;
    //        }
    //        if p.x < min_x {
    //            min_x = p.x;
    //        }

    //        if p.y > max_y {
    //            max_y = p.y;
    //        }
    //        if p.y < min_y {
    //            min_y = p.y;
    //        }
    //    }

    //    println!("z = {}", z);
    //    for x in min_x..=max_x {
    //        for y in min_y..=max_y {
    //            if *layer.get(&Point {x, y, z}).unwrap() {
    //                print!("#");
    //            } else {
    //                print!(".");
    //            }
    //        }
    //        println!();
    //    }
    //}
}

fn main() {
    let mut sdim = PocketDim::from_input(SAMPLE);
    for _ in 0..6 {
        sdim.cycle();
    }
    assert_eq!(sdim.count_active(), 848);

    let mut dim = PocketDim::from_input(INPUT);
    for _ in 0..6 {
        dim.cycle();
    }

    println!("After 6 cycles, the pocket dimension has {} active cubes", dim.count_active());
}

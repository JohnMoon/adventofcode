const INPUT: &str = include_str!("input.txt");

struct Passport {
    byr: Option<i64>,
    iyr: Option<i64>,
    eyr: Option<i64>,
    hgt: Option<Height>,
    hcl: Option<String>,
    ecl: Option<EyeColor>,
    pid: Option<String>,
    cid: Option<String>
}

impl Passport {
    fn new_null() -> Passport {
        Passport {
            byr: None,
            iyr: None,
            eyr: None,
            hgt: None,
            hcl: None,
            ecl: None,
            pid: None,
            cid: None
        }
    }

    fn from_string(input: &str) -> Passport {
        let mut ret = Passport::new_null();
        let tokens: Vec<&str> = input.split_whitespace().collect();
        for token in tokens {
            let field = token.split(':').next().unwrap();
            let value = token.split(':').nth(1).unwrap();

            match field {
                "byr" => ret.byr = Some(value.parse::<i64>().unwrap()),
                "iyr" => ret.iyr = Some(value.parse::<i64>().unwrap()),
                "eyr" => ret.eyr = Some(value.parse::<i64>().unwrap()),
                "hgt" => ret.hgt = Some(Height::from_string(value)),
                "hcl" => ret.hcl = Some(value.to_string()),
                "ecl" => ret.ecl = Some(EyeColor::from_string(value)),
                "pid" => ret.pid = Some(value.to_string()),
                "cid" => ret.cid = Some(value.to_string()),
                _ => panic!("Invalid field!")
            }
        }

        ret
    }

    fn is_valid_p1(&self) -> bool {
        // cid is optional, so don't include here
        self.byr.is_some() &&
            self.iyr.is_some() &&
            self.eyr.is_some() &&
            self.hgt.is_some() &&
            self.hcl.is_some() &&
            self.ecl.is_some() &&
            self.pid.is_some()
    }

    fn is_valid_p2(&self) -> bool {
        // cid is optional, so don't include here
        self.byr.is_some() && self.byr.unwrap() >= 1920 && self.byr.unwrap() <= 2002 &&
            self.iyr.is_some() && self.iyr.unwrap() >= 2010 && self.iyr.unwrap() <= 2020 &&
            self.eyr.is_some() && self.eyr.unwrap() >= 2020 && self.eyr.unwrap() <= 2030 &&
            self.hgt.is_some() && self.hgt.clone().unwrap().is_valid() &&
            self.hcl.is_some() && valid_hex(&self.hcl.clone().unwrap()) &&
            self.ecl.is_some() && self.ecl.clone().unwrap().is_valid() &&
            self.pid.is_some() && self.pid.clone().unwrap().chars().filter(|c| c.is_numeric()).count() == 9
    }
}

fn main() {
    let mut passports = Vec::new();
    let mut p_str = "".to_string();

    // Add a newline to make parsing logic easier
    let input_w_newline = &format!("{}\n", INPUT);

    for line in input_w_newline.lines() {
        if line.is_empty() {
            passports.push(Passport::from_string(&p_str));
            p_str = "".to_string();
        } else if p_str.is_empty() {
                p_str = line.to_string();
        } else {
            p_str = format!("{} {}", p_str, line);
        }
    }

    let valid_count_p1 = passports.iter().filter(|p| p.is_valid_p1()).count();
    println!("{}/{} passports are valid (in part 1)!", valid_count_p1, passports.len());

    let valid_count_p2 = passports.iter().filter(|p| p.is_valid_p2()).count();
    println!("{}/{} passports are valid (in part 2)!", valid_count_p2, passports.len());
}

fn valid_hex(input: &str) -> bool {
    input.starts_with('#') &&
        i64::from_str_radix(&input[1..], 16).is_ok()
}

#[derive (Clone)]
struct Height {
    unit: String,
    value: u32
}

impl Height {
    fn from_string(input: &str) -> Height {
        let digits_s: String = input.chars().filter(|c| c.is_numeric()).collect();
        let unit: String = input.chars().filter(|c| !c.is_numeric()).collect();
        Height {
            unit,
            value: digits_s.parse::<u32>().unwrap()
        }
    }

    fn is_valid(&self) -> bool {
        (self.unit == "cm" && self.value >= 150 && self.value <= 193) ||
            (self.unit == "in" && self.value >= 59 && self.value <= 76)
    }
}

#[derive (Clone)]
enum EyeColors {
    AMB,
    BLU,
    BRN,
    GRY,
    GRN,
    HZL,
    OTH
}

#[derive (Clone)]
struct EyeColor {
    color: Option<EyeColors>
}

impl EyeColor {
    fn from_string(input: &str) -> EyeColor {
        let color = match input {
            "amb" => Some(EyeColors::AMB),
            "blu" => Some(EyeColors::BLU),
            "brn" => Some(EyeColors::BRN),
            "gry" => Some(EyeColors::GRY),
            "grn" => Some(EyeColors::GRN),
            "hzl" => Some(EyeColors::HZL),
            "oth" => Some(EyeColors::OTH),
            _ => None
        };
        EyeColor {
            color
        }
    }

    fn is_valid(&self) -> bool {
        self.color.is_some()
    }
}

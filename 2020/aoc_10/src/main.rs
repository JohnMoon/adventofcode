const INPUT: &str = include_str!("input.txt");
const SAMPLE: &str = include_str!("sample.txt");
const SAMPLE2: &str = include_str!("sample2.txt");

use std::collections::HashMap;

#[derive (Debug, Clone)]
struct Adapter {
    input: Option<u32>,
    output: u32,
    outgoing_paths: Vec<Adapter>
}

impl Adapter {
    fn new(input: Option<u32>, output: u32) -> Adapter {
        Adapter {
            input,
            output,
            outgoing_paths: Vec::new()
        }
    }

    fn can_take(&self, input: u32) -> bool {
        (input < self.output) &&
            ((self.output >= 1 && input >= (self.output - 1)) ||
            (self.output >= 2 && input >= (self.output - 2)) ||
            (self.output >= 3 && input >= (self.output - 3)))
    }

}

#[derive (Debug)]
struct Chain {
    chain: Vec<Adapter>,
    dev_jolts: u32,
    total_paths: u64
}

impl Chain {
    fn from_input(input: &str) -> Chain {
        let mut bag_contents = vec![Adapter::new(None, 0)];
        bag_contents.append(&mut input.lines().map(|l| {
            Adapter::new(None, l.parse::<u32>().unwrap())
        }).collect());

        bag_contents.sort_by_key(|a| a.output);

        let dev_jolts = bag_contents.last().unwrap().output + 3;
        bag_contents.push(Adapter::new(None, dev_jolts));

        let mut ret = Chain {
            chain: bag_contents,
            dev_jolts,
            total_paths: 0
        };
        ret.assemble();
        ret
    }

    fn assemble(&mut self) {
        for i in 0..self.chain.len() {
            let mut output_set = false;
            for j in (i + 1)..self.chain.len() {
                let a2 = self.chain[j].clone();
                if a2.can_take(self.chain[i].output) {
                    if self.chain[j].input.is_none() && !output_set {
                        self.chain[j].input = Some(self.chain[i].output);
                        output_set = true;
                    }
                    self.chain[i].outgoing_paths.push(a2);
                }
            }
        }
    }

    fn jolt_deltas(&self, n: u32) -> usize {
        let ret = self.chain.iter().filter(|a| {
            if let Some(input) = a.input {
                a.output - input == n
            } else {
                false
            }
        }).count();
        ret
    }

    fn total_outgoing_paths(&mut self) -> usize {
        let mut outgoing_paths_map: HashMap<u32, usize> = HashMap::new();
        for (rev_i, a) in self.chain.iter().rev().enumerate() {
            if a.output == self.dev_jolts {
                outgoing_paths_map.insert(a.output, 1);
                continue;
            }

            // Adapter = 19
            let mut paths = 0;
            for upstream_a in self.chain.iter().rev().take(rev_i) {
                if upstream_a.can_take(a.output) && outgoing_paths_map.contains_key(&upstream_a.output) {
                    paths += outgoing_paths_map.get(&upstream_a.output).unwrap();
                }
            }
            //println!("Node {} has {} paths", a.output, paths);
            outgoing_paths_map.insert(a.output, paths);
        }
        *outgoing_paths_map.get(&0).unwrap()
    }
}

fn main() {
    let mut sample_chain = Chain::from_input(SAMPLE);
    let mut sample2_chain = Chain::from_input(SAMPLE2);

    assert!(sample_chain.jolt_deltas(1) == 22 && sample_chain.jolt_deltas(3) == 10);
    assert!(sample2_chain.jolt_deltas(1) == 7 && sample2_chain.jolt_deltas(3) == 5);

    assert_eq!(sample2_chain.total_outgoing_paths(), 8);
    assert_eq!(sample_chain.total_outgoing_paths(), 19208);

    println!("Tests passed!");

    let mut chain = Chain::from_input(INPUT);

    println!("Product of 1-jolt and 3-jolt differences: {}",
            chain.jolt_deltas(1) * chain.jolt_deltas(3));
    println!("Branches reaching device: {}", chain.total_outgoing_paths());
}

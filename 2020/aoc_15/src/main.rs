const INPUT: &str = include_str!("input.txt");
const SAMPLE: &str = include_str!("sample.txt");

use std::collections::HashMap;

struct Game {
    nums: Vec<usize>,
    indicies: HashMap<usize, Vec<usize>>
}

impl Game {
    fn from_input(input: &str) -> Game {
        let mut start_nums = Vec::new();
        let mut indicies: HashMap<usize, Vec<usize>> = HashMap::new();
        for (i, n_str) in input.trim().split(',').enumerate() {
            let n = n_str.parse::<usize>().unwrap();
            start_nums.push(n);

            match indicies.get_mut(&n) {
                Some(i_vec) => { i_vec.push(i); },
                None => { indicies.insert(n, vec![i]); }
            };
        }

        Game {
            nums: start_nums,
            indicies
        }
    }

    fn get_next_num(&mut self) -> usize {
        let cur_num = self.nums.last().unwrap();
        let cur_num_index = self.nums.len() - 1;
        match self.indicies.get_mut(cur_num) {
            Some(i_vec) => { 
                let last_occ_index = *i_vec.last().unwrap();
                i_vec.push(cur_num_index);
                cur_num_index - last_occ_index
            },
            None => {
                self.indicies.insert(*cur_num, vec![cur_num_index]);
                0
            }
        }
    }

    fn run_till(&mut self, end: usize) -> usize {
        loop {
            let next = self.get_next_num();
            self.nums.push(next);
            if self.nums.len() == end {
                return next;
            }
        }
    }
}

fn main() {
    let mut sample_game = Game::from_input(SAMPLE);
    assert_eq!(sample_game.run_till(2020), 436);

    sample_game = Game::from_input(SAMPLE);
    assert_eq!(sample_game.run_till(30_000_000), 175_594);

    let mut game = Game::from_input(INPUT);
    println!("The 2020th number is {}", game.run_till(2020));

    game = Game::from_input(INPUT);
    println!("The 30,000,000th number is {}", game.run_till(30_000_000));
}

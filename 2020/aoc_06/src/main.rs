const INPUT: &str = include_str!("input.txt");

struct Group {
    yes_letters: Vec<char>
}

impl Group {
    fn from_string_p1(input: &str) -> Group {
        let mut yes_letters = Vec::new();

        for c in input.chars().filter(|c| c.is_alphabetic()) {
            if ! yes_letters.iter().any(|&y| y == c) {
                yes_letters.push(c);
            }
        }

        Group {
            yes_letters
        }
    }

    fn from_string_p2(input: &str) -> Group {
        let mut yes_letter_mat: Vec<Vec<char>> = Vec::new();
        for line in input.lines() {
            let mut yes_letters: Vec<char> = Vec::new();
            for c in line.chars().filter(|c| c.is_alphabetic()) {
                if ! yes_letters.iter().any(|&y| y == c) {
                    yes_letters.push(c);
                }
            }
            yes_letter_mat.push(yes_letters);
        }

        let mut everyone_yes_letters: Vec<char> = Vec::new();
        let first_person = yes_letter_mat.first().unwrap().clone();
        for letter in first_person {
            let mut everyone = true;
            for vec in yes_letter_mat.iter().skip(1) {
                if ! vec.iter().any(|&c| c == letter) {
                    everyone = false;
                    break;
                }
            }
            if everyone {
                everyone_yes_letters.push(letter);
            }
        }

        Group {
            yes_letters: everyone_yes_letters
        }
    }
}


fn main() {
    // Add a newline to make parsing logic easier
    let input_w_newline = &format!("{}\n", INPUT);
    for part in 1..=2 {
        let mut groups = Vec::new();
        let mut g_str = "".to_string();

        for line in input_w_newline.lines() {
            if line.is_empty() {
                if part == 1 {
                    groups.push(Group::from_string_p1(&g_str));
                } else {
                    groups.push(Group::from_string_p2(&g_str));
                }

                g_str = "".to_string();
            } else if g_str.is_empty() {
                g_str = line.to_string();
            } else if part == 1 {
                g_str = format!("{} {}", g_str, line);
            } else {
                g_str = format!("{}\n{}", g_str, line);
            }
        }

        let yes_sum = groups.iter().fold(0, |acc, g| acc + g.yes_letters.len());

        println!("Sum of all yes answers (part {}): {}", part, yes_sum);
    }
}

const INPUT: &str = include_str!("input.txt");
const SAMPLE: &str = include_str!("sample.txt");

use std::collections::HashMap;

struct Ingredients {
    map: HashMap<String, Option<String>>
}

#[derive (Clone, Debug)]
struct Food {
    ingredients: Vec<String>,
    allergens: Vec<String>
}

#[derive (Clone, Debug)]
struct FoodList {
    list: Vec<Food>
}

impl FoodList { // IngList
    fn from_input(input: &str) -> FoodList {
        let mut food_list = Vec::new();
        for line in input.lines() {
            let mut ing_list = Vec::new();
            let mut alg_list = Vec::new();
            let mut in_alg = false;
            for word in line.split_whitespace() {
                if word == "(contains" {
                    in_alg = true;
                    continue;
                }

                if ! in_alg {
                    ing_list.push(word.to_string());
                } else {
                    alg_list.push(word.replace(")", "").replace(",", ""));
                }
            }
            food_list.push(Food {
                ingredients: ing_list,
                allergens: alg_list
            });
        }
        FoodList {
            list: food_list
        }
    }

    fn get_ingredients(&self) -> Ingredients {
        let mut all_allergens = Vec::new();
        let mut ingredients: HashMap<String, Option<String>> = HashMap::new();
        for food in &self.list {
            for a in &food.allergens {
                if ! all_allergens.contains(&a) {
                    all_allergens.push(a);
                }
            }

            for i in &food.ingredients {
                if ! ingredients.contains_key(i) {
                    ingredients.insert(i.clone(), None);
                }
            }
        }

        let mut fit_map: HashMap<String, Vec<String>> = HashMap::new();
        for a in all_allergens {
            let mut ing_lists_w_allergen = Vec::new();
            for food in &self.list {
                if food.allergens.contains(&a) {
                    ing_lists_w_allergen.push(food.ingredients.clone());
                }
            }

            let first_list = ing_lists_w_allergen.first().unwrap();
            for ing in first_list {
                let mut candidate = true;
                for ing_list in ing_lists_w_allergen.iter().skip(1) {
                    if ! ing_list.contains(ing) {
                        candidate = false;
                        break;
                    }
                }
                if candidate {
                    if fit_map.contains_key(ing) {
                        let ing_ref = fit_map.get_mut(ing).unwrap();
                        ing_ref.push(a.clone());
                    } else {
                        fit_map.insert(ing.clone(), vec![a.clone()]);
                    }
                }
            }
        }

        let mut assigned_algs = Vec::new();
        loop {
            let mut assigned_ings = Vec::new();
            for (i, a_v) in &fit_map {
                let filtered_a: Vec<&String> = a_v.iter().filter(|&a| !assigned_algs.contains(a)).collect();
                if filtered_a.len() == 1 {
                    let cand_alg = ingredients.get_mut(i).unwrap();
                    *cand_alg = Some(filtered_a[0].clone());

                    assigned_ings.push(i.clone());
                    assigned_algs.push(filtered_a[0].clone());
                }
            }

            if assigned_ings.is_empty() {
                break;
            } else {
                for i in assigned_ings {
                    fit_map.remove(&i);
                }
            }
        }

        Ingredients {
            map: ingredients
        }
    }

    fn get_ingredient_occurance(&self, ing: &str) -> usize {
        let mut sum = 0;
        for food in &self.list {
            sum += food.ingredients.iter().filter(|&i| i == ing).count();
        }
        sum
    }
}

fn run_p1(input: &str, val: Option<usize>) {
    let food_list = FoodList::from_input(input);
    let ings = food_list.get_ingredients();

    let mut no_alg_ing_occurances = 0;
    for (k, v) in &ings.map {
        if v.is_none() {
            no_alg_ing_occurances += food_list.get_ingredient_occurance(&k);
        }
    }

    println!("The ingredients with no allergens are present {} times", no_alg_ing_occurances);

    if let Some(v) = val {
        assert_eq!(no_alg_ing_occurances, v);
    }
}

fn run_p2(input: &str) {
    let food_list = FoodList::from_input(input);
    let ings = food_list.get_ingredients();

    let mut dangerous_ings: Vec<(String, String)> = Vec::new();
    for (k, v) in &ings.map {
        if let Some(a) = v {
            dangerous_ings.push((k.clone(), a.clone()));
        }
    }

    dangerous_ings.sort_by(|a, b| a.1.cmp(&b.1));

    print!("Dangerous list: ");
    for i in 0..dangerous_ings.len() {
        if i == dangerous_ings.len() - 1 {
            println!("{}", dangerous_ings[i].0);
        } else {
            print!("{},", dangerous_ings[i].0);
        }
    }
}

fn main() {
    println!("Sample:");
    run_p1(SAMPLE, Some(5));
    run_p2(SAMPLE);
    println!("\nInput:");
    run_p1(INPUT, Some(2461));
    run_p2(INPUT);
}

const INPUT: &str = include_str!("input.txt");
const SAMPLE: &str = include_str!("sample.txt");
const SAMPLE2: &str = include_str!("sample2.txt");

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
struct Bus {
    id: u64,
    earliest_dep_time: u64
}

impl Bus {
    fn new(id: u64, zero_time: u64) -> Bus {
        let mut earliest_dep_time = zero_time + 1;

        while earliest_dep_time % id != 0 {
            earliest_dep_time += 1;
        }

        Bus {
            id,
            earliest_dep_time
        }
    }
}

struct BusSchedule {
    arrival_time: u64,
    buses: Vec<Option<Bus>>
}

impl BusSchedule {
    fn from_input(input: &str) -> BusSchedule {
        let arrival_time = input.lines().next().unwrap().parse::<u64>().unwrap();

        let bus_line = input.lines().nth(1).unwrap();

        let mut buses = Vec::new();
        for b in bus_line.split(',') {
            if b == "x" {
                buses.push(None);
                continue;
            }

            let bus_id = b.parse::<u64>().unwrap();
            buses.push(Some(Bus::new(bus_id, arrival_time)));
        }

        BusSchedule {
            arrival_time,
            buses
        }
    }

    fn get_earliest_bus(&self) -> Option<Bus> {
        let mut earliest_bus: Option<Bus> = None;
        let mut lowest_wait_time = 1_000_000_000_000;
        for bus_e in &self.buses {
            if let Some(bus) = bus_e {
                let minutes_to_wait = bus.earliest_dep_time - self.arrival_time;
                if minutes_to_wait < lowest_wait_time {
                    lowest_wait_time = minutes_to_wait;
                    earliest_bus = Some(*bus);
                }
            }
        }
        earliest_bus
    }

    fn get_earliest_departure_seq(&self, start: u64) ->  u64 {
        let mut max_bus: (u64, u64) = (0, 0);
        let mut id_index: Vec<(u64, u64)> = Vec::new();
        for (i, bus) in self.buses.iter().enumerate() {
            if let Some(b) = bus {
                if b.id > max_bus.0 {
                    max_bus = (b.id, i as u64);
                }
                id_index.push((b.id, i as u64));
            }
        }

        let mut candidate = start;
        while candidate % max_bus.0 != 0 {
            candidate += 1;
        }
        let mut success;
        let mut ctr: u64 = 0;
        println!("Loop count: 0 (Candidate: {})", candidate);
        loop {
            ctr += 1;
            if ctr % 100_000_000 == 0 {
                println!("Loop count: {} | Candidate: {}", ctr, candidate);
            }

            assert_eq!(candidate % max_bus.0, 0);

            success = true;
            for (bus_id, index) in &id_index {
                let mod_factor = (candidate + index - max_bus.1) % bus_id;
                if mod_factor != 0 {
                    success = false;
                    candidate += max_bus.0;
                    break;
                }
            }

            if success {
                return candidate - max_bus.1;
            }
        }
    }
}

fn main() {
    let sample_sched = BusSchedule::from_input(SAMPLE);
    let sample_earliest_bus = sample_sched.get_earliest_bus().unwrap();
    assert_eq!(sample_earliest_bus.id * (sample_earliest_bus.earliest_dep_time - sample_sched.arrival_time), 295);
    println!("Test case 1 passed!");
    //let sample_sched_2 = BusSchedule::from_input(SAMPLE2);
    //assert_eq!(sample_sched_2.get_earliest_departure_seq(3000), 3417);
    //println!("Test case 2 passed!");
    let sample_sched_2 = BusSchedule::from_input(SAMPLE2);
    assert_eq!(sample_sched_2.get_earliest_departure_seq(100), 1202161486);
    println!("Test case 2 passed!");

    let sched = BusSchedule::from_input(INPUT);
    let earliest_bus = sched.get_earliest_bus().unwrap();
    println!("The earliest bus is ID {}. Must wait {} minutes for it.",
        earliest_bus.id, earliest_bus.earliest_dep_time - sched.arrival_time);
    println!("Product of those two values is: {}",
        earliest_bus.id * (earliest_bus.earliest_dep_time - sched.arrival_time));
    //println!("Part 2 earliest sequential departure time: {}", sched.get_earliest_departure_seq(100000000000000));
    println!("132434199998584 is too LOW");
    println!("Part 2 earliest sequential departure time: {}", sched.get_earliest_departure_seq(132434199998584));
}

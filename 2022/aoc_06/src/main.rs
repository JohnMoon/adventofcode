const INPUT: &str = include_str!("input.txt");
const SAMPLE: &str = include_str!("sample.txt");

fn all_chars_distinct(chars: &[char]) -> bool {
    ! (1..chars.len()).any(|i| {
        chars[i..].contains(&chars[i - 1])
    })
}

fn find_distinct_n(msg: &str, n: usize) -> usize {
    n + msg.chars()
        .collect::<Vec<char>>()
        .windows(n)
        .position(all_chars_distinct)
        .expect("no distinct sequence!")
}

fn main() {
    assert_eq!(find_distinct_n(SAMPLE, 4), 7);
    println!("First distinct sequence of 4: {}\n", find_distinct_n(INPUT, 4));

    assert_eq!(find_distinct_n(SAMPLE, 14), 19);
    println!("First distinct sequence of 14: {}", find_distinct_n(INPUT, 14));
}

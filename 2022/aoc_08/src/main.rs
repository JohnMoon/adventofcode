const INPUT: &str = include_str!("input.txt");
const SAMPLE: &str = include_str!("sample.txt");

use std::collections::HashMap;

struct Forest {
    max_x: usize,
    max_y: usize,
    trees: HashMap<(usize, usize), u32>
}

impl Forest {
    fn from_str(data: &str) -> Forest {
        let lines: Vec<&str> = data.lines().collect();
        let max_x = lines[0].len() - 1;
        let max_y = lines.len() - 1;
        let mut trees: HashMap<(usize, usize), u32> = HashMap::new();
        for (y, line) in lines.iter().enumerate() {
            for (x, c) in line.chars().enumerate() {
                trees.insert((x, y), c.to_digit(10).unwrap());
            }
        }
        Forest { max_x, max_y, trees }
    }

    fn tree_is_visible(&self, x: usize, y: usize) -> bool {
        let h = self.trees.get(&(x, y));
        // Left
        (0..x).all(|tx| self.trees.get(&(tx, y)) < h) ||
        // Right
        (x + 1..=self.max_x).all(|tx| self.trees.get(&(tx, y)) < h) ||
        // Up
        (0..y).all(|ty| self.trees.get(&(x, ty)) < h) ||
        // Down
        (y + 1..=self.max_y).all(|ty| self.trees.get(&(x, ty)) < h)
    }

    fn get_scenic_score(&self, x: usize, y: usize) -> u64 {
        let h = self.trees.get(&(x, y));
        let mut score = 1;

        // Left
        score *= if let Some(i) = (0..x).rev().position(|tx| {
            self.trees.get(&(tx, y)) >= h
        }) { 1 + i as u64 } else { x as u64 };

        // Right
        score *= if let Some(i) = (x + 1..=self.max_x).position(|tx| {
            self.trees.get(&(tx, y)) >= h
        }) { 1 + i as u64 } else { (self.max_x - x) as u64 };

        // Up
        score *= if let Some(i) = (0..y).rev().position(|ty| {
            self.trees.get(&(x, ty)) >= h
        }) { 1 + i as u64 } else { y as u64 };

        // Down
        score *= if let Some(i) = (y + 1..=self.max_y).position(|ty| {
            self.trees.get(&(x, ty)) >= h
        }) { 1 + i as u64 } else { (self.max_y - y) as u64 };

        score
    }
}

fn num_visible_trees(data: &str) -> u64 {
    let forest = Forest::from_str(data);
    let mut count = 0;
    for x in 0..=forest.max_x {
        for y in 0..=forest.max_y {
            if forest.tree_is_visible(x, y) {
                count += 1;
            }
        }
    }
    count
}

fn get_max_scenic_score(data: &str) -> u64 {
    let forest = Forest::from_str(data);

    let mut max = 0;
    for x in 0..=forest.max_x {
        for y in 0..=forest.max_y {
            let s = forest.get_scenic_score(x, y);
            if s > max {
                max = s;
            }
        }
    }
    max
}

fn main() {
    assert_eq!(num_visible_trees(SAMPLE), 21);
    println!("Number of visible trees: {}", num_visible_trees(INPUT));

    assert_eq!(get_max_scenic_score(SAMPLE), 8);
    println!("Max scenic score: {}", get_max_scenic_score(INPUT));
}

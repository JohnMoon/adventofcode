const INPUT: &str = include_str!("input.txt");
const SAMPLE: &str = include_str!("sample.txt");

#[derive(Eq, PartialEq, Copy, Clone)]
enum Shape {R, P, S}

impl Shape {
    fn from_str(s: &str) -> Shape {
        match s {
            "A" | "X" => Shape::R,
            "B" | "Y" => Shape::P,
            "C" | "Z" => Shape::S,
            _ => panic!("Invalid input!")
        }
    }

    fn val(&self) -> u64 {
        match self {
            Shape::R => 1,
            Shape::P => 2,
            Shape::S => 3,
        }
    }

    fn wins_against(&self) -> Shape {
        match self {
            Shape::R => Shape::S,
            Shape::P => Shape::R,
            Shape::S => Shape::P,
        }
    }

    fn draws_against(&self) -> Shape {
        *self
    }

    fn loses_against(&self) -> Shape {
        match self {
            Shape::R => Shape::P,
            Shape::P => Shape::S,
            Shape::S => Shape::R,
        }
    }

    fn vs(&self, other: &Shape) -> u64 {
        self.val() + if self.wins_against() == *other {
            6
        } else if self.draws_against() == *other {
            3
        } else {
            0
        }
    }
}

fn run_strat(guide: &str) -> u64 {
    let move_set: Vec<(Shape, Shape)> = guide
        .lines()
        .map(|l| {
            let mut split = l.split_whitespace();
            (Shape::from_str(split.next().unwrap()), Shape::from_str(split.next().unwrap()))
        })
        .collect();

    move_set.iter()
            .fold(0, |acc, (other, me)| acc + me.vs(other))
}

fn run_strat_v2(guide: &str) -> u64 {
    let move_set: Vec<(Shape, Shape)> = guide
        .lines()
        .map(|l| {
            let mut split = l.split_whitespace();
            let other = Shape::from_str(split.next().unwrap());
            let result = split.next().unwrap();
            let me = match result {
                "X" => other.wins_against(),
                "Y" => other.draws_against(),
                "Z" => other.loses_against(),
                _ => panic!("Bad result!")
            };
            (other, me)
        })
        .collect();

    move_set.iter()
            .fold(0, |acc, (other, me)| acc + me.vs(other))
}

fn main() {
    assert_eq!(run_strat(SAMPLE), 15);
    println!("Total score following strategy 1: {}", run_strat(INPUT));

    assert_eq!(run_strat_v2(SAMPLE), 12);
    println!("Total score following strategy 2: {}", run_strat_v2(INPUT));
}

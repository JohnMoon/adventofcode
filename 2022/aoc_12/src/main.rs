const INPUT: &str = include_str!("input.txt");
const SAMPLE: &str = include_str!("sample.txt");

use std::collections::HashMap;

struct Node {
    altitude: u32,
    cached_paths: Vec<Option<Vec<(usize, usize)>>>,
}

struct Grid {
    // Grid is a map of points -> Node tuples
    grid: HashMap<(usize, usize), Node>,
    start: (usize, usize),
    goal: (usize, usize),
}

impl Grid {
    fn new() -> Grid {
        Grid {
            grid: HashMap::new(),
            start: (0, 0),
            goal: (0, 0),
        }
    }

    fn from_str(data: &str) -> Grid {
        let mut ret = Grid::new();
        for (y, line) in data.lines().enumerate() {
            for (x, c) in line.chars().enumerate() {
                let altitude = if c == 'S' {
                    ret.start = (x, y);
                    0
                } else if c == 'E' {
                    ret.goal = (x, y);
                    'z' as u32 - 'a' as u32
                } else {
                    c as u32 -  'a' as u32
                };
                ret.grid.insert((x, y), Node { altitude, cached_paths: Vec::new() });
            }
        }

        ret
    }

    fn get_fewest_steps_to_goal(&mut self, start_pt: (usize, usize), visited: &[(usize, usize)]) -> Option<Vec<(usize, usize)>> {
        // Add this node to the current path
        let new_visited = [visited, &[start_pt]].concat();
        //println!("Depth: {}", new_visited.len());

        // Base case - we ARE the goal
        if start_pt == self.goal {
            return Some(new_visited);
        }

        // Check to see if any of our cached results will work
        //let mut cache_min_path: Option<Vec<(usize, usize)>> = None;
        //for cached_path in &self.grid.get(&start_pt).unwrap().cached_paths {
        //    if let Some(cp) = cached_path {
        //        if cp.iter().any(|p| visited.contains(p)) {
        //            continue;
        //        }

        //        if let Some(cmp) = &cache_min_path {
        //            if cp.len() < cmp.len() {
        //                cache_min_path = Some(cp.clone());
        //            }
        //        } else {
        //            cache_min_path = Some(cp.clone());
        //        }
        //    }
        //}

        //if cache_min_path.is_some() {
        //    println!("Cache hit");
        //    return cache_min_path;
        //}

        let mut points_to_check = Vec::new();

        let start_alt = self.grid.get(&start_pt).unwrap().altitude;

        // Check each valid adjacent node
        let adj_pts = [(start_pt.0 - 1, start_pt.1), (start_pt.0 + 1, start_pt.1), (start_pt.0, start_pt.1 - 1), (start_pt.0, start_pt.1 + 1)];
        for adj_pt in &adj_pts {
            if self.grid.contains_key(adj_pt) && ! visited.contains(adj_pt) { 
                let adj_alt = self.grid.get(adj_pt).unwrap().altitude;
                if adj_alt <= (start_alt + 1) {
                    points_to_check.push(adj_pt)
                }
            }
        }

        // If there are no nodes to visit, there's no path to the goal!
        if points_to_check.is_empty() {
            return None;
        }

        let mut min_path: Option<Vec<(usize, usize)>> = None;
        for p in points_to_check {
            if let Some(path) = self.get_fewest_steps_to_goal(*p, &new_visited) {
                if let Some(mp) = &min_path {
                    if path.len() < mp.len() {
                        min_path = Some(path);
                    }
                } else {
                    min_path = Some(path);
                }
            }
        }
        //self.grid.get_mut(&start_pt).unwrap().cached_paths.push(min_path.clone());
        min_path
    }
}

fn fewest_steps(data: &str) -> usize {
    let mut grid = Grid::from_str(data);
    let path = grid.get_fewest_steps_to_goal(grid.start, &Vec::new()).unwrap();
    println!("Path: {:?}", path);
    path.len() - 1
}

fn main() {
    assert_eq!(fewest_steps(SAMPLE), 31);
    println!("Fewest steps: {}", fewest_steps(INPUT))
}
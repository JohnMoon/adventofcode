const INPUT: &str = include_str!("input.txt");
const SAMPLE: &str = include_str!("sample.txt");

fn get_char_val(c: char) -> u64 {
    let (base_c, base_n) = if c.is_lowercase() {
        ('a', 1)
    } else {
        ('A', 27)
    };

    c as u64 - base_c as u64 + base_n
}

fn get_sum(data: &str) -> u64 {
    data.lines()
        .fold(0, |acc, l| {
            let (comp0, comp1) = l.split_at(l.len() / 2);
            let in_both = comp0.chars().find(|&c| {
                comp1.contains(c)
            }).unwrap();
            acc + get_char_val(in_both)
        })
}

fn get_sum_v2(data: &str) -> u64 {
    let lines: Vec<&str> = data.lines().collect();
    let mut sum = 0;
    for n in (0..lines.len()).step_by(3) {
        let group = &lines[n..n + 3];
        let in_all = group[0].chars().find(|&c| {
            group[1].contains(c) && group[2].contains(c)
        }).unwrap();
        sum += get_char_val(in_all);
    }
    sum
}

fn main() {
    assert_eq!(get_sum(SAMPLE), 157);
    println!("Priority sum: {}", get_sum(INPUT));

    assert_eq!(get_sum_v2(SAMPLE), 70);
    println!("Priority sum v2: {}", get_sum_v2(INPUT));
}

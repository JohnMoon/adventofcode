const INPUT: &str = include_str!("input.txt");
const SAMPLE: &str = include_str!("sample.txt");

fn sig_strength_sum(data: &str) -> i64 {
    let mut sig_strengths = Vec::new();
    let mut reg_x: i64 = 1;
    let mut cycles: i64 = 1;
    for line in data.lines() {
        if cycles == 20 || ((cycles - 20) % 40 == 0) {
            sig_strengths.push(cycles * reg_x);
        }

        if line.starts_with("addx") {
            cycles += 1;
            if cycles == 20 || ((cycles - 20) % 40 == 0) {
                sig_strengths.push(cycles * reg_x);
            }
            reg_x += line.split_once(' ').unwrap().1.parse::<i64>().unwrap();
        }
        cycles += 1;
    }

    sig_strengths.iter().sum()
}

fn draw_crt(data: &str) {
    let mut reg_x: i64 = 1;
    let mut cycles: i64 = 1;
    for line in data.lines() {
        if cycles % 40 == 1 {
            println!();
        }
        if (cycles - 1) % 40 == reg_x ||
           (cycles - 1) % 40 == reg_x - 1 ||
           (cycles - 1) % 40 == reg_x + 1
        {
            print!("#");
        } else {
            print!(".");
        }

        if line.starts_with("addx") {
            cycles += 1;
            if cycles % 40 == 1 {
                println!();
            }
            if (cycles - 1) % 40 == reg_x ||
               (cycles - 1) % 40 == reg_x - 1 ||
               (cycles - 1) % 40 == reg_x + 1
            {
                print!("#");
            } else {
                print!(".");
            }

            reg_x += line.split_once(' ').unwrap().1.parse::<i64>().unwrap();
        }
        cycles += 1;
    }
    println!()
}

fn main() {
    assert_eq!(sig_strength_sum(SAMPLE), 13140);
    println!("Sig strength sum: {}", sig_strength_sum(INPUT));

    draw_crt(SAMPLE);
    draw_crt(INPUT);
}

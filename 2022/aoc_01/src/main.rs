const INPUT: &str = include_str!("input.txt");
const SAMPLE: &str = include_str!("sample.txt");

fn get_cal_list(data: &str) -> Vec<u64> {
    data.split("\n\n")
        .map(|g| {
            g.lines()
             .fold(0, |acc, l| acc + l.parse::<u64>().unwrap())
        })
        .collect()
}

fn get_max(data: &str) -> u64 {
    *get_cal_list(data).iter().max().unwrap()
}

fn get_top_three_max(data: &str) -> u64 {
    let mut list = get_cal_list(data);
    list.sort_unstable();
    list.iter().rev().take(3).sum()
}

fn main() {
    assert_eq!(get_max(SAMPLE), 24_000);
    println!("The max calories any elf is carying is {}", get_max(INPUT));

    assert_eq!(get_top_three_max(SAMPLE), 45_000);
    println!("The max calories the top three elves are carying is {}", get_top_three_max(INPUT));
}

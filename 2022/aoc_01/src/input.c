#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#define SAMPLE "sample.txt"
#define INPUT "input.txt"

char *get_input(char *path) {
    char *buffer = 0;
    long length;
    FILE *f = fopen(path, "rb");

    if (f) {
        fseek(f, 0, SEEK_END);
        length = ftell(f);
        fseek(f, 0, SEEK_SET);
        buffer = malloc(length);
        if (buffer)
            fread(buffer, 1, length, f);
        fclose (f);
    }

    return buffer;
}

char *split(char *str, const char *delim)
{
    char *p = strstr(str, delim);

    if (p == NULL)
        return NULL;

    *p = '\0';
    return p + strlen(delim);
}

uint64_t *get_cal_list(char *data) {


}

uint64_t get_max(char *data) {


    free(data);
    return 0;
}

int main() {
    uint64_t sample_val = get_max(get_input(SAMPLE));
    return 0;
}

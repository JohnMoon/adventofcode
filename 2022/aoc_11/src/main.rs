const INPUT: &str = include_str!("input.txt");
const SAMPLE: &str = include_str!("sample.txt");

use std::collections::HashMap;

struct Monkey {
    id: usize,
    items: Vec<u64>,
    op: (String, String),
    test_div_by: u64,
    if_true_dst: usize,
    if_false_dst: usize,
    num_inspections: u64,
    div_three: bool,
}

impl Monkey {
    fn from_lines(lines: Vec<&str>, div_three: bool) -> Monkey {
        let mut ret = Monkey {
            id: 0,
            items: Vec::new(),
            op: (String::new(), String::new()),
            test_div_by: 0,
            if_true_dst: 0,
            if_false_dst: 0,
            num_inspections: 0,
            div_three,
        };

        let trimmed_lines = lines.iter().map(|l| l.trim_start());
        for line in trimmed_lines {
            if line.starts_with("Monkey") {
                ret.id = line.chars().nth(7).unwrap().to_digit(10).unwrap() as usize;
            }
            else if line.starts_with("Starting") {
                ret.items = line[16..].split(',')
                    .map(|x| {
                        x.trim().parse().unwrap()
                    }).collect();
            }
            else if line.starts_with("Operation") {
                let (op, var) = line[21..].split_once(' ').unwrap();
                ret.op.0 = String::from(op);
                ret.op.1 = String::from(var);
            }
            else if line.starts_with("Test") {
                ret.test_div_by = line.split_whitespace().last().unwrap().parse().unwrap();
            }
            else if line.starts_with("If true") {
                ret.if_true_dst = line.split_whitespace().last().unwrap().parse().unwrap();
            }
            else if line.starts_with("If false") {
                ret.if_false_dst = line.split_whitespace().last().unwrap().parse().unwrap();
            }
        }

        ret
    }
}

fn make_monkey_set(data: &str, div_three: bool) -> HashMap<usize, Monkey> {
    let mut ret = HashMap::new();

    for line_group in data.split("\n\n") {
        let monkey = Monkey::from_lines(line_group.lines().collect(), div_three);
        ret.insert(monkey.id, monkey);
    }

    ret
}

fn run_round(monkey_set: &mut HashMap<usize, Monkey>) {
    // For part 2, taking the product of the divisibility tests allows us to keep the 
    // worry levels manageable
    let combined_mod = monkey_set.values().fold(1, |acc, m| m.test_div_by * acc);
    for id in 0..monkey_set.len() {
        loop {
            let m = monkey_set.get_mut(&id).unwrap();
            if m.items.is_empty() {
                break;
            }

            let item = m.items.remove(0);
            let op_val = m.op.1.parse::<u64>().unwrap_or(item);

            let mut worry_level = match m.op.0.as_str() {
                "+" => {
                    item.checked_add(op_val).unwrap()
                },
                "*" => {
                    //println!("{} * {}", item, op_val);
                    item.checked_mul(op_val).unwrap()
                },
                _ => panic!("Uncovered op!")
            };

            if m.div_three {
                worry_level /= 3;
            } else {
                worry_level %= combined_mod;
            }

            m.num_inspections = m.num_inspections.checked_add(1).unwrap();

            let dst = if worry_level % m.test_div_by == 0 {
                m.if_true_dst
            } else {
                m.if_false_dst
            };

            monkey_set.get_mut(&dst).unwrap().items.push(worry_level);
        }
    }
}

fn calc_monkey_business(data: &str, rounds: usize, div_three: bool) -> u64 {
    let mut monkey_set = make_monkey_set(data, div_three);

    for _ in 0..rounds {
        run_round(&mut monkey_set);
    }

    let mut monkey_inspection_list = Vec::new();
    for m in monkey_set.values() {
        monkey_inspection_list.push(m.num_inspections as u64);
    }
    monkey_inspection_list.sort_unstable();
    monkey_inspection_list.iter().rev().take(2).fold(1, |acc, v| acc.checked_mul(*v).unwrap())
}

fn main() {
    assert_eq!(calc_monkey_business(SAMPLE, 20, true), 10605);
    println!("Monkey business: {}", calc_monkey_business(INPUT, 20, true));

    assert_eq!(calc_monkey_business(SAMPLE, 10_000, false), 2_713_310_158);
    println!("Monkey business: {}", calc_monkey_business(INPUT, 10_000, false));
}
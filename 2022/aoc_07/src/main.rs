const INPUT: &str = include_str!("input.txt");
const SAMPLE: &str = include_str!("sample.txt");

use std::env;
use std::fs;
use std::io::Write;
use std::collections::HashMap;

fn get_root_dir_from_input(data: &str) -> String {
    let mut root = env::temp_dir();
    root.push("aoc_07");

    let _ = fs::remove_dir_all(&root);
    fs::create_dir_all(&root).unwrap();
    env::set_current_dir(&root).unwrap();

    // Skip the first line which changes to root
    let mut lines = data.lines().skip(1);
    let mut line = lines.next();

    loop {
        if line.is_none() {
            break;
        }

        let split: Vec<&str> = line.unwrap().split_whitespace().collect();
        let cmd = split.get(1).unwrap();
        match *cmd {
            "cd" => {
                let dst = split.get(2).unwrap();
                fs::create_dir_all(dst).unwrap();
                env::set_current_dir(dst).unwrap();
                line = lines.next();
            },
            "ls" => {
                line = lines.next();
                while line.is_some() && ! line.unwrap().starts_with('$') {
                    let (e1, e2) = line.unwrap().split_once(' ').unwrap();
                    if e1 == "dir" {
                        fs::create_dir_all(e2).unwrap();
                    } else {
                        // Write the file "size" to the file
                        let mut new_f = fs::File::create(e2).unwrap();
                        writeln!(new_f, "{}", e1).unwrap();
                    }
                    line = lines.next();
                }
            },
            _ => panic!("invalid cmd")
        };
    }

    String::from(root.to_str().unwrap())
}

fn get_dir_size(dir_path: String, path_sizes: &mut HashMap<String, usize>) -> usize {
    let mut acc = 0;
    for p in fs::read_dir(&dir_path).unwrap() {
        let path = p.unwrap().path();
        let md = fs::metadata(&path).unwrap();
        if md.is_dir() {
            acc += get_dir_size(String::from(path.to_str().unwrap()), path_sizes);
        } else {
            let fstr = fs::read_to_string(path).unwrap();
            let fsize = fstr.trim().parse::<usize>().unwrap();
            acc += fsize;
        }
    }
    path_sizes.insert(dir_path, acc);
    acc
}

fn find_total_sizes_lt(data: &str, lt: usize) -> usize {
    let root = get_root_dir_from_input(data);
    let mut path_sizes: HashMap<String, usize> = HashMap::new();

    let _ = get_dir_size(root, &mut path_sizes);

    let mut sum = 0;
    for size in path_sizes.values() {
        if size <= &lt {
            sum += size;
        }
    }

    sum
}

fn find_smallest_dir_to_del(data: &str) -> usize {
    let root = get_root_dir_from_input(data);
    let mut path_sizes: HashMap<String, usize> = HashMap::new();

    let root_size = get_dir_size(root, &mut path_sizes);

    let unused_space = 70_000_000 - root_size;
    let needed_space = 30_000_000;
    let need_to_free = needed_space - unused_space;

    let mut smallest = root_size;
    let mut smallest_path = "/";
    for (path, size) in &path_sizes {
        if size >= &need_to_free && size < &smallest {
            smallest = *size;
            smallest_path = path;
        }
    }
    println!("Smallest path is {} ({})", smallest_path, smallest);


    smallest
}


fn main() {
    assert_eq!(find_total_sizes_lt(SAMPLE, 100_000), 95_437);
    println!("Total size less than 100,000: {}", find_total_sizes_lt(INPUT, 100_000));

    assert_eq!(find_smallest_dir_to_del(SAMPLE), 24_933_642);
    println!("Smallest size to del: {}", find_smallest_dir_to_del(INPUT));
}

const INPUT: &str = include_str!("input.txt");
const SAMPLE: &str = include_str!("sample.txt");
const SAMPLE_TWO: &str = include_str!("sample_two.txt");

use std::collections::HashSet;

struct Rope {
    nodes: Vec<(i64, i64)>,
    tail_visited_points: HashSet<(i64, i64)>,
}

impl Rope {
    fn run_move(&mut self, dir: &str, n: usize) {
        for _ in 0..n {
            let head = &mut self.nodes[0];
            match dir {
                "R" => {
                    *head = (head.0 + 1, head.1);
                },
                "L" => {
                    *head = (head.0 - 1, head.1);
                },
                "U" => {
                    *head = (head.0, head.1 + 1);
                },
                "D" => {
                    *head = (head.0, head.1 - 1);
                },
                _ => panic!("Bad direction")
            };

            for i in 1..self.nodes.len() {
                let last = self.nodes[i - 1];
                let curr = &mut self.nodes[i];

                // If we're already touching, no action needed
                if (last.0 - curr.0).abs() <= 1 && (last.1- curr.1).abs() <= 1 {
                    continue;
                }

                // If we're in the same row or column, easy, just fill the gap
                if last.0 == curr.0 {
                    if last.1 > curr.1 {
                        curr.1 = last.1 - 1;
                    } else {
                        curr.1 = last.1 + 1;
                    }
                    continue;
                }
                if last.1 == curr.1 {
                    if last.0 > curr.0 {
                        curr.0 = last.0 - 1;
                    } else {
                        curr.0 = last.0 + 1;
                    }
                    continue;
                }

                // Otherwise, if the nodes aren't touching and aren't in the same
                // row or column, the curr always moves one step diagonally to
                // keep up with last
                // .....    .....    .....
                // .....    ..L..    ..L..
                // ..L.. -> ..... -> ..C..
                // .C...    .C...    .....
                // .....    .....    .....
                //
                // .....    .....    .....
                // .....    .....    .....
                // ..L.. -> ...L. -> ..CL.
                // .C...    .C...    .....
                // .....    .....    .....
                if last.0 > curr.0 {
                    curr.0 += 1;
                } else {
                    curr.0 -= 1;
                }
                if last.1 > curr.1 {
                    curr.1 += 1;
                } else {
                    curr.1 -= 1;
                }
            }

            self.tail_visited_points.insert(*self.nodes.last().unwrap());
        }

        //self.tail_visited_points.insert(*self.nodes.last().unwrap());
    }
}

fn num_visited_positions(data: &str, n_nodes: usize) -> usize {
    let mut rope = Rope { nodes: vec![(0, 0); n_nodes], tail_visited_points: HashSet::new() };
    for line in data.lines() {
        let (dir, n_str) = line.split_once(' ').unwrap();
        let n = n_str.parse::<usize>().unwrap();
        rope.run_move(dir, n);
    }

    rope.tail_visited_points.len()
}

fn main() {
    assert_eq!(num_visited_positions(SAMPLE, 2), 13);
    println!("Number of visited positions: {}", num_visited_positions(INPUT, 2));

    assert_eq!(num_visited_positions(SAMPLE, 10), 1);
    assert_eq!(num_visited_positions(SAMPLE_TWO, 10), 36);
    println!("Number of visited positions: {}", num_visited_positions(INPUT, 10));
}

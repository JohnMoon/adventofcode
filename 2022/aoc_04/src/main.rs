const INPUT: &str = include_str!("input.txt");
const SAMPLE: &str = include_str!("sample.txt");

fn get_pairs(data: &str) -> Vec<((u64, u64), (u64, u64))> {
    data.lines()
        .map(|l| {
            let (e1, e2) = l.split_once(',').unwrap();
            let (e1_start, e1_end) = e1.split_once('-').unwrap();
            let (e2_start, e2_end) = e2.split_once('-').unwrap();
            (
                (e1_start.parse::<u64>().unwrap(), e1_end.parse::<u64>().unwrap()),
                (e2_start.parse::<u64>().unwrap(), e2_end.parse::<u64>().unwrap()),
            )
        }).collect()
}

fn get_num_overlaps(data: &str, full: bool) -> u64 {
    let pairs = get_pairs(data);
    let mut count = 0;
    for (e1, e2) in pairs {
        let e1_range: Vec<u64> = (e1.0..=e1.1).collect();
        let e2_range: Vec<u64> = (e2.0..=e2.1).collect();

        // Full overlaps
        if full {
            if e1_range.iter().all(|x| e2_range.contains(x)) ||
               e2_range.iter().all(|x| e1_range.contains(x))
            {
                count += 1;
            }
        // Partial overlaps
        } else if e1_range.iter().any(|x| e2_range.contains(x)) {
            count += 1;
        }
    }
    count
}

fn main() {
    assert_eq!(get_num_overlaps(SAMPLE, true), 2);
    println!("Number of full overlaps: {}", get_num_overlaps(INPUT, true));

    assert_eq!(get_num_overlaps(SAMPLE, false), 4);
    println!("Number of overlaps: {}", get_num_overlaps(INPUT, false));

}

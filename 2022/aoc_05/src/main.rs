const INPUT: &str = include_str!("input.txt");
const SAMPLE: &str = include_str!("sample.txt");
const ASDF: &str = include_str!("asdf.txt");

struct Move {
    qty: usize,
    src: usize,
    dst: usize,
}

struct Stacks {
    stacks: Vec<Vec<char>>,
    moves: Vec<Move>,
}

impl Stacks {
    fn from_str(data: &str) -> Stacks {
        let lines: Vec<&str> = data.lines().collect();
        let line_len = lines[0].len();
        let n_stacks = (line_len / 4) + 1;

        let mut stacks = Vec::with_capacity(n_stacks);
        for _ in 0..n_stacks {
            stacks.push(Vec::new());
        }

        let mut moves = Vec::new();
        for l in lines {
            if l.is_empty() {
                continue;
            }

            if ! l.starts_with("move") {
                if l.contains('1') {
                    continue;
                }

                for (n, stack) in stacks.iter_mut().enumerate().take(n_stacks) {
                    let crate_char: char = l.chars().nth((n * 4) + 1).unwrap();
                    if crate_char != ' ' {
                        stack.insert(0, crate_char);
                    }
                }
            } else {
                // "move <qty> from <src> to <dst>"
                let move_split: Vec<&str> = l.split_whitespace().collect();
                moves.push(Move {
                    qty: move_split[1].parse::<usize>().unwrap(),
                    src: move_split[3].parse::<usize>().unwrap() - 1,
                    dst: move_split[5].parse::<usize>().unwrap() - 1,
                });
            }
        }

        Stacks {stacks, moves}
    }

    fn get_top_crate_msg(&self) -> String {
        let mut top_chars = Vec::new();
        for s in &self.stacks {
            if let Some(c) = s.last() {
                top_chars.push(c);
            }
        }

        String::from_iter(top_chars)
    }

    fn run_moves_p1(&mut self) {
        for m in &mut self.moves {
            for _ in 0..m.qty {
                let c = self.stacks[m.src].pop().unwrap();
                self.stacks[m.dst].push(c);
            }
        }
    }

    fn run_moves_p2(&mut self) {
        for m in &mut self.moves {
            let mut to_move = Vec::new();
            for _ in 0..m.qty {
                let c = self.stacks[m.src].pop().unwrap();
                to_move.push(c);
            }

            for _ in 0..m.qty {
                let c = to_move.pop().unwrap();
                self.stacks[m.dst].push(c);
            }
        }
    }
}

fn run_p1(data: &str) -> String {
    let mut stacks = Stacks::from_str(data);
    stacks.run_moves_p1();
    stacks.get_top_crate_msg()
}

fn run_p2(data: &str) -> String {
    let mut stacks = Stacks::from_str(data);
    stacks.run_moves_p2();
    stacks.get_top_crate_msg()
}

fn main() {
    assert_eq!(&run_p1(SAMPLE), "CMZ");
    println!("P1 message: {}", run_p1(INPUT));

    assert_eq!(&run_p2(SAMPLE), "MCD");
    println!("P2 message: {}", run_p2(INPUT));

    println!("AoC message: {}", run_p1(ASDF));
    println!("AoC message: {}", run_p2(ASDF));
}

const INPUT: &str = include_str!("input.txt");
const SAMPLE: &str = include_str!("sample.txt");

#[derive(Clone, Copy, Debug)]
struct Round {
    red: u32,
    green: u32,
    blue: u32,
}

#[derive(Clone, Debug)]
struct Game {
    id: u32,
    rounds: Vec<Round>,
}

impl Game {
    fn from_str(s: &str) -> Game {
        // Example:
        // Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
        let (game_id, rest) = s.split_once(": ").unwrap();
        let id = game_id
            .replace("Game ", "")
            .replace(':', "")
            .parse::<u32>()
            .unwrap();

        let rest_trimmed = rest.replace(", ", ",").replace("; ", ";");
        let mut rounds = Vec::new();
        for round_line in rest_trimmed.split(';') {
            let mut red = 0;
            let mut green = 0;
            let mut blue = 0;
            for pick in round_line.split(',') {
                let (n_str, color) = pick.split_once(' ').unwrap();
                match color {
                    "red" => red += n_str.parse::<u32>().unwrap(),
                    "green" => green += n_str.parse::<u32>().unwrap(),
                    "blue" => blue += n_str.parse::<u32>().unwrap(),
                    _ => panic!("Invalid color"),
                }
            }
            rounds.push(Round { red, green, blue });
        }

        Game { id, rounds }
    }

    fn get_power(&self) -> u32 {
        let red_max = self.rounds.iter().fold(0, |acc, r| r.red.max(acc));
        let green_max = self.rounds.iter().fold(0, |acc, r| r.green.max(acc));
        let blue_max = self.rounds.iter().fold(0, |acc, r| r.blue.max(acc));

        red_max * green_max * blue_max
    }
}

fn get_sum_of_possible_ids(input: &str, red: u32, green: u32, blue: u32) -> u32 {
    input
        .lines()
        .map(Game::from_str)
        .filter(|g| {
            g.rounds
                .iter()
                .all(|&r| r.red <= red && r.green <= green && r.blue <= blue)
        })
        .fold(0, |acc, g| acc + g.id)
}

fn get_sum_of_game_powers(input: &str) -> u32 {
    let games: Vec<Game> = input.lines().map(Game::from_str).collect();

    games.iter().fold(0, |acc, g| acc + g.get_power())
}

fn main() {
    assert_eq!(get_sum_of_possible_ids(SAMPLE, 12, 13, 14), 8);
    println!(
        "Sum of possible IDs (p1): {}",
        get_sum_of_possible_ids(INPUT, 12, 13, 14)
    );

    assert_eq!(get_sum_of_game_powers(SAMPLE), 2286);
    println!("Sum of game powers (p2): {}", get_sum_of_game_powers(INPUT));
}

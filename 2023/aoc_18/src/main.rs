use std::{collections::HashSet, f64::consts::PI};

const INPUT: &str = include_str!("input.txt");
const SAMPLE: &str = include_str!("sample.txt");
const SAMPLE2: &str = include_str!("sample2.txt");

#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash)]
enum Direction {
    Up,
    Down,
    Left,
    Right,
}

#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash, PartialOrd, Ord)]
struct Point {
    x: i64,
    y: i64,
}

impl Point {
    fn new(x: i64, y: i64) -> Point {
        Point { x, y }
    }
}

#[derive(Clone, Debug)]
struct Grid {
    grid: HashSet<Point>,
    circuit: Vec<Point>,
    x_max: i64,
    x_min: i64,
    y_max: i64,
    y_min: i64,
}

impl std::fmt::Display for Grid {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for y in self.y_min..=self.y_max {
            for x in self.x_min..=self.x_max {
                if self.grid.contains(&Point::new(x, y)) {
                    write!(f, "#")?;
                } else {
                    write!(f, ".")?;
                }
            }
            writeln!(f)?;
        }
        Ok(())
    }
}

fn hex_to_instruction(s: &str) -> (Direction, i64) {
    // (#70c710) = R 461937
    // Each hexadecimal code is six hexadecimal digits long.
    // The first five hexadecimal digits encode the distance
    // in meters as a five-digit hexadecimal number. The last
    // hexadecimal digit encodes the direction to dig:
    // 0 means R, 1 means D, 2 means L, and 3 means U.
    let hex_str = s.replace(['(', ')', '#'], "");
    let (dist_hex_str, dir_hex_str) = hex_str.split_at(5);
    let dist = i64::from_str_radix(dist_hex_str, 16).unwrap();
    let dir = match dir_hex_str {
        "0" => Direction::Right,
        "1" => Direction::Down,
        "2" => Direction::Left,
        "3" => Direction::Up,
        _ => panic!("Unsupported direction!"),
    };

    if s == "(#70c710)" {
        assert_eq!((dir, dist), (Direction::Right, 461937));
    }

    (dir, dist)
}

impl Grid {
    fn from_str(input: &str, hex_instructions: bool) -> Grid {
        let mut grid = HashSet::new();
        let mut circuit = Vec::new();

        let mut point = Point::new(0, 0);
        let mut x_max = 0;
        let mut x_min = 0;
        let mut y_max = 0;
        let mut y_min = 0;

        grid.insert(point);
        circuit.push(point);

        for line in input.lines() {
            let tokens: Vec<&str> = line.split_whitespace().collect();
            let (dir, n_steps) = if !hex_instructions {
                let dir = match tokens[0] {
                    "U" => Direction::Up,
                    "D" => Direction::Down,
                    "L" => Direction::Left,
                    "R" => Direction::Right,
                    _ => panic!("Invalid direction"),
                };
                let n_steps = tokens[1].parse::<i64>().unwrap();
                (dir, n_steps)
            } else {
                hex_to_instruction(tokens[2])
            };

            let old_point = point;
            match dir {
                Direction::Up => point = Point::new(point.x, point.y - n_steps),
                Direction::Down => point = Point::new(point.x, point.y + n_steps),
                Direction::Left => point = Point::new(point.x - n_steps, point.y),
                Direction::Right => point = Point::new(point.x + n_steps, point.y),
            }

            circuit.push(point);

            x_max = std::cmp::max(point.x, x_max);
            x_min = std::cmp::min(point.x, x_min);
            y_max = std::cmp::max(point.y, y_max);
            y_min = std::cmp::min(point.y, y_min);

            if old_point.y > point.y {
                for y in point.y..=old_point.y {
                    grid.insert(Point::new(point.x, y));
                }
            } else if old_point.y < point.y {
                for y in old_point.y..=point.y {
                    grid.insert(Point::new(point.x, y));
                }
            } else if old_point.x > point.x {
                for x in point.x..=old_point.x {
                    grid.insert(Point::new(x, point.y));
                }
            } else if old_point.x < point.x {
                for x in old_point.x..=point.x {
                    grid.insert(Point::new(x, point.y));
                }
            }
        }

        Grid {
            grid,
            circuit,
            x_max,
            x_min,
            y_max,
            y_min,
        }
    }

    fn get_num_enclosed_points(&self) -> u64 {
        let mut sum = self.grid.len() as u64;
        let mut checks = 0;

        // TODO: Have to make this faster. Would take about a week to run
        // on my laptop. Extend enclosed points in all directions to the
        // grid edges and keep track?
        println!(
            "{} points to check",
            (self.x_max - self.x_min).abs() * (self.y_max - self.y_min).abs()
        );
        for x in self.x_min..self.x_max {
            let mut in_enclosure = false;
            for y in self.y_min..self.y_max {
                checks += 1;
                if checks % 10_000_000 == 0 {
                    println!("Checked {}", checks);
                }
                let p = Point::new(x, y);
                if !in_enclosure {
                    if self.point_is_enclosed(p) {
                        in_enclosure = true;
                        sum += 1;
                    }
                } else if self.grid.contains(&p) {
                    in_enclosure = false;
                    continue;
                } else {
                    sum += 1;
                }
            }
        }
        sum
    }

    fn point_is_enclosed(&self, p: Point) -> bool {
        if self.grid.contains(&p) {
            return false;
        }

        let start_angle = self.get_angle_degs(p, self.circuit[0]);
        let mut angle = start_angle;
        let mut delta_angle_sum = 0.0;
        let mut last_p = self.circuit[0];
        let mut loops = 0.0;
        for circuit_p in self.circuit.iter().skip(1) {
            if p.x > circuit_p.x {
                if circuit_p.y <= p.y && last_p.y > p.y {
                    loops += 1.0;
                } else if circuit_p.y > p.y && last_p.y <= p.y {
                    loops -= 1.0;
                }
            }
            let new_angle = self.get_angle_degs(p, *circuit_p) + (loops * 360.0);
            let delta_angle = new_angle - angle;
            angle = new_angle;
            delta_angle_sum += delta_angle;
            last_p = *circuit_p;
        }

        // The final sum should be pretty close to 0 or 360 (0 meaning not enclosed, 360 meaning enclosed)
        let final_angle_sum = delta_angle_sum.abs().round();
        assert!(
            (final_angle_sum > -2.0 && final_angle_sum < 2.0)
                || (final_angle_sum > 358.0 && final_angle_sum < 362.0)
        );

        final_angle_sum > 180.0
    }

    fn get_angle_degs(&self, p1: Point, p2: Point) -> f64 {
        let x_delta = (p2.x - p1.x) as f64;
        let y_delta = -(p2.y - p1.y) as f64;

        // First/fourth quadrant
        let angle = if x_delta >= 0.0 {
            (y_delta / x_delta).atan() * (180.0 / PI)
        // Second/third quadrant
        } else {
            ((y_delta / x_delta).atan() + PI) * (180.0 / PI)
        };

        if angle < 0.0 {
            angle + 360.0
        } else {
            angle
        }
    }

    fn get_lagoon_volume(&self) -> u64 {
        self.get_num_enclosed_points()
    }
}

fn lagoon_volume(input: &str, hex_instructions: bool) -> u64 {
    let grid = Grid::from_str(input, hex_instructions);
    grid.get_lagoon_volume()
}

fn main() {
    assert_eq!(lagoon_volume(SAMPLE, false), 62);
    assert_eq!(lagoon_volume(SAMPLE2, false), 62 + (8 * 6) - 1);
    println!("Lagoon volume (p1): {}", lagoon_volume(INPUT, false));

    assert_eq!(lagoon_volume(SAMPLE, true), 952_408_144_115);
    println!("Lagoon volume (p2): {}", lagoon_volume(INPUT, true));
}

const INPUT: &str = include_str!("input.txt");
const SAMPLE: &str = include_str!("sample.txt");

#[derive(Clone, Copy, Debug)]
struct Race {
    time_ms: u64,
    record_distance_mm: u64,
}

impl Race {
    fn get_ways_to_win(&self) -> u64 {
        (0..=self.time_ms).fold(0, |acc, hold_time_ms| {
            if (hold_time_ms * (self.time_ms - hold_time_ms)) > self.record_distance_mm {
                acc + 1
            } else {
                acc
            }
        })
    }
}

fn get_ways_to_win_product(input: &str) -> u64 {
    let times: Vec<u64> = input
        .lines()
        .next()
        .unwrap()
        .split_whitespace()
        .skip(1)
        .map(|n| n.parse::<u64>().unwrap())
        .collect();
    let dists: Vec<u64> = input
        .lines()
        .nth(1)
        .unwrap()
        .split_whitespace()
        .skip(1)
        .map(|n| n.parse::<u64>().unwrap())
        .collect();

    let mut races = Vec::new();
    for i in 0..times.len() {
        races.push(Race {
            time_ms: times[i],
            record_distance_mm: dists[i],
        });
    }

    races.iter().fold(1, |acc, r| r.get_ways_to_win() * acc)
}

fn get_ways_to_win(input: &str) -> u64 {
    let time_ms = input
        .lines()
        .next()
        .unwrap()
        .replace("Time:", "")
        .replace(' ', "")
        .parse::<u64>()
        .unwrap();
    let record_distance_mm = input
        .lines()
        .nth(1)
        .unwrap()
        .replace("Distance:", "")
        .replace(' ', "")
        .parse::<u64>()
        .unwrap();

    let race = Race {
        time_ms,
        record_distance_mm,
    };

    race.get_ways_to_win()
}

fn main() {
    assert_eq!(get_ways_to_win_product(SAMPLE), 288);
    println!(
        "Ways to win (multiplied together) (p1): {}",
        get_ways_to_win_product(INPUT)
    );

    assert_eq!(get_ways_to_win(SAMPLE), 71503);
    println!("Ways to win (p2): {}", get_ways_to_win(INPUT));
}

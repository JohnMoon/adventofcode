use std::{fmt, str::SplitTerminator};
const INPUT: &str = include_str!("input.txt");
const SAMPLE: &str = include_str!("sample.txt");

#[derive(Clone, Debug)]
struct Record {
    springs: Vec<char>,
    damaged_groups: Vec<usize>,
    damaged_sum: u32,
}

impl fmt::Display for Record {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        for c in &self.springs {
            write!(f, "{}", c)?;
        }
        write!(f, " ")?;
        for g in &self.damaged_groups {
            write!(f, "{}, ", g)?;
        }
        Ok(())
    }
}

impl Record {
    fn from_str(input: &str) -> Record {
        let (springs_str, groups_str) = input.split_once(' ').unwrap();
        let s = springs_str.to_string();
        let g = groups_str.to_string();

        let damaged_groups: Vec<usize> = g.split(',').map(|c| c.parse::<usize>().unwrap()).collect();
        let damaged_sum: usize = damaged_groups.iter().sum();
        Record {
            springs: s.chars().collect(),
            damaged_groups,
            damaged_sum: damaged_sum as u32,
        }
    }

    fn get_number_of_arrangements(&self, folded: bool) -> u64 {
        let wildcard_indices: Vec<usize> = self.springs.iter().enumerate().filter_map(|(i, &c)| (c == '?').then_some(i)).collect();
        let mut arrangement = self.springs.clone();
        let native_damaged = arrangement.iter().filter(|&c| *c == '#').count() as u32;
        let mut valid_spring_arrangements = Vec::new();

        // If bit 0 in combo_num is set, set wildcard index 0 to set
        // If bit 1 in combo_num is set, set wildcard index 1 to set, etc.
        for combo_num in 0..(2usize.pow(wildcard_indices.len() as u32)) {
            if combo_num.count_ones() + native_damaged != self.damaged_sum {
                continue;
            }
            for (i, wildcard_index) in wildcard_indices.iter().enumerate() {
                if combo_num & (1 << i) == 0 {
                    arrangement[*wildcard_index] = '.';
                } else {
                    arrangement[*wildcard_index] = '#';
                }
            }

            if self.arrangement_is_valid(&arrangement) {
                valid_spring_arrangements.push(arrangement.clone());
            }
        }

        if !folded {
            return valid_spring_arrangements.len() as u64;
        }

        let mut unfolded_records: Vec<Record> = Vec::new();

        for v_springs in &valid_spring_arrangements {
            let orig_springs = v_springs.clone();
            let orig_damaged_groups = self.damaged_groups.clone();
            let mut springs = orig_springs.clone();
            let mut damaged_groups = orig_damaged_groups.clone();
            for _ in 1..5 {
                springs.push('?');
                springs.append(&mut orig_springs.clone());
                damaged_groups.append(&mut orig_damaged_groups.clone());
            }
            let damaged_sum: usize = damaged_groups.iter().sum();
            println!("Springs: {:?}", springs);
            unfolded_records.push(Record {
                springs,
                damaged_groups,
                damaged_sum: damaged_sum as u32,
            })
        }

        unfolded_records.iter().fold(0, |acc, r| acc + r.get_number_of_arrangements(false)) * valid_spring_arrangements.len() as u64
    }

    fn arrangement_is_valid(&self, arrangement: &[char]) -> bool {
        let mut n_damaged_in_a_row = 0;
        let mut group_index = 0;
        let mut in_group = false;
        let mut group_size = 0;
        for c in arrangement {
            // If we've already fulfilled the requirements of the
            // group captures, run out the clock.
            if group_index == self.damaged_groups.len() {
                if *c == '#' {
                    return false;
                } else {
                    continue;
                }
            }

            group_size = self.damaged_groups[group_index];
            match c {
                '.' => {
                    if in_group {
                        if n_damaged_in_a_row != group_size {
                            return false;
                        } else {
                            group_index += 1;
                        }
                    }
                    n_damaged_in_a_row = 0;
                    in_group = false;
                },
                '#' => {
                    in_group = true;
                    n_damaged_in_a_row += 1;
                    if n_damaged_in_a_row > group_size {
                        return false;
                    }
                },
                _ => panic!("Unsupported char"),
            }
        }

        if in_group {
            if n_damaged_in_a_row != group_size {
                return false;
            } else {
                group_index += 1;
            }
        }

        if group_index != self.damaged_groups.len() {
            return false;
        }

        true
    }
}

fn sum_of_arrangements(input: &str, folded: bool) -> u64 {
    let records: Vec<Record> = input.lines().map(Record::from_str).collect();
    records.iter().fold(0, |acc, r| {
        let n =r.get_number_of_arrangements(folded);
        println!("{:?} -> {}", r.springs, n);
        acc + n
    })
}

fn main() {
    //assert_eq!(sum_of_arrangements(SAMPLE, false), 21);
    //println!("Sum of arrangements (p1): {}", sum_of_arrangements(INPUT, false));

    assert_eq!(sum_of_arrangements(SAMPLE, true), 525_152);
    println!("Test passed");
    //println!("Sum of unfolded arrangements (p2): {}", sum_of_arrangements(INPUT, true));
}

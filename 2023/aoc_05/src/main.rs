use std::collections::HashMap;

const INPUT: &str = include_str!("input.txt");
const SAMPLE: &str = include_str!("sample.txt");

#[derive(Debug, Clone)]
struct Map {
    src_id: String,
    dst_id: String,
    src_start: u64,
    src_end: u64,
    dst_start: u64,
    dst_end: u64,
    offset: i64,
}

impl Map {
    fn new(src_id: &str, dst_id: &str, d: u64, s: u64, r: u64) -> Map {
        Map {
            src_id: src_id.to_string(),
            dst_id: dst_id.to_string(),
            src_start: s,
            src_end: s + r,
            dst_start: d,
            dst_end: d + r,
            offset: d as i64 - s as i64,
        }
    }
}

#[derive(Debug, Clone)]
struct Almanac {
    seeds: Vec<u64>,
    // HashMap of dst_id to Vec<Map>
    almanac: HashMap<String, Vec<Map>>,
}

impl Almanac {
    fn from_str(input: &str) -> Almanac {
        let mut seeds = Vec::new();
        let mut almanac: HashMap<String, Vec<Map>> = HashMap::new();

        let mut current_src_id = String::new();
        let mut current_dst_id = String::new();
        for line in input.lines() {
            if line.starts_with("seeds:") {
                let (_, seeds_str) = line.split_once(' ').unwrap();
                seeds = seeds_str
                    .split_whitespace()
                    .map(|n| n.parse::<u64>().unwrap())
                    .collect();
                continue;
            }

            if line.contains("map:") {
                let (map_id_str, _) = line.split_once(" map:").unwrap();
                let (src_id_str, dst_id_str) = map_id_str.split_once("-to-").unwrap();
                current_src_id = src_id_str.to_string();
                current_dst_id = dst_id_str.to_string();
                continue;
            }

            if line.is_empty() {
                current_src_id.clear();
                current_dst_id.clear();
                continue;
            }

            let nums: Vec<u64> = line
                .splitn(3, ' ')
                .map(|n| n.parse::<u64>().unwrap())
                .collect();

            almanac.entry(current_dst_id.clone()).or_default().push(Map::new(
                &current_src_id,
                &current_dst_id,
                nums[0],
                nums[1],
                nums[2],
            ))
        }

        Almanac { seeds, almanac }
    }

    fn get_lowest_location_number_p1(&self) -> u64 {
        self.seeds.iter().fold(u64::MAX, |acc, s| {
            let loc_num = self.seed_to_location(*s);
            if  loc_num < acc {
                loc_num
            } else {
                acc
            }
        })
    }

    fn get_lowest_location_number_p2(&self) -> u64 {
        let mut location_number_candidate = 0;
        loop {
            let seed_num = self.location_to_seed(location_number_candidate);
            for seed_range in self.seeds.chunks(2) {
                if seed_num >= seed_range[0] && seed_num < seed_range[0] + seed_range[1] {
                    return location_number_candidate;
                }
            }
            location_number_candidate += 1;
        }
    }

    fn seed_to_location(&self, seed_number: u64) -> u64 {
        let mut current_num = seed_number;
        let mut dst = "soil".to_string();
        loop {
            for range in self.almanac.get(&dst).unwrap() {
                if current_num >= range.src_start && current_num < range.src_end {
                    current_num = (current_num as i64 + range.offset) as u64;
                    break;
                }
            }

            if let Some(dst_vec) = self.almanac.values().find(|&m| m[0].src_id == dst) {
                dst = dst_vec[0].dst_id.clone();
            } else {
                break;
            }
        }

        current_num
    }

    fn location_to_seed(&self, location_number: u64) -> u64 {
        let mut current_dst = "location";
        let mut current_n = location_number;

        loop {
            for map in self.almanac.get(current_dst).unwrap() {
                // We have a range, step to the next map
                if current_n >= map.dst_start && current_n < map.dst_end {
                    current_n = (current_n as i64 - map.offset) as u64;
                    break;
                }
            }
            current_dst = &self.almanac.get(current_dst).unwrap()[0].src_id;

            if current_dst == "seed" {
                break;
            }
        }
        current_n

    }
}

fn main() {
    assert_eq!(Almanac::from_str(SAMPLE).get_lowest_location_number_p1(), 35);
    println!(
        "lowest_location_number (p1): {}",
        Almanac::from_str(INPUT).get_lowest_location_number_p1()
    );

    assert_eq!(
        Almanac::from_str(SAMPLE).get_lowest_location_number_p2(),
        46
    );
    println!(
        "lowest_location_number (p2): {}",
        Almanac::from_str(INPUT).get_lowest_location_number_p2()
    );
}

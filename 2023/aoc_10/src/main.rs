use std::{collections::HashMap, f64::consts::PI};

const SAMPLE_CUSTOM: &str = include_str!("sample_custom.txt");
const INPUT: &str = include_str!("input.txt");
const SAMPLE1: &str = include_str!("sample1.txt");
const SAMPLE2: &str = include_str!("sample2.txt");
const SAMPLE3: &str = include_str!("sample3.txt");
const SAMPLE3B: &str = include_str!("sample3_b.txt");
const SAMPLE4: &str = include_str!("sample4.txt");
const SAMPLE5: &str = include_str!("sample5.txt");

#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash)]
struct Point {
    x: i64,
    y: i64,
}

impl Point {
    fn new(x: i64, y: i64) -> Point {
        Point { x, y }
    }

    fn get_udlr(&self) -> (Point, Point, Point, Point) {
        let up = Point::new(self.x, self.y - 1);
        let down = Point::new(self.x, self.y + 1);
        let left = Point::new(self.x - 1, self.y);
        let right = Point::new(self.x + 1, self.y);
        (up, down, left, right)
    }
}

#[derive(Clone, Debug)]
struct Grid {
    grid: HashMap<Point, char>,
    x_max: i64,
    y_max: i64,
}

impl Grid {
    fn from_str(input: &str) -> Grid {
        let mut grid = HashMap::new();

        for (y, line) in input.lines().enumerate() {
            for (x, c) in line.chars().enumerate() {
                grid.insert(
                    Point {
                        x: x as i64,
                        y: y as i64,
                    },
                    c,
                );
            }
        }

        Grid {
            grid,
            x_max: input.lines().next().unwrap().chars().count() as i64,
            y_max: input.lines().count() as i64,
        }
    }

    fn get_circuit(&self) -> Vec<Point> {
        let start_point = *self.grid.iter().find(|(_, &c)| c == 'S').unwrap().0;
        let mut ret = Vec::new();
        let mut current_point = start_point;
        let mut last_point = start_point;

        loop {
            ret.push(current_point);
            if last_point != start_point && current_point == start_point {
                break;
            }

            let current_char = *self.grid.get(&current_point).unwrap();
            let (up, down, left, right) = current_point.get_udlr();

            if up != last_point && ['|', 'L', 'J', 'S'].contains(&current_char) {
                if let Some(c) = self.grid.get(&up) {
                    if ['|', '7', 'F', 'S'].contains(c) {
                        last_point = current_point;
                        current_point = up;
                        continue;
                    }
                }
            }

            if down != last_point && ['|', '7', 'F', 'S'].contains(&current_char) {
                if let Some(c) = self.grid.get(&down) {
                    if ['|', 'L', 'J', 'S'].contains(c) {
                        last_point = current_point;
                        current_point = down;
                        continue;
                    }
                }
            }

            if left != last_point && ['-', 'J', '7', 'S'].contains(&current_char) {
                if let Some(c) = self.grid.get(&left) {
                    if ['-', 'L', 'F', 'S'].contains(c) {
                        last_point = current_point;
                        current_point = left;
                        continue;
                    }
                }
            }

            if right != last_point && ['-', 'L', 'F', 'S'].contains(&current_char) {
                if let Some(c) = self.grid.get(&right) {
                    if ['-', 'J', '7', 'S'].contains(c) {
                        last_point = current_point;
                        current_point = right;
                        continue;
                    }
                }
            }
        }

        ret
    }

    fn get_farthest_distance(&self) -> u64 {
        (self.get_circuit().len() / 2) as u64
    }

    fn count_enclosed_tiles(&self) -> u64 {
        self.get_enclosed_tiles().len() as u64
    }

    fn get_enclosed_tiles(&self) -> Vec<Point> {
        let circuit = self.get_circuit();
        let mut enclosed_tiles = Vec::new();
        for x in 0..self.x_max {
            for y in 0..self.y_max {
                let p = Point::new(x, y);
                if self.point_is_enclosed(&circuit, p) {
                    enclosed_tiles.push(p);
                }
            }
        }

        enclosed_tiles
    }

    fn get_angle_degs(&self, p1: Point, p2: Point) -> f64 {
        let x_delta = (p2.x - p1.x) as f64;
        let y_delta = -(p2.y - p1.y) as f64;

        // First/fourth quadrant
        let angle = if x_delta >= 0.0 {
            (y_delta / x_delta).atan() * (180.0 / PI)
        // Second/third quadrant
        } else {
            ((y_delta / x_delta).atan() + PI) * (180.0 / PI)
        };

        if angle < 0.0 {
            angle + 360.0
        } else {
            angle
        }
    }

    fn point_is_enclosed(&self, circuit: &[Point], p: Point) -> bool {
        if circuit.contains(&p) {
            return false;
        }

        let start_angle = self.get_angle_degs(p, circuit[0]);
        let mut angle = start_angle;
        let mut delta_angle_sum = 0.0;
        let mut last_p = circuit[0];
        let mut loops = 0.0;
        for circuit_p in circuit.iter().skip(1) {
            if p.y == circuit_p.y && p.y < last_p.y && p.x < circuit_p.x {
                loops += 1.0;
            }
            if p.y < circuit_p.y && p.y == last_p.y && p.x < circuit_p.x {
                loops -= 1.0;
            }

            let new_angle = self.get_angle_degs(p, *circuit_p) + (loops * 360.0);
            let delta_angle = new_angle - angle;
            angle = new_angle;
            last_p = *circuit_p;
            delta_angle_sum += delta_angle;
        }

        delta_angle_sum.abs() > 180.0
    }
}

fn get_farthest_distance(input: &str) -> u64 {
    Grid::from_str(input).get_farthest_distance()
}

fn count_enclosed_tiles(input: &str) -> u64 {
    Grid::from_str(input).count_enclosed_tiles()
}

fn main() {
    assert_eq!(get_farthest_distance(SAMPLE1), 4);
    assert_eq!(get_farthest_distance(SAMPLE2), 8);
    println!("Farthest distance (p1): {}", get_farthest_distance(INPUT));

    assert_eq!(count_enclosed_tiles(SAMPLE_CUSTOM), 9);
    assert_eq!(count_enclosed_tiles(SAMPLE3), 4);
    assert_eq!(count_enclosed_tiles(SAMPLE3B), 4);
    assert_eq!(count_enclosed_tiles(SAMPLE4), 8);
    assert_eq!(count_enclosed_tiles(SAMPLE5), 10);
    println!("Enclosed tiles (p2): {}", count_enclosed_tiles(INPUT));
}

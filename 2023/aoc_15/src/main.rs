use std::collections::HashMap;

const INPUT: &str = include_str!("input.txt");
const SAMPLE: &str = include_str!("sample.txt");

struct Step {
    init_str: String,
    label: String,
    focal_length: Option<i64>,
}

impl Step {
    fn from_str(input: &str) -> Step {
        if input.contains('=') {
            let (label_str, focal_length_str) = input.split_once('=').unwrap();
            Step {
                init_str: input.to_string(),
                label: label_str.to_string(),
                focal_length: Some(focal_length_str.parse::<i64>().unwrap()),
            }
        } else {
            let (label_str, _) = input.split_once('-').unwrap();
            Step {
                init_str: input.to_string(),
                label: label_str.to_string(),
                focal_length: None,
            }
        }
    }

    fn get_label_hash_value(&self) -> i64 {
        self.get_hash_value(&self.label)
    }

    fn get_init_hash_value(&self) -> i64 {
        self.get_hash_value(&self.init_str)
    }

    fn get_hash_value(&self, s: &str) -> i64 {
        let mut current_value: i64 = 0;
        for c in s.chars() {
            current_value += c as i64;
            current_value *= 17;
            current_value %= 256;
        }
        current_value
    }
}

fn hash_sum(input: &str) -> i64 {
    let steps: Vec<Step> = input.trim().split(',').map(Step::from_str).collect();
    steps.iter().fold(0, |acc, s| acc + s.get_init_hash_value())
}

fn focus_power(input: &str) -> i64 {
    let steps: Vec<Step> = input.trim().split(',').map(Step::from_str).collect();
    let mut map: HashMap<i64, Vec<(String, i64)>> = HashMap::new();

    for step in steps {
        let label_hash = step.get_label_hash_value();
        let map_entry = map.entry(label_hash).or_default();
        if let Some(f) = step.focal_length {
            if let Some(i) = map_entry.iter().position(|(l, _)| l == &step.label) {
                map_entry[i] = (step.label, f);
            } else {
                map_entry.push((step.label, f));
            }
        } else if let Some(i) = map_entry.iter().position(|(l, _)| l == &step.label) {
            map_entry.remove(i);
        }
    }

    let mut sum = 0;
    for (box_num, lens_vec) in map {
        for (slot_index, (_, focal_length)) in lens_vec.iter().enumerate() {
            sum += (box_num + 1) * (slot_index as i64 + 1) * focal_length
        }
    }
    sum
}

fn main() {
    assert_eq!(hash_sum(SAMPLE), 1_320);
    println!("Hash sum (p1): {}", hash_sum(INPUT));

    assert_eq!(focus_power(SAMPLE), 145);
    println!("Focus power (p2): {}", focus_power(INPUT));
}

use std::collections::HashMap;

const INPUT: &str = include_str!("input.txt");
const SAMPLE: &str = include_str!("sample.txt");

#[derive(Clone, Copy, Debug, Hash, PartialEq, Eq)]
struct Part {
    x: u64,
    m: u64,
    a: u64,
    s: u64,
}

impl Part {
    fn from_line(line: &str) -> Part {
        let nums: Vec<u64> = line
            .replace(['{', '}', '=', 'x', 'm', 'a', 's'], "")
            .split(',')
            .map(|n| n.parse::<u64>().unwrap())
            .collect();
        Part {
            x: nums[0],
            m: nums[1],
            a: nums[2],
            s: nums[3],
        }
    }
}

#[derive(Clone, Debug, Hash, PartialEq, Eq)]
struct Workflow {
    name: String,
    steps: Vec<Step>,
}

impl Workflow {
    fn from_line(line: &str) -> Workflow {
        let (name_str, rest) = line.split_once('{').unwrap();
        let rest_str = rest.replace('}', "");
        let step_strs: Vec<&str> = rest_str.split(',').collect();
        let steps = step_strs
            .iter()
            .map(|&step_str| {
                if let Some((conditional_str, dest_str)) = step_str.split_once(':') {
                    let left_hand = Some(conditional_str.chars().next().unwrap());
                    let condition = Some(match conditional_str.chars().nth(1).unwrap() {
                        '>' => Condition::Gt,
                        '<' => Condition::Lt,
                        c => panic!("Unsupported condition: {}", c),
                    });
                    let right_hand = Some(
                        conditional_str
                            .chars()
                            .skip(2)
                            .collect::<String>()
                            .parse::<u64>()
                            .unwrap(),
                    );
                    Step {
                        left_hand,
                        condition,
                        right_hand,
                        destination: dest_str.to_string(),
                    }
                } else {
                    Step {
                        left_hand: None,
                        condition: None,
                        right_hand: None,
                        destination: step_str.to_string(),
                    }
                }
            })
            .collect();

        Workflow {
            name: name_str.to_string(),
            steps,
        }
    }
}

#[derive(Clone, Debug, Hash, PartialEq, Eq)]
enum Condition {
    Gt,
    Lt,
}

#[derive(Clone, Debug, Hash, PartialEq, Eq)]
struct Step {
    left_hand: Option<char>,
    condition: Option<Condition>,
    right_hand: Option<u64>,
    destination: String,
}

#[derive(Debug)]
struct RatingGuide {
    workflows: HashMap<String, Workflow>,
    parts: Vec<Part>,
}

impl RatingGuide {
    fn from_str(input: &str) -> RatingGuide {
        let (workflow_lines, part_lines) = input.split_once("\n\n").unwrap();
        let mut workflows = HashMap::new();
        for line in workflow_lines.lines() {
            let workflow = Workflow::from_line(line);
            workflows.insert(workflow.name.clone(), workflow);
        }
        let parts = part_lines.lines().map(Part::from_line).collect();
        RatingGuide { workflows, parts }
    }

    fn get_accepted_parts(&self) -> Vec<Part> {
        self.parts
            .clone()
            .into_iter()
            .filter(|p| self.part_is_accepted(p))
            .collect()
    }

    fn part_is_accepted(&self, part: &Part) -> bool {
        let mut current_workflow = self.workflows.get("in").unwrap();
        loop {
            for step in &current_workflow.steps {
                if step.left_hand.is_none() {
                    match step.destination.as_str() {
                        "A" => return true,
                        "R" => return false,
                        d => current_workflow = self.workflows.get(d).unwrap(),
                    };
                    break;
                } else {
                    let lhv = match step.left_hand.unwrap() {
                        'x' => part.x,
                        'm' => part.m,
                        'a' => part.a,
                        's' => part.s,
                        _ => panic!("Invalid rating letter"),
                    };
                    match step.condition.as_ref().unwrap() {
                        Condition::Gt => {
                            if lhv > step.right_hand.unwrap() {
                                match step.destination.as_str() {
                                    "A" => return true,
                                    "R" => return false,
                                    d => current_workflow = self.workflows.get(d).unwrap(),
                                };
                                break;
                            }
                        }
                        Condition::Lt => {
                            if lhv < step.right_hand.unwrap() {
                                match step.destination.as_str() {
                                    "A" => return true,
                                    "R" => return false,
                                    d => current_workflow = self.workflows.get(d).unwrap(),
                                };
                                break;
                            }
                        }
                    }
                }
            }
        }
    }

    fn accepted_part_count(&self) -> u64 {
        let n = 4_000u64;
        let total_part_count = n.pow(4);
        let mut checked = 0;
        let mut accepted = 0;

        // TODO: this is a foolishly naive solution.
        for x in 1..=n {
            for m in 1..=n {
                for a in 1..=n {
                    for s in 1..=n {
                        if self.part_is_accepted(&Part { x, m, a, s }) {
                            accepted += 1;
                        }

                        checked += 1;
                        if checked % 1_000_000 == 0 {
                            println!(
                                "Checked {}/{} ({}%)",
                                checked,
                                total_part_count,
                                (checked as f64 / total_part_count as f64) * 100.0
                            );
                        }
                    }
                }
            }
        }
        accepted
    }
}

fn accepted_part_sum(input: &str) -> u64 {
    let guide = RatingGuide::from_str(input);
    let accepted_parts = guide.get_accepted_parts();
    accepted_parts
        .iter()
        .fold(0, |acc, p| acc + p.x + p.m + p.a + p.s)
}

fn possible_accepted_parts(input: &str) -> u64 {
    let guide = RatingGuide::from_str(input);
    guide.accepted_part_count()
}

fn main() {
    assert_eq!(accepted_part_sum(SAMPLE), 19_114);
    println!(
        "Sum of accepted part ratings (p1): {}",
        accepted_part_sum(INPUT)
    );

    assert_eq!(possible_accepted_parts(SAMPLE), 167_409_079_868_000);
    println!(
        "Possible accepted parts (p2): {}",
        possible_accepted_parts(INPUT)
    );
}

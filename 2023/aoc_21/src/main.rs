use std::collections::{HashMap, HashSet};

const INPUT: &str = include_str!("input.txt");
const SAMPLE: &str = include_str!("sample.txt");

#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash)]
struct Point {
    x: i64,
    y: i64,
}

impl Point {
    fn new(x: i64, y: i64) -> Point {
        Point { x, y }
    }

    fn get_udlr(&self) -> [Point; 4] {
        let up = Point::new(self.x, self.y - 1);
        let down = Point::new(self.x, self.y + 1);
        let left = Point::new(self.x - 1, self.y);
        let right = Point::new(self.x + 1, self.y);
        [up, down, left, right]
    }
}

#[derive(Clone, Debug, PartialEq, Eq)]
struct Garden {
    grid: HashMap<Point, char>,
    x_max: i64,
    y_max: i64,
    start: Point,
}

impl Garden {
    fn from_str(input: &str) -> Garden {
        let mut grid = HashMap::new();

        let mut start = None;
        for (y, line) in input.lines().enumerate() {
            for (x, c) in line.chars().enumerate() {
                let real_c = if c == 'S' {
                    start = Some(Point::new(x as i64, y as i64));
                    '.'
                } else {
                    c
                };

                grid.insert(Point::new(x as i64, y as i64), real_c);
            }
        }

        Garden {
            grid,
            x_max: input.lines().next().unwrap().chars().count() as i64,
            y_max: input.lines().count() as i64,
            start: start.unwrap(),
        }
    }

    fn num_plots_reachable_in_n_steps(&self, n_steps: u64) -> u64 {
        let mut reachable = HashSet::from([self.start]);
        for _ in 0..n_steps {
            let last_reachable = reachable.clone();
            reachable.clear();
            for p in last_reachable {
                for step_p in p.get_udlr() {
                    if let Some(step_c) = self.grid.get(&step_p) {
                        if *step_c == '.' {
                            reachable.insert(step_p);
                        }
                    }
                }
            }
        }
        reachable.len() as u64
    }
}

fn num_plots_reachable_in_n_steps(input: &str, n_steps: u64) -> u64 {
    let garden = Garden::from_str(input);
    garden.num_plots_reachable_in_n_steps(n_steps)
}

fn main() {
    assert_eq!(num_plots_reachable_in_n_steps(SAMPLE, 6), 16);
    println!(
        "Plots reachable in 64 steps (p1): {}",
        num_plots_reachable_in_n_steps(INPUT, 64)
    );
}

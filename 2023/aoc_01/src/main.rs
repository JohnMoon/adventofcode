const INPUT: &str = include_str!("input.txt");
const SAMPLE: &str = include_str!("sample.txt");
const SAMPLE2: &str = include_str!("sample2.txt");

fn get_cal_val_p1(line: &str) -> u32 {
    let first_dig = line.chars().find(|c| c.is_ascii_digit()).unwrap();

    let last_dig = line.chars().rev().find(|c| c.is_ascii_digit()).unwrap();

    format!("{}{}", first_dig, last_dig).parse::<u32>().unwrap()
}

fn get_cal_val_p2(line: &str) -> u32 {
    let mut min_idx = line.len();
    let mut first_dig = None;
    let mut max_idx = 0;
    let mut last_dig = None;
    for dig_str in [
        "one", "two", "three", "four", "five", "six", "seven", "eight", "nine",
    ] {
        let int_str = dig_str_to_int_str(dig_str);

        if let Some(i) = line.find(dig_str) {
            if i < min_idx {
                min_idx = i;
                first_dig = Some(int_str.clone());
            }
        }

        if let Some(i) = line.find(&int_str) {
            if i < min_idx {
                min_idx = i;
                first_dig = Some(int_str.clone());
            }
        }

        if let Some(i) = line.rfind(dig_str) {
            if i >= max_idx {
                max_idx = i;
                last_dig = Some(int_str.clone());
            }
        }

        if let Some(i) = line.rfind(&int_str) {
            if i >= max_idx {
                max_idx = i;
                last_dig = Some(int_str);
            }
        }
    }

    format!("{}{}", first_dig.unwrap(), last_dig.unwrap())
        .parse::<u32>()
        .unwrap()
}

fn dig_str_to_int_str(dig_str: &str) -> String {
    match dig_str {
        "one" => "1".to_string(),
        "two" => "2".to_string(),
        "three" => "3".to_string(),
        "four" => "4".to_string(),
        "five" => "5".to_string(),
        "six" => "6".to_string(),
        "seven" => "7".to_string(),
        "eight" => "8".to_string(),
        "nine" => "9".to_string(),
        _ => panic!("Invalid digit string"),
    }
}

fn get_summed_cal_val_p1(input: &str) -> u32 {
    input.lines().map(get_cal_val_p1).sum()
}

fn get_summed_cal_val_p2(input: &str) -> u32 {
    input.lines().map(get_cal_val_p2).sum()
}

fn main() {
    assert_eq!(get_summed_cal_val_p1(SAMPLE), 142);
    println!("Summed cal value (p1): {}", get_summed_cal_val_p1(INPUT));

    assert_eq!(get_summed_cal_val_p2(SAMPLE2), 281);
    println!("Summed cal value (p2): {}", get_summed_cal_val_p2(INPUT));
}

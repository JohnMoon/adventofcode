use std::collections::HashMap;

const INPUT: &str = include_str!("input.txt");
const SAMPLE: &str = include_str!("sample.txt");

#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash)]
struct Point {
    x: i64,
    y: i64,
}

impl Point {
    fn new(x: i64, y: i64) -> Point {
        Point { x, y }
    }
}

#[derive(Clone, Debug, PartialEq, Eq)]
struct Grid {
    grid: HashMap<Point, char>,
    x_max: i64,
    y_max: i64,
    num_cycles: usize,
}

impl Grid {
    fn from_str(input: &str) -> Grid {
        let mut grid = HashMap::new();

        for (y, line) in input.lines().enumerate() {
            for (x, c) in line.chars().enumerate() {
                grid.insert(
                    Point {
                        x: x as i64,
                        y: y as i64,
                    },
                    c,
                );
            }
        }

        Grid {
            grid,
            x_max: input.lines().next().unwrap().chars().count() as i64,
            y_max: input.lines().count() as i64,
            num_cycles: 0,
        }
    }

    fn tilt_north(&mut self) {
        for x in 0..self.x_max {
            for y in 0..self.y_max {
                let start_p = Point::new(x, y);
                let c = *self.grid.get(&start_p).unwrap();
                // If we have a round rock, move it north until it hits
                // an edge, another round rock, or a square rock.
                if c == 'O' {
                    for t_y in (0..start_p.y).rev() {
                        let dst_p = Point::new(start_p.x, t_y);
                        let dst_c = *self.grid.get(&dst_p).unwrap();

                        if ['O', '#'].contains(&dst_c) {
                            self.grid.insert(start_p, '.');
                            self.grid.insert(Point::new(dst_p.x, dst_p.y + 1), 'O');
                            break;
                        }
                        if dst_p.y == 0 {
                            self.grid.insert(start_p, '.');
                            self.grid.insert(dst_p, 'O');
                            break;
                        }
                    }
                }
            }
        }
    }

    fn tilt_south(&mut self) {
        for x in 0..self.x_max {
            for y in (0..self.y_max).rev() {
                let start_p = Point::new(x, y);
                //println!("start {:?}", start_p);
                let c = *self.grid.get(&start_p).unwrap();
                // If we have a round rock, move it south until it hits
                // an edge, another round rock, or a square rock.
                if c == 'O' {
                    for t_y in (start_p.y + 1)..self.y_max {
                        let dst_p = Point::new(start_p.x, t_y);
                        let dst_c = *self.grid.get(&dst_p).unwrap();

                        if ['O', '#'].contains(&dst_c) {
                            self.grid.insert(start_p, '.');
                            self.grid.insert(Point::new(dst_p.x, dst_p.y - 1), 'O');
                            break;
                        }
                        if dst_p.y == (self.y_max - 1) {
                            self.grid.insert(start_p, '.');
                            self.grid.insert(dst_p, 'O');
                            break;
                        }
                    }
                }
            }
        }
    }

    fn tilt_west(&mut self) {
        for y in 0..self.y_max {
            for x in 0..self.x_max {
                let start_p = Point::new(x, y);
                let c = *self.grid.get(&start_p).unwrap();
                // If we have a round rock, move it west until it hits
                // an edge, another round rock, or a square rock.
                if c == 'O' {
                    for t_x in (0..start_p.x).rev() {
                        let dst_p = Point::new(t_x, start_p.y);
                        let dst_c = *self.grid.get(&dst_p).unwrap();

                        if ['O', '#'].contains(&dst_c) {
                            self.grid.insert(start_p, '.');
                            self.grid.insert(Point::new(dst_p.x + 1, dst_p.y), 'O');
                            break;
                        }
                        if dst_p.x == 0 {
                            self.grid.insert(start_p, '.');
                            self.grid.insert(dst_p, 'O');
                            break;
                        }
                    }
                }
            }
        }
    }

    fn tilt_east(&mut self) {
        for y in 0..self.y_max {
            for x in (0..self.x_max).rev() {
                let start_p = Point::new(x, y);
                let c = *self.grid.get(&start_p).unwrap();
                // If we have a round rock, move it east until it hits
                // an edge, another round rock, or a square rock.
                if c == 'O' {
                    for t_x in (start_p.x + 1)..self.x_max {
                        let dst_p = Point::new(t_x, start_p.y);
                        let dst_c = *self.grid.get(&dst_p).unwrap();

                        if ['O', '#'].contains(&dst_c) {
                            self.grid.insert(start_p, '.');
                            self.grid.insert(Point::new(dst_p.x - 1, dst_p.y), 'O');
                            break;
                        }
                        if dst_p.x == (self.x_max - 1) {
                            self.grid.insert(start_p, '.');
                            self.grid.insert(dst_p, 'O');
                            break;
                        }
                    }
                }
            }
        }
    }

    fn get_total_north_support(&self) -> u64 {
        let mut sum = 0;
        for y in 0..self.y_max {
            let scalar = self.y_max - y;
            let n_round_rocks = self
                .grid
                .iter()
                .filter(|(&p, &c)| p.y == y && c == 'O')
                .count();
            sum += scalar * n_round_rocks as i64;
        }
        sum as u64
    }
}

fn total_north_support(input: &str) -> u64 {
    let mut grid = Grid::from_str(input);
    grid.tilt_north();
    grid.get_total_north_support()
}

fn total_north_support_n_cycles(input: &str, cycles: usize) -> u64 {
    let mut grid = Grid::from_str(input);
    let mut seen_configurations: Vec<(HashMap<Point, char>, usize)> = Vec::new();
    let mut jumped = false;
    while grid.num_cycles <= cycles {
        grid.tilt_north();
        grid.tilt_west();
        grid.tilt_south();
        grid.tilt_east();

        grid.num_cycles += 1;

        if !jumped {
            if let Some((_, orig_cycle)) =
                seen_configurations.iter().find(|&(g, _)| g == &grid.grid)
            {
                // With a loop, we can sort of skip to the end
                let period = grid.num_cycles - orig_cycle;
                let remaining_cycles = cycles - grid.num_cycles;
                let almost_done_cycle = ((remaining_cycles / period) * period) - orig_cycle;
                grid.num_cycles = almost_done_cycle;
                jumped = true;
            } else {
                seen_configurations.push((grid.grid.clone(), grid.num_cycles));
            }
        }
    }
    grid.get_total_north_support()
}

fn main() {
    assert_eq!(total_north_support(SAMPLE), 136);
    println!("Total north support (p1): {}", total_north_support(INPUT));

    assert_eq!(total_north_support_n_cycles(SAMPLE, 1_000_000_000), 64);
    println!(
        "Total north support (p2): {}",
        total_north_support_n_cycles(INPUT, 1_000_000_000)
    );
}

use std::collections::{HashMap, HashSet};

const INPUT: &str = include_str!("input.txt");
const SAMPLE: &str = include_str!("sample.txt");

#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash)]
enum Direction {
    Up,
    Down,
    Left,
    Right,
}

#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash, PartialOrd, Ord)]
struct Point {
    x: i64,
    y: i64,
}

impl Point {
    fn new(x: i64, y: i64) -> Point {
        Point { x, y }
    }

    fn get_udlr(&self) -> (Point, Point, Point, Point) {
        let up = Point::new(self.x, self.y - 1);
        let down = Point::new(self.x, self.y + 1);
        let left = Point::new(self.x - 1, self.y);
        let right = Point::new(self.x + 1, self.y);
        (up, down, left, right)
    }
}

#[derive(Clone, Debug)]
struct Grid {
    grid: HashMap<Point, char>,
    x_max: i64,
    y_max: i64,
}

impl Grid {
    fn from_str(input: &str) -> Grid {
        let mut grid = HashMap::new();

        for (y, line) in input.lines().enumerate() {
            for (x, c) in line.chars().enumerate() {
                grid.insert(
                    Point {
                        x: x as i64,
                        y: y as i64,
                    },
                    c,
                );
            }
        }

        Grid {
            grid,
            x_max: input.lines().next().unwrap().chars().count() as i64 - 1,
            y_max: input.lines().count() as i64 - 1,
        }
    }

    fn num_energized_tiles(&self, omni: bool) -> u64 {
        let starts_to_check = if omni {
            let mut ret = Vec::new();
            for x in 0..=self.x_max {
                ret.push((Point::new(x, 0), Direction::Down));
                ret.push((Point::new(x, self.y_max), Direction::Up));
            }
            for y in 0..=self.y_max {
                ret.push((Point::new(0, y), Direction::Right));
                ret.push((Point::new(self.x_max, y), Direction::Left));
            }
            ret
        } else {
            vec![(Point::new(0, 0), Direction::Right)]
        };

        let mut max = 0;
        for (start_point, start_direction) in starts_to_check {
            let visited_tiles: HashSet<(Point, Direction)> =
                self.num_energized_tiles_r(start_point, start_direction, &HashSet::new());
            let mut energized_tiles = visited_tiles
                .iter()
                .map(|&(p, _)| p)
                .collect::<Vec<Point>>();
            energized_tiles.sort_unstable();
            energized_tiles.dedup();
            let energized_tile_count = energized_tiles.len() as u64;
            if energized_tile_count > max {
                max = energized_tile_count;
            }
        }
        max
    }

    fn num_energized_tiles_r(
        &self,
        start_point: Point,
        start_dir: Direction,
        visited: &HashSet<(Point, Direction)>,
    ) -> HashSet<(Point, Direction)> {
        let mut p = start_point;
        let mut d = start_dir;
        let mut newly_visited: HashSet<(Point, Direction)> = visited.clone();
        while !newly_visited.contains(&(p, d)) {
            if p.x < 0 || p.x > self.x_max || p.y < 0 || p.y > self.y_max {
                break;
            }

            newly_visited.insert((p, d));

            let c = *self.grid.get(&p).unwrap();
            let (up, down, left, right) = p.get_udlr();
            match c {
                '.' => match d {
                    Direction::Up => p = up,
                    Direction::Down => p = down,
                    Direction::Left => p = left,
                    Direction::Right => p = right,
                },
                '\\' => match d {
                    Direction::Up => {
                        p = left;
                        d = Direction::Left;
                    }
                    Direction::Down => {
                        p = right;
                        d = Direction::Right;
                    }
                    Direction::Left => {
                        p = up;
                        d = Direction::Up;
                    }
                    Direction::Right => {
                        p = down;
                        d = Direction::Down;
                    }
                },
                '/' => match d {
                    Direction::Up => {
                        p = right;
                        d = Direction::Right;
                    }
                    Direction::Down => {
                        p = left;
                        d = Direction::Left;
                    }
                    Direction::Left => {
                        p = down;
                        d = Direction::Down;
                    }
                    Direction::Right => {
                        p = up;
                        d = Direction::Up;
                    }
                },
                '-' => match d {
                    Direction::Up | Direction::Down => {
                        for t in &self.num_energized_tiles_r(left, Direction::Left, &newly_visited)
                        {
                            newly_visited.insert(*t);
                        }
                        for t in
                            &self.num_energized_tiles_r(right, Direction::Right, &newly_visited)
                        {
                            newly_visited.insert(*t);
                        }
                    }
                    Direction::Left => p = left,
                    Direction::Right => p = right,
                },
                '|' => match d {
                    Direction::Up => p = up,
                    Direction::Down => p = down,
                    Direction::Left | Direction::Right => {
                        for t in &self.num_energized_tiles_r(up, Direction::Up, &newly_visited) {
                            newly_visited.insert(*t);
                        }
                        for t in &self.num_energized_tiles_r(down, Direction::Down, &newly_visited)
                        {
                            newly_visited.insert(*t);
                        }
                    }
                },
                _ => panic!("Uncovered point: {}", c),
            }
        }
        newly_visited
    }
}

fn num_energized_tiles(input: &str, omni: bool) -> u64 {
    let grid = Grid::from_str(input);
    grid.num_energized_tiles(omni)
}

fn main() {
    assert_eq!(num_energized_tiles(SAMPLE, false), 46);
    println!(
        "Number of energized tiles (p1): {}",
        num_energized_tiles(INPUT, false)
    );

    assert_eq!(num_energized_tiles(SAMPLE, true), 51);
    println!("P2 test passed");
    println!(
        "Number of energized tiles (p2): {}",
        num_energized_tiles(INPUT, true)
    );
}

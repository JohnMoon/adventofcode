use std::{collections::HashMap, f32::consts::E};

const INPUT: &str = include_str!("input.txt");
const SAMPLE: &str = include_str!("sample.txt");

#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash)]
enum Direction {
    Up,
    Down,
    Left,
    Right,
}

#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash)]
enum Degree {
    One,
    Two,
    Three
}

#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash, PartialOrd, Ord)]
struct Point {
    x: i64,
    y: i64,
}

impl Point {
    fn new(x: i64, y: i64) -> Point {
        Point { x, y }
    }

    fn get_udlr(&self) -> (Point, Point, Point, Point) {
        let up = Point::new(self.x, self.y - 1);
        let down = Point::new(self.x, self.y + 1);
        let left = Point::new(self.x - 1, self.y);
        let right = Point::new(self.x + 1, self.y);
        (up, down, left, right)
    }
}

#[derive(Clone, Debug)]
struct Grid {
    grid: HashMap<Point, u64>,
    x_max: i64,
    y_max: i64,
}

impl Grid {
    fn from_str(input: &str) -> Grid {
        let mut grid = HashMap::new();

        for (y, line) in input.lines().enumerate() {
            for (x, c) in line.chars().enumerate() {
                grid.insert(
                    Point {
                        x: x as i64,
                        y: y as i64,
                    },
                    c.to_digit(10).unwrap() as u64,
                );
            }
        }

        Grid {
            grid,
            x_max: input.lines().next().unwrap().chars().count() as i64 - 1,
            y_max: input.lines().count() as i64 - 1,
        }
    }

    fn get_min_heat_loss(&self) -> u64 {
        let start = Point::new(0, 0);
        let end = Point::new(self.x_max, self.y_max);
        let mut min_heat_losses: HashMap<(Point, Degree), u64> = HashMap::new();
        self.get_min_heat_loss_r(start, end, &mut min_heat_losses)
    }

    fn get_min_heat_loss_r(&self, start: Point, end: Point, min_heat_losses: &mut HashMap<(Point, Degree), u64>) -> u64 {
        let (up, left, down, right) = end.get_udlr();
        let min_adjacent = [up, down, left, right].iter().fold(u64::MAX, |acc, p| {
            if p.x < 0 || p.x > self.x_max || p.y < 0 || p.y > self.y_max {
                return acc;
            }

            let heat_loss = self.get_min_heat_loss_r(start, *p, min_heat_losses);
            if heat_loss < acc {
                heat_loss
            } else {
                acc
            }
        });
        println!("Returning w/ min_adjacent: {}", min_adjacent);
        if min_adjacent == u64::MAX {
            *self.grid.get(&end).unwrap()
        } else {
            min_adjacent + self.grid.get(&end).unwrap()
        }
    }
}

fn min_heat_loss(input: &str) -> u64 {
    let grid = Grid::from_str(input);
    grid.get_min_heat_loss()
}

fn main() {
    assert_eq!(min_heat_loss(SAMPLE), 102);
    //println!("Minimum heat loss (p1): {}", min_heat_loss(INPUT));
}

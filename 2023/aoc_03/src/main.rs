use std::collections::HashMap;

const INPUT: &str = include_str!("input.txt");
const SAMPLE: &str = include_str!("sample.txt");

fn get_sums(input: &str) -> (u32, u32) {
    let mut engine: Vec<Vec<char>> = input.lines().map(|l| l.chars().collect()).collect();

    // Pad the engine with '.' to make it easier to deal with boundaries
    engine.insert(0, (0..engine[0].len()).map(|_| '.').collect::<Vec<char>>());
    engine.push((0..=engine[0].len()).map(|_| '.').collect::<Vec<char>>());
    for row in engine.iter_mut() {
        row.insert(0, '.');
        row.push('.');
    }

    let mut sum = 0;
    let mut num_str = "".to_string();

    // Map of gear positions to part numbers attached to them
    let mut gear_candidate_map: HashMap<(usize, usize), Vec<u32>> = HashMap::new();

    // Only iterate to positions inside of the padding
    for row in 1..(engine.len() - 1) {
        for col in 1..(engine[0].len() - 1) {
            let elem = engine[row][col];
            if elem == '.' {
                num_str = "".to_string();
                continue;
            }

            if elem.is_ascii_digit() {
                num_str.push(elem);

                // If the next char is not a digit, check for a symbol
                // adjacent to the current position and the position of the
                // last num_str.len() positions.
                let mut is_part_number = false;
                if !engine[row][col + 1].is_ascii_digit() {
                    let mut col_list = vec![col, col + 1];
                    for i in 1..=num_str.len() {
                        col_list.push(col - i);
                    }

                    // Iterate over every position in a rectangle around the number
                    for r in [row - 1, row, row + 1] {
                        for c in &col_list {
                            let sym = engine[r][*c];
                            if (!sym.is_ascii_digit()) && (sym != '.') {
                                is_part_number = true;
                                if sym == '*' {
                                    gear_candidate_map
                                        .entry((r, *c))
                                        .or_default()
                                        .push(num_str.parse::<u32>().unwrap());
                                }
                                break;
                            }
                        }
                    }
                }

                if is_part_number {
                    sum += num_str.parse::<u32>().unwrap();
                    num_str = "".to_string();
                }
            }
        }
    }

    let gear_ratio_sum = gear_candidate_map
        .values()
        .filter(|l| l.len() == 2)
        .fold(0, |acc, l| acc + l.iter().product::<u32>());

    (sum, gear_ratio_sum)
}

fn main() {
    assert_eq!(get_sums(SAMPLE), (4361, 467835));
    println!(
        "Part number sum, gear ratio sum (p1 & p2): {:?}",
        get_sums(INPUT)
    );
}

const INPUT: &str = include_str!("input.txt");
const SAMPLE: &str = include_str!("sample.txt");

#[derive(Debug, Clone)]
struct Grid {
    grid: Vec<Vec<char>>,
    rotated_grid: Vec<Vec<char>>,
}

impl Grid {
    fn from_str(input: &str) -> Grid {
        let grid: Vec<Vec<char>> = input
            .lines()
            .map(|l| l.chars().collect::<Vec<char>>())
            .collect();
        let mut rotated_grid: Vec<Vec<char>> = Vec::new();

        for i in 0..grid[0].len() {
            let mut t_row = Vec::new();
            for grid_row in grid.iter().rev() {
                t_row.push(grid_row[i]);
            }
            rotated_grid.push(t_row);
        }

        Grid { grid, rotated_grid }
    }

    fn get_reflection_value(&self, smudge: bool) -> u64 {
        if let Some(v) = self.get_mirror_index("vertical", smudge) {
            return v;
        }

        if let Some(v) = self.get_mirror_index("horizontal", smudge) {
            return 100 * v;
        }

        0
    }

    fn get_mirror_index(&self, direction: &str, smudge: bool) -> Option<u64> {
        let grid = match direction {
            "vertical" => self.rotated_grid.clone(),
            "horizontal" => self.grid.clone(),
            _ => panic!("Unsupported direction!"),
        };

        for candidate_index in 0..(grid.len() - 1) {
            let (left_index, right_index) = if candidate_index < grid.len() / 2 {
                (0, candidate_index + candidate_index + 1)
            } else {
                let r = candidate_index + (grid.len() - candidate_index) - 1;
                (candidate_index - (r - candidate_index) + 1, r)
            };

            let v1: Vec<&Vec<char>> = grid[left_index..=candidate_index].iter().collect();
            let v2: Vec<&Vec<char>> = grid[(candidate_index + 1)..=right_index]
                .iter()
                .rev()
                .collect();
            if !smudge {
                if v1 == v2 {
                    return Some(candidate_index as u64 + 1);
                }
            } else {
                // If there's only one char different between the two sets, then
                // we have a smudge, but the candidate index is otherwise valid.
                let mut num_diff = 0;
                for (i, v1_sub) in v1.iter().enumerate() {
                    for (j, c) in v1_sub.iter().enumerate() {
                        if *c != v2[i][j] {
                            num_diff += 1;
                        }
                    }
                }
                if num_diff == 1 {
                    return Some(candidate_index as u64 + 1);
                }
            }
        }

        None
    }
}

fn get_reflection_value_sum(input: &str, smudge: bool) -> u64 {
    let grids: Vec<Grid> = input.split("\n\n").map(Grid::from_str).collect();
    grids
        .iter()
        .fold(0, |acc, g| acc + g.get_reflection_value(smudge))
}

fn main() {
    assert_eq!(get_reflection_value_sum(SAMPLE, false), 405);
    println!(
        "Reflection value (p1): {}",
        get_reflection_value_sum(INPUT, false)
    );

    assert_eq!(get_reflection_value_sum(SAMPLE, true), 400);
    println!(
        "Reflection value w/ smudge (p2): {}",
        get_reflection_value_sum(INPUT, true)
    );
}

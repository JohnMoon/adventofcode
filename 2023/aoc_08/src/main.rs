use std::collections::HashMap;

const INPUT: &str = include_str!("input.txt");
const SAMPLE: &str = include_str!("sample.txt");
const SAMPLE2: &str = include_str!("sample2.txt");

enum Dir {
    Left,
    Right,
}

struct Map {
    instructions: Vec<Dir>,
    map: HashMap<String, (String, String)>,
}

impl Map {
    fn from_str(input: &str) -> Map {
        let instructions = input
            .lines()
            .nth(0)
            .unwrap()
            .chars()
            .map(|c| match c {
                'L' => Dir::Left,
                'R' => Dir::Right,
                _ => panic!("Invalid char"),
            })
            .collect();

        let mut map: HashMap<String, (String, String)> = HashMap::new();
        for line in input.lines().skip(2) {
            let positions: Vec<String> = line
                .replace("= ", "")
                .replace(['(', ',', ')'], "")
                .split_whitespace()
                .map(|s| s.to_string())
                .collect();

            map.insert(
                positions[0].clone(),
                (positions[1].clone(), positions[2].clone()),
            );
        }

        Map { instructions, map }
    }

    fn steps_from_to(&self, start: &str, end: &str) -> u64 {
        let mut steps: u64 = 0;
        let mut current_pos = start;
        for dir in self.instructions.iter().cycle() {
            current_pos = match dir {
                Dir::Left => &self.map.get(current_pos).unwrap().0,
                Dir::Right => &self.map.get(current_pos).unwrap().1,
            };

            steps += 1;

            if current_pos == end {
                return steps;
            }

            if steps > 1_000_000 {
                return 0;
            }
        }
        unreachable!();
    }

    fn steps_from_to_spooky(&self) -> u64 {
        let destinations: Vec<String> = self
            .map
            .clone()
            .into_keys()
            .filter(|p| p.ends_with('Z'))
            .collect();

        let starting_positions: Vec<String> = self
            .map
            .clone()
            .into_keys()
            .filter(|p| p.ends_with('A'))
            .collect();

        let mut step_vec = Vec::new();
        for start in &starting_positions {
            for end in &destinations {
                let steps = self.steps_from_to(start, end);
                if steps == 0 {
                    continue;
                }
                step_vec.push(steps);
            }
        }

        lcm(&step_vec)
    }
}

// Stolen from: https://github.com/TheAlgorithms/Rust/blob/master/src/math/lcm_of_n_numbers.rs
pub fn lcm(nums: &[u64]) -> u64 {
    if nums.len() == 1 {
        return nums[0];
    }
    let a = nums[0];
    let b = lcm(&nums[1..]);
    a * b / gcd_of_two_numbers(a, b)
}

fn gcd_of_two_numbers(a: u64, b: u64) -> u64 {
    if b == 0 {
        return a;
    }
    gcd_of_two_numbers(b, a % b)
}

fn steps_required(input: &str) -> u64 {
    let map = Map::from_str(input);
    map.steps_from_to("AAA", "ZZZ")
}

fn steps_required_spooky(input: &str) -> u64 {
    let map = Map::from_str(input);
    map.steps_from_to_spooky()
}

fn main() {
    assert_eq!(steps_required(SAMPLE), 6);
    println!("Steps required (p1): {}", steps_required(INPUT));

    assert_eq!(steps_required_spooky(SAMPLE2), 6);
    println!("Steps required (p2): {}", steps_required_spooky(INPUT));
}

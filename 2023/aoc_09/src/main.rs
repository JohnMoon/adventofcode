const INPUT: &str = include_str!("input.txt");
const SAMPLE: &str = include_str!("sample.txt");

struct ValHistory {
    seq: Vec<i64>,
}

impl ValHistory {
    fn from_str(input: &str) -> ValHistory {
        ValHistory {
            seq: input
                .split_whitespace()
                .map(|v| v.parse::<i64>().unwrap())
                .collect(),
        }
    }

    fn extrapolate_value(&self, forward: bool) -> i64 {
        let mut delta_sequences = vec![self.seq.clone()];
        loop {
            let mut delta_sequence: Vec<i64> = delta_sequences
                .last()
                .unwrap()
                .windows(2)
                .map(|w| w[1] - w[0])
                .collect();

            if delta_sequence.iter().all(|v| v == &delta_sequence[0]) {
                if forward {
                    delta_sequence.push(delta_sequence[0]);
                } else {
                    delta_sequence.insert(0, delta_sequence[0]);
                }
                delta_sequences.push(delta_sequence);
                break;
            }

            delta_sequences.push(delta_sequence);
        }

        for i in (0..(delta_sequences.len() - 1)).rev() {
            if forward {
                let last_val = *delta_sequences[i].last().unwrap();
                let next_seq_val = *delta_sequences[i + 1].last().unwrap();
                delta_sequences[i].push(next_seq_val + last_val);
            } else {
                let first_val = delta_sequences[i][0];
                let back_seq_val = delta_sequences[i + 1][0];
                delta_sequences[i].insert(0, first_val - back_seq_val);
            }
        }

        if forward {
            *delta_sequences[0].last().unwrap()
        } else {
            delta_sequences[0][0]
        }
    }
}

fn get_forward_extrapolated_value_sum(input: &str) -> i64 {
    let val_histories: Vec<ValHistory> = input.lines().map(ValHistory::from_str).collect();
    val_histories
        .iter()
        .fold(0, |acc, v| acc + v.extrapolate_value(true))
}

fn get_backward_extrapolated_value_sum(input: &str) -> i64 {
    let val_histories: Vec<ValHistory> = input.lines().map(ValHistory::from_str).collect();
    val_histories
        .iter()
        .fold(0, |acc, v| acc + v.extrapolate_value(false))
}

fn main() {
    assert_eq!(get_forward_extrapolated_value_sum(SAMPLE), 114);
    println!(
        "Forward extrapolated value sum (p1): {}",
        get_forward_extrapolated_value_sum(INPUT)
    );

    assert_eq!(get_backward_extrapolated_value_sum(SAMPLE), 2);
    println!(
        "Backward extrapolated value sum (p2): {}",
        get_backward_extrapolated_value_sum(INPUT)
    );
}

use std::collections::HashMap;

const INPUT: &str = include_str!("input.txt");
const SAMPLE: &str = include_str!("sample.txt");
const SAMPLE2: &str = include_str!("sample2.txt");
const SAMPLE3: &str = include_str!("sample3.txt");

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum State {
    High,
    Low,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum ModuleType {
    Broadcaster,
    FlipFlop,
    Conjunction,
}

#[derive(Debug, Clone)]
struct Pulse {
    src: String,
    dst: String,
    state: State,
}

#[derive(Debug, Clone, Copy)]
struct PulseCounter {
    low_pulses_rcvd: u64,
    low_pulses_sent: u64,
    high_pulses_rcvd: u64,
    high_pulses_sent: u64,
}

impl PulseCounter {
    fn new() -> PulseCounter {
        PulseCounter {
            low_pulses_rcvd: 0,
            low_pulses_sent: 0,
            high_pulses_rcvd: 0,
            high_pulses_sent: 0,
        }
    }
}

trait Module {
    fn drive(&mut self, inputs: &[Pulse]) -> Vec<Pulse>;
    fn get_name(&self) -> String;
    fn get_module_type(&self) -> ModuleType;
    fn get_outputs(&self) -> Vec<String>;
    fn get_pulse_counter(&self) -> PulseCounter;
    fn set_inputs(&mut self, _inputs: &[String]) {}
}

#[derive(Debug, Clone)]
struct Broadcaster {
    name: String,
    outputs: Vec<String>,
    pulse_counter: PulseCounter,
}

impl Module for Broadcaster {
    fn drive(&mut self, inputs: &[Pulse]) -> Vec<Pulse> {
        let mut output_signals: Vec<Pulse> = Vec::new();
        for pulse in inputs {
            match pulse.state {
                State::High => self.pulse_counter.high_pulses_rcvd += 1,
                State::Low => self.pulse_counter.low_pulses_rcvd += 1,
            };
            for output in &self.outputs {
                let new_pulse = Pulse {
                    src: self.name.clone(),
                    dst: (*output).clone(),
                    state: pulse.state,
                };

                output_signals.push(new_pulse);

                match pulse.state {
                    State::High => self.pulse_counter.high_pulses_sent += 1,
                    State::Low => self.pulse_counter.low_pulses_sent += 1,
                };
            }
        }
        output_signals
    }
    fn get_name(&self) -> String {
        self.name.clone()
    }
    fn get_pulse_counter(&self) -> PulseCounter {
        self.pulse_counter
    }
    fn get_module_type(&self) -> ModuleType {
        ModuleType::Broadcaster
    }
    fn get_outputs(&self) -> Vec<String> {
        self.outputs.clone()
    }
}

#[derive(Debug, Clone)]
struct FlipFlop {
    name: String,
    outputs: Vec<String>,
    pulse_counter: PulseCounter,
    current_state: State,
}

impl Module for FlipFlop {
    fn drive(&mut self, inputs: &[Pulse]) -> Vec<Pulse> {
        let mut output_signals: Vec<Pulse> = Vec::new();
        for pulse in inputs {
            match pulse.state {
                State::High => {
                    self.pulse_counter.high_pulses_rcvd += 1;
                    continue;
                }
                State::Low => self.pulse_counter.low_pulses_rcvd += 1,
            };

            // Flip the current state
            self.current_state = match self.current_state {
                State::Low => State::High,
                State::High => State::Low,
            };

            for output in &self.outputs {
                let new_pulse = Pulse {
                    src: self.name.clone(),
                    dst: (*output).clone(),
                    state: self.current_state,
                };

                output_signals.push(new_pulse);

                match self.current_state {
                    State::High => self.pulse_counter.high_pulses_sent += 1,
                    State::Low => self.pulse_counter.low_pulses_sent += 1,
                };
            }
        }
        output_signals
    }
    fn get_name(&self) -> String {
        self.name.clone()
    }
    fn get_pulse_counter(&self) -> PulseCounter {
        self.pulse_counter
    }
    fn get_module_type(&self) -> ModuleType {
        ModuleType::FlipFlop
    }
    fn get_outputs(&self) -> Vec<String> {
        self.outputs.clone()
    }
}

#[derive(Debug, Clone)]
struct Conjunction {
    name: String,
    outputs: Vec<String>,
    inputs: HashMap<String, State>,
    pulse_counter: PulseCounter,
}

impl Module for Conjunction {
    fn drive(&mut self, inputs: &[Pulse]) -> Vec<Pulse> {
        let mut output_signals: Vec<Pulse> = Vec::new();
        for pulse in inputs {
            match pulse.state {
                State::High => self.pulse_counter.high_pulses_rcvd += 1,
                State::Low => self.pulse_counter.low_pulses_rcvd += 1,
            };

            for input in inputs {
                *self.inputs.get_mut(&input.src).unwrap() = input.state;
            }

            let all_inputs_high = self.inputs.values().all(|&s| s == State::High);

            for output in &self.outputs {
                let output_state = if all_inputs_high {
                    State::Low
                } else {
                    State::High
                };

                output_signals.push(Pulse {
                    src: self.name.clone(),
                    dst: (*output).clone(),
                    state: output_state,
                });

                match output_state {
                    State::High => self.pulse_counter.high_pulses_sent += 1,
                    State::Low => self.pulse_counter.low_pulses_sent += 1,
                };
            }
        }
        output_signals
    }
    fn get_name(&self) -> String {
        self.name.clone()
    }
    fn get_pulse_counter(&self) -> PulseCounter {
        self.pulse_counter
    }
    fn get_module_type(&self) -> ModuleType {
        ModuleType::Conjunction
    }
    fn get_outputs(&self) -> Vec<String> {
        self.outputs.clone()
    }
    fn set_inputs(&mut self, inputs: &[String]) {
        for n in inputs {
            self.inputs.insert(n.to_string(), State::Low);
        }
    }
}

struct Machine {
    modules: HashMap<String, Box<dyn Module>>,
    button_presses: u64,
    rx_low: bool,
}

impl Machine {
    fn from_str(input: &str) -> Machine {
        let mut modules: HashMap<String, Box<dyn Module>> = HashMap::new();
        for line in input.lines() {
            let s = line.replace(" -> ", ",").replace(", ", ",");
            let (label_str, rest) = s.split_once(',').unwrap();
            let outputs: Vec<String> = rest.split(',').map(|s| s.to_string()).collect();
            let first_char = label_str.chars().next().unwrap();
            let mut label = label_str.to_string();
            let module: Box<dyn Module> = match first_char {
                '%' => {
                    label = label.replace('%', "");
                    Box::new(FlipFlop {
                        name: label.clone(),
                        outputs,
                        pulse_counter: PulseCounter::new(),
                        current_state: State::Low,
                    })
                }
                '&' => {
                    label = label.replace('&', "");
                    Box::new(Conjunction {
                        name: label.clone(),
                        outputs,
                        pulse_counter: PulseCounter::new(),
                        inputs: HashMap::new(),
                    })
                }
                _ => Box::new(Broadcaster {
                    name: label.clone(),
                    outputs,
                    pulse_counter: PulseCounter::new(),
                }),
            };
            modules.insert(label, module);
        }

        // Set all conjunction inputs to "memory low"
        let mut conjunction_module_input_map: HashMap<String, Vec<String>> = HashMap::new();
        for (conj_name, _) in modules
            .iter_mut()
            .filter(|(_, module)| module.get_module_type() == ModuleType::Conjunction)
        {
            conjunction_module_input_map.insert((*conj_name).clone(), Vec::new());
        }

        for (_, module) in modules.iter() {
            for output in module.get_outputs() {
                if let Some(conjunction_module_inputs) =
                    conjunction_module_input_map.get_mut(&output)
                {
                    conjunction_module_inputs.push(module.get_name());
                }
            }
        }

        for (conj_mod_name, conj_mod_inputs) in conjunction_module_input_map {
            modules
                .get_mut(&conj_mod_name)
                .unwrap()
                .set_inputs(&conj_mod_inputs);
        }

        Machine {
            modules,
            button_presses: 0,
            rx_low: false,
        }
    }

    fn press_button(&mut self) {
        self.button_presses += 1;

        // Map of pulses that are all going to the same destination.
        let mut output_pulse_map: HashMap<String, Vec<Pulse>> = HashMap::new();
        output_pulse_map.insert(
            "broadcaster".to_string(),
            vec![Pulse {
                src: "button".to_string(),
                dst: "broadcaster".to_string(),
                state: State::Low,
            }],
        );
        while !output_pulse_map.is_empty() {
            let current_output_pulse_map = output_pulse_map.clone();
            output_pulse_map.clear();
            for (dst, output_pulses) in current_output_pulse_map {
                if let Some(dst_module) = self.modules.get_mut(&dst) {
                    for new_pulse in dst_module.drive(&output_pulses) {
                        output_pulse_map
                            .entry(new_pulse.dst.clone())
                            .or_default()
                            .push(new_pulse);
                    }
                } else if dst == "rx" && output_pulses.iter().any(|p| p.state == State::Low) {
                    self.rx_low = true;
                }
            }
        }
    }
}

fn pulses_sent(input: &str, button_presses: u64) -> u64 {
    let mut machine = Machine::from_str(input);
    //println!("The machine:");
    //for (label, module) in &machine.modules {
    //    println!("{}: {:?} -> {:?}", label, module.get_module_type(), module.get_outputs());
    //}
    for _ in 0..button_presses {
        machine.press_button();
    }

    //for (name, m) in &machine.modules {
    //    println!("Module {}: {:?}", name, m.get_pulse_counter());
    //}

    let low_pulses_sent = machine
        .modules
        .iter()
        .fold(machine.button_presses, |acc, (_, m)| {
            acc + m.get_pulse_counter().low_pulses_sent
        });
    let high_pulses_sent = machine.modules.iter().fold(0, |acc, (_, m)| {
        acc + m.get_pulse_counter().high_pulses_sent
    });
    low_pulses_sent * high_pulses_sent
}

fn fewest_pulses_to_rx_low(input: &str) -> u64 {
    let mut machine = Machine::from_str(input);
    while !machine.rx_low {
        machine.press_button();
    }
    machine.button_presses
}

fn main() {
    for n_presses in 1..=3 {
        assert_eq!(
            pulses_sent(SAMPLE, n_presses),
            (8 * n_presses) * (4 * n_presses)
        );
    }
    assert_eq!(pulses_sent(SAMPLE, 1_000), 32_000_000);
    println!("Sample 1 passed\n\n");

    assert_eq!(pulses_sent(SAMPLE2, 1_000), 11_687_500);
    println!("Sample 2 passed\n\n");

    println!("Pulses sent (p1): {}", pulses_sent(INPUT, 1_000));

    assert_eq!(fewest_pulses_to_rx_low(SAMPLE3), 1);
    println!(
        "Fewest pulses to rx low (p2): {}",
        fewest_pulses_to_rx_low(INPUT)
    );
}

use std::collections::HashMap;

const INPUT: &str = include_str!("input.txt");
const SAMPLE: &str = include_str!("sample.txt");

#[derive(Copy, Clone, Debug, Hash, PartialEq, Eq)]
struct Point {
    x: i64,
    y: i64,
}

struct Universe {
    galaxy_map: HashMap<u64, Point>,
}

impl Universe {
    fn from_str(input: &str, expansion_factor: i64) -> Universe {
        let mut grid_vec = Vec::new();
        let mut id = 0;
        for line in input.lines() {
            let mut row = Vec::new();
            for c in line.chars() {
                match c {
                    '#' => {
                        id += 1;
                        row.push(id);
                    }
                    '.' => row.push(0),
                    _ => panic!("Invalid symbol!"),
                };
            }
            grid_vec.push(row);
        }

        let mut galaxy_map = HashMap::new();
        for (y, row) in grid_vec.iter().enumerate() {
            for (x, val) in row.iter().enumerate() {
                if *val != 0 {
                    galaxy_map.insert(
                        *val,
                        Point {
                            x: x as i64,
                            y: y as i64,
                        },
                    );
                }
            }
        }

        let mut row_insert_indices = Vec::new();
        for (y, row) in grid_vec.iter().enumerate() {
            if row.iter().all(|&p| p == 0) {
                row_insert_indices.push(y);
            }
        }

        for (i, y) in row_insert_indices.iter().enumerate() {
            let adjusted_y = *y as i64 + i as i64 * (expansion_factor - 1);
            let impacted_galaxies: Vec<u64> = galaxy_map
                .iter()
                .filter_map(
                    |(&id, &point)| {
                        if point.y > adjusted_y {
                            Some(id)
                        } else {
                            None
                        }
                    },
                )
                .collect();

            for galaxy in impacted_galaxies {
                galaxy_map.get_mut(&galaxy).unwrap().y += expansion_factor - 1;
            }
        }

        let mut col_insert_indices = Vec::new();
        for x in 0..grid_vec[0].len() {
            if grid_vec.iter().all(|r| r[x] == 0) {
                col_insert_indices.push(x);
            }
        }

        for (i, x) in col_insert_indices.iter().enumerate() {
            let adjusted_x = *x as i64 + i as i64 * (expansion_factor - 1);
            let impacted_galaxies: Vec<u64> = galaxy_map
                .iter()
                .filter_map(
                    |(&id, &point)| {
                        if point.x > adjusted_x {
                            Some(id)
                        } else {
                            None
                        }
                    },
                )
                .collect();

            for galaxy in impacted_galaxies {
                galaxy_map.get_mut(&galaxy).unwrap().x += expansion_factor - 1;
            }
        }

        Universe { galaxy_map }
    }

    fn get_sum_of_paths(&self) -> u64 {
        let galaxy_ids: Vec<u64> = self.galaxy_map.clone().into_keys().collect();

        let mut sum = 0;
        for i in 0..galaxy_ids.len() {
            for j in (i + 1)..galaxy_ids.len() {
                sum += self.get_manhattan_distance(galaxy_ids[i], galaxy_ids[j]);
            }
        }
        sum
    }

    fn get_manhattan_distance(&self, from_id: u64, to_id: u64) -> u64 {
        let p1 = self.galaxy_map.get(&from_id).unwrap();
        let p2 = self.galaxy_map.get(&to_id).unwrap();

        ((p1.x - p2.x).abs() + (p1.y - p2.y).abs()) as u64
    }
}

fn get_sum_of_paths(input: &str, expansion_factor: i64) -> u64 {
    Universe::from_str(input, expansion_factor).get_sum_of_paths()
}

fn main() {
    assert_eq!(get_sum_of_paths(SAMPLE, 2), 374);
    println!("Sum of paths (p1): {}", get_sum_of_paths(INPUT, 2));

    assert_eq!(get_sum_of_paths(SAMPLE, 10), 1030);
    assert_eq!(get_sum_of_paths(SAMPLE, 100), 8410);
    println!("Sum of paths (p1): {}", get_sum_of_paths(INPUT, 1_000_000));
}

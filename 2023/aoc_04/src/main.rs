use std::collections::HashMap;

const INPUT: &str = include_str!("input.txt");
const SAMPLE: &str = include_str!("sample.txt");

#[derive(Clone, Debug)]
struct Card {
    id: u32,
    winning_nums: Vec<u32>,
    nums: Vec<u32>,
}

impl Card {
    fn from_str(s: &str) -> Card {
        // Example:
        // Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53
        let (card_id, rest) = s.split_once(": ").unwrap();
        let id = card_id
            .replace("Card", "")
            .replace([' ', ':'], "")
            .parse::<u32>()
            .unwrap();

        let (winning_nums_str, nums_str) = rest.split_once(" | ").unwrap();

        let winning_nums: Vec<u32> = winning_nums_str
            .split_whitespace()
            .map(|n| n.parse::<u32>().unwrap())
            .collect();

        let nums: Vec<u32> = nums_str
            .split_whitespace()
            .map(|n| n.parse::<u32>().unwrap())
            .collect();

        Card {
            id,
            winning_nums,
            nums,
        }
    }

    fn count_matching_numbers(&self) -> usize {
        self.nums
            .iter()
            .filter(|&n| self.winning_nums.contains(n))
            .count()
    }

    fn get_point_value(&self) -> u32 {
        let matching_numbers = self.count_matching_numbers();
        if matching_numbers == 0 {
            0
        } else {
            2u32.pow((matching_numbers - 1) as u32)
        }
    }
}

fn get_total_points(input: &str) -> u32 {
    let cards: Vec<Card> = input.lines().map(Card::from_str).collect();

    cards.iter().fold(0, |acc, c| acc + c.get_point_value())
}

fn get_total_cards(input: &str) -> u32 {
    let cards: Vec<Card> = input.lines().map(Card::from_str).collect();

    // Make a cache of "seen" cards to improve runtime
    let mut card_map: HashMap<u32, u32> = HashMap::new();

    (0..cards.len()).fold(0, |acc, i| {
        acc + get_card_count_r(&cards[i..], &mut card_map)
    })
}

fn get_card_count_r(cards: &[Card], card_map: &mut HashMap<u32, u32>) -> u32 {
    let orig_id = cards[0].id;

    if let Some(total_cards) = card_map.get(&orig_id) {
        return *total_cards;
    }

    let mut i = 0;
    let mut total_cards = 1;
    let cards_to_process = cards[i].count_matching_numbers();

    while i < cards_to_process {
        i += 1;
        total_cards += get_card_count_r(&cards[i..], card_map);
    }

    card_map.insert(orig_id, total_cards);

    total_cards
}

fn main() {
    assert_eq!(get_total_points(SAMPLE), 13);
    println!("Total points (p1): {}", get_total_points(INPUT));

    assert_eq!(get_total_cards(SAMPLE), 30);
    println!("Total cards (p2): {}", get_total_cards(INPUT));
}

use std::{cmp::Ordering, collections::HashMap};

const INPUT: &str = include_str!("input.txt");
const SAMPLE: &str = include_str!("sample.txt");

#[derive(Clone, Debug, PartialEq, Eq)]
struct Hand {
    cards: Vec<u32>,
    bid: u32,
    jacks_wild: bool,
    strength: u32,
}

impl PartialOrd for Hand {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for Hand {
    fn cmp(&self, other: &Self) -> Ordering {
        let mut ret = self.strength.cmp(&other.strength);
        if ret != Ordering::Equal {
            return ret;
        }

        for i in 0..self.cards.len() {
            let a = if self.jacks_wild && self.cards[i] == 11 {
                1
            } else {
                self.cards[i]
            };
            let b = if other.jacks_wild && other.cards[i] == 11 {
                1
            } else {
                other.cards[i]
            };

            ret = a.cmp(&b);
            if ret != Ordering::Equal {
                return ret;
            }
        }

        panic!("{:?} and {:?} are the same hand!", self, other);
    }
}

impl Hand {
    fn from_str(input: &str, jacks_wild: bool) -> Hand {
        let (cards_str, bid_str) = input.split_once(' ').unwrap();
        let cards: Vec<u32> = cards_str
            .chars()
            .map(|c| match c {
                '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9' => c.to_digit(10).unwrap(),
                'T' => 10,
                'J' => 11,
                'Q' => 12,
                'K' => 13,
                'A' => 14,
                _ => panic!("Invalid card"),
            })
            .collect();

        let bid = bid_str.parse::<u32>().unwrap();

        let mut ret = Hand {
            cards,
            bid,
            jacks_wild,
            strength: 0,
        };
        ret.strength = ret.get_strength();
        ret
    }

    fn get_strength(&self) -> u32 {
        let mut combos_to_check = vec![self.cards.clone()];
        if self.jacks_wild && self.cards.contains(&11) {
            let mut map: HashMap<u32, u32> = HashMap::new();
            for card in &self.cards {
                *map.entry(*card).or_default() += 1;
            }

            // With wild jacks, it always makes the best hand
            // to replace the jacks with whatever card occurs
            // most often in the hand. If there's a tie, replace
            // with the highest value card.
            let mut majority_card = 0;
            let mut max_count = 0;
            for (card, count) in map {
                if card == 11 {
                    continue;
                }

                if count > max_count {
                    max_count = count;
                    majority_card = card;
                    continue;
                }

                if count == max_count && card > majority_card {
                    majority_card = card;
                }
            }

            let mut combo = self.cards.clone();
            combo.iter_mut().for_each(|c| {
                if *c == 11 {
                    *c = majority_card;
                }
            });
            combos_to_check.push(combo);
        }

        let mut max_hand = 0;
        for combo in combos_to_check {
            // Five of a kind
            if combo[1..].iter().all(|c| c == &combo[0]) {
                max_hand = 7;
                // Max achieved, no need to keep checking
                break;
            }

            // Four of a kind
            for card in &combo[0..2] {
                if combo.iter().filter(|&c| c == card).count() == 4 {
                    max_hand = u32::max(6, max_hand);
                    continue;
                }
            }

            // Remaining hands need more sophisticated counting
            let mut map: HashMap<u32, u32> = HashMap::new();
            for card in &combo {
                *map.entry(*card).or_default() += 1;
            }

            // Full house
            if map.keys().len() == 2 {
                max_hand = u32::max(5, max_hand);
                continue;
            }

            // Three of a kind
            if map.values().any(|&c| c == 3) {
                max_hand = u32::max(4, max_hand);
                continue;
            }

            // Two pair
            if map.values().filter(|&c| *c == 2).count() == 2 {
                max_hand = u32::max(3, max_hand);
                continue;
            }

            // One pair
            if map.values().filter(|&c| *c == 2).count() == 1 {
                max_hand = u32::max(2, max_hand);
                continue;
            }

            max_hand = u32::max(1, max_hand);
        }
        max_hand
    }
}

#[derive(Clone, Debug)]
struct Game {
    hands: Vec<Hand>,
}

impl Game {
    fn from_str(input: &str, jacks_wild: bool) -> Game {
        Game {
            hands: input
                .lines()
                .map(|l| Hand::from_str(l, jacks_wild))
                .collect(),
        }
    }

    fn get_total_winnings(&self) -> u32 {
        let mut sorted_hands = self.hands.clone();
        sorted_hands.sort_unstable();

        sorted_hands.iter().enumerate().fold(0, |acc, (i,h)| {
            acc + ((i + 1) as u32 * h.bid)
        })
    }
}

fn get_total_winnings(input: &str, jacks_wild: bool) -> u32 {
    let game = Game::from_str(input, jacks_wild);
    game.get_total_winnings()
}

fn main() {
    assert_eq!(get_total_winnings(SAMPLE, false), 6440);
    println!("Total winnings (p1): {}", get_total_winnings(INPUT, false));

    assert_eq!(get_total_winnings(SAMPLE, true), 5905);
    println!("Total winnings (p2): {}", get_total_winnings(INPUT, true));
}

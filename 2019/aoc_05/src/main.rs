const INPUT: &str = include_str!("input.txt");

struct Intcode {
    prog: Vec<i32>,
    pos: usize,
}

enum ParamMode {
    POS,
    IMM
}

impl ParamMode {
    fn from_char(c: char) -> ParamMode {
        match c {
            '0' => ParamMode::POS,
            '1' => ParamMode::IMM,
            _ => panic!("Invalid parameter mode!")
        }
    }
}

impl Intcode {
    fn get_values(&self, n_params: usize) -> Vec<i32> {
        let mut ret = Vec::new();

        // Reverse the op chars
        let op_chars: Vec<char> = self.prog[self.pos].to_string().chars().rev().collect();
        let count = op_chars.len();
        for p in 0..n_params {
            let mode = {
                if count > p + 2 {
                    ParamMode::from_char(op_chars[p + 2])
                } else {
                    ParamMode::POS
                }
            };

            let val = match mode {
                ParamMode::POS => self.prog[self.prog[(self.pos + 1 + p) as usize] as usize],
                ParamMode::IMM => self.prog[(self.pos + 1 + p) as usize]
            };
            ret.push(val);
        }
        ret
    }

    // Addition takes two params and stores result at the address in 3
    fn add(&mut self) {
        let values = self.get_values(2);
        let index = self.prog[(self.pos + 3) as usize] as usize;
        self.prog[index] = values[0] + values[1];
        self.pos += 4;
    }

    // Multiplication takes two params and stores result at the address in 3
    fn mult(&mut self) {
        let values = self.get_values(2);
        let index = self.prog[(self.pos + 3) as usize] as usize;
        self.prog[index] = values[0] * values[1];
        self.pos += 4;
    }

    // input op takes only one parameter - the input destination
    // it can only be a positional mode parameter
    fn input(&mut self, input: i32) {
        let index = self.prog[(self.pos + 1) as usize] as usize;
        self.prog[index] = input;
        self.pos += 2;
    }

    // output op takes only one parameter - the output location
    fn output(&mut self) -> i32 {
        let values = self.get_values(1);
        self.pos += 2;
        values[0]
    }

    // if first param is non-zero, set pos to value of second param
    fn jump_if_true(&mut self) {
        let values = self.get_values(2);
        if values[0] != 0 {
            self.pos = values[1] as usize;
        } else {
            self.pos += 3;
        }
    }

    // if first param is zero, set pos to value of second param
    fn jump_if_false(&mut self) {
        let values = self.get_values(2);
        if values[0] == 0 {
            self.pos = values[1] as usize;
        } else {
            self.pos += 3;
        }
    }

    // if first param is < second param, store 1 in third param, else store 0
    fn lt(&mut self) {
        let values = self.get_values(2);
        let index = self.prog[(self.pos + 3) as usize] as usize;
        if values[0] < values[1] {
            self.prog[index] = 1;
        } else {
            self.prog[index] = 0;
        }
        self.pos += 4;
    }

    // if first param is == second param, store 1 in third param, else store 0
    fn eq(&mut self) {
        let values = self.get_values(2);
        let index = self.prog[(self.pos + 3) as usize] as usize;
        if values[0] == values[1] {
            self.prog[index] = 1;
        } else {
            self.prog[index] = 0;
        }
        self.pos += 4;
    }

    fn run(&mut self, input: i32) {
        let mut op = self.prog[self.pos] % 100;
        let mut out = None;
        while op != 99 {
            match op {
                1 => self.add(),
                2 => self.mult(),
                3 => self.input(input),
                4 => out = Some(self.output()),
                5 => self.jump_if_true(),
                6 => self.jump_if_false(),
                7 => self.lt(),
                8 => self.eq(),
                _ => panic!("Unsupported op code!")
            };
            if let Some(x) = out {
                println!("{}", x);
                out = None;
            }
            op = self.prog[self.pos] % 100;
        }
    }
}

fn main() {
    let _input: Vec<&str> = INPUT.split(',').collect::<Vec<_>>()
        .iter().map(|&s| s.trim()).collect::<Vec<_>>();

    let input = _input.iter()
        .filter(|s| s.parse::<i32>().is_ok())
        .map(|&s| s.parse::<i32>().unwrap())
        .collect::<Vec<_>>();

    let mut pt1_prog = Intcode {
        prog: input.clone(),
        pos: 0
    };

    println!("Running part 1:");
    pt1_prog.run(1);

    let mut pt2_prog = Intcode {
        prog: input,
        pos: 0
    };

    println!("Running part 2:");
    pt2_prog.run(5);
}

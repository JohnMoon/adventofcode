use std::collections::HashMap;

const INPUT: &str = include_str!("input.txt");

fn get_orbiter_sum(id: String, mut sum: usize, objects: &HashMap<String, Vec<String>>) -> usize {
    if let Some(orbiters) = objects.get(&id) {
        let c_sum = sum + 1;
        for o in orbiters {
            sum += get_orbiter_sum(o.to_string(), c_sum, &objects);
        }
    }
    sum
}

fn get_path_to(id: String, objects: &HashMap<String, Vec<String>>) -> Vec<String> {
    let mut ret = Vec::new();
    let mut current = id;
    while current != "COM" {
        for (id, orbiters) in objects.iter() {
            if orbiters.contains(&current) {
                ret.push(id.to_string());
                current = id.to_string();
                break;
            }
        }
    }
    ret.iter().rev().cloned().collect()
}

fn get_shortest_distance(from: String, to: String, objects: &HashMap<String, Vec<String>>) -> usize {
    let p_to_a = get_path_to(from, &objects);
    let p_to_b = get_path_to(to, &objects);

    let mut p_to_a_uniq = p_to_a.clone();
    p_to_a_uniq.retain(|x| ! p_to_b.contains(x));

    let mut p_to_b_uniq = p_to_b;
    p_to_b_uniq.retain(|x| ! p_to_a.contains(x));

    p_to_a_uniq.len() + p_to_b_uniq.len()
}

fn main() {
    let input: Vec<&str> = INPUT.lines().collect();
    let mut objects: HashMap<String, Vec<String>> = HashMap::new();

    for line in input {
        let line: Vec<&str> = line.split(')').collect();

        let id = line[0].to_string();
        let orbiter = line[1].to_string();

        objects.entry(id)
            .and_modify(|o| o.push(orbiter.clone()))
            .or_insert_with(|| vec![orbiter]);
    }

    println!("Orbital sum: {}", get_orbiter_sum("COM".to_string(), 0, &objects));

    println!("Shortest distance between YOU and SAN: {}",
             get_shortest_distance("YOU".to_string(), "SAN".to_string(), &objects));

}

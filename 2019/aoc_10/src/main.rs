use std::fmt;

extern crate num_integer;

const INPUT: &str = include_str!("input.txt");
//const INPUT: &str = include_str!("test_input.txt");

#[derive(Clone, Copy, PartialEq, Debug)]
struct Point {
    x: usize,
    y: usize
}

#[derive(Clone, Copy, PartialEq)]
struct Line {
    start: Point,
    end: Point
}
#[derive(Clone, PartialEq)]
struct Asteroid {
    loc: Point,
    //can_see: Vec<Asteroid>
    can_see: u32
}

struct Belt {
    grid: Vec<Option<Asteroid>>,
    width: usize,
    height: usize
}

impl fmt::Display for Belt {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut i = 0;
        for v in &self.grid {
            write!(f, "{}", if v.is_some() { '*' } else { '.' }).unwrap();
            i += 1;
            if i >= self.width {
                writeln!(f).unwrap();
                i = 0;
            }
        }
        fmt::Result::Ok(())
    }
}

impl Line {
    fn get_integral_points_between(&self) -> Vec<Point> {
        let mut ret = Vec::new();
        if self.start.x == self.end.x {
            for y in (self.start.y + 1)..self.end.y {
                ret.push(Point {x: self.start.x, y});
            }
            return ret;
        } else if self.start.y == self.end.y {
            for x in (self.start.x + 1)..self.end.x {
                ret.push(Point {x, y: self.start.y});
            }
            return ret;
        }

        let dx_full = (self.start.x as i32 - self.end.x as i32).abs() as usize;
        let dy_full = (self.start.y as i32 - self.end.y as i32).abs() as usize;

        let gcd = num_integer::gcd(dx_full, dy_full);
        let dx_norm = dx_full / gcd;
        let dy_norm = dy_full / gcd;

        // If there's no divisible integral points, return empty vector
        if dx_norm == dx_full  || dy_norm == dy_full {
            return Vec::new();
        }

        //println!("Points between {:?} and {:?} (dx: {}, dy: {}):", self.start, self.end, dx_norm, dy_norm);

        let mut x = self.start.x;
        let mut y = self.start.y;
        let mut t;
        while x != self.end.x && y != self.end.y {
            t = Point {x, y};
            if t != self.start && t != self.end {
                //println!("{:?}", t);
                ret.push(t);
            }

            if x > self.end.x {
                x -= dx_norm;
            } else {
                x += dx_norm;
            }
            if y > self.end.y {
                y -= dy_norm;
            } else {
                y += dy_norm;
            }
        }
        //println!();
        ret
    }

    fn touches(&self, points: &[Point]) -> bool {
        // Get all  points between the two asteroids
        let line_points = self.get_integral_points_between();
        if line_points.is_empty() {
            return false;
        }

        // If any asteroid lands on that point, return true
        for lp in line_points {
            if points.contains(&lp) {
                //println!("{:?} blocks!", lp);
                return true;
            }
        }
        false
    }
}

impl Belt {
    fn get_obs_loc(&mut self) -> Option<Asteroid> {
        // Iterate through every asteroid in relation to every other asteroid
        let mut asteroids: Vec<Asteroid> = self.grid.iter_mut()
            .filter_map(|x| x.clone())
            .collect();
        let points: Vec<Point> = asteroids.iter().map(|x| x.loc).collect();
        for i in {0..asteroids.len()} {
            for j in {(i + 1)..asteroids.len()} {
                let line = Line {start: asteroids[i].loc, end: asteroids[j].loc};
                // If there's no asteroid in between i an j increment i's counter
                if ! line.touches(&points) {
                    asteroids[i].can_see += 1;
                    asteroids[j].can_see += 1;
                }
            }
        }

        let mut can_see_most = Asteroid { loc: Point { x: 0, y: 0 }, can_see: 0 };
        for a in asteroids {
            if a.can_see > can_see_most.can_see {
                can_see_most = a;
            }
        }

        println!("Asteroid at {:?} can see the most other asteroids: {}",
                 can_see_most.loc, can_see_most.can_see);
        Some(can_see_most)
    }
}

impl Belt {
    fn new() -> Belt {
        Belt {
            grid: Vec::new(),
            width: 0,
            height: 0
        }
    }
}

fn main() {
    let mut belt = Belt::new();
    let width = INPUT.lines().count();
    let mut height = 0;
    for (y, line) in INPUT.lines().enumerate() {
        height = line.len();
        for (x, c) in line.chars().enumerate() {
            if c == '#' {
                belt.grid.push(Some(Asteroid {
                    loc: Point { x, y },
                    can_see: 0
                }));
            } else {
                belt.grid.push(None);
            }
        }
    }
    belt.width = width;
    belt.height = height;

    belt.get_obs_loc();
}

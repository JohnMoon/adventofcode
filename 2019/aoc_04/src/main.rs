const PW_LEN: usize = 6;

#[derive(Clone)]
struct Password {
    digits: [u32; PW_LEN]
}

impl Password {
    fn from_u32(n: u32) -> Password {
        let mut pw = Password {
            digits: [0; PW_LEN]
        };
        for (i, c) in n.to_string().chars().enumerate() {
            pw.digits[i] = c.to_digit(10).unwrap();
        }
        pw
    }

    fn is_valid(&self) -> bool {
        for i in 0..(PW_LEN - 1) {
            if self.digits[i] > self.digits[i + 1] {
                return false;
            }
        }

        // At this point, we know the digits are in descending order
        // so we just need to determine if there's a double-digit
        // anywhere AND that the double-digit is not part of a larger
        // set of digits
        //
        // 111122
        let mut group_digs = Vec::new();
        for i in 0..(PW_LEN - 1) {
            // We have a double-digit
            if self.digits[i] == self.digits[i + 1] {
                // Is that double digit already part of a larger group?
                if group_digs.contains(&self.digits[i]) {
                    continue;
                }
                // That double digit is not part of a larger set
                if (i < PW_LEN - 2 && self.digits[i] != self.digits[i + 2]) || i == PW_LEN - 2 {
                    return true;
                } else {
                    group_digs.push(self.digits[i]);
                }
            }
        }

        false
    }
}

fn main() {
    assert!(Password { digits: [1,1,1,1,2,2] }.is_valid());
    assert!(! Password { digits: [1,2,3,4,4,4] }.is_valid());
    let range: Vec<u32> = (284_639u32..748_759u32).collect();
    let mut pws = Vec::new();
    for n in range {
        pws.push(Password::from_u32(n));
    }

    let pws: Vec<Password> = pws.iter().filter(|p| p.is_valid()).cloned().collect();

    println!("{} passwords are valid", pws.len());
}

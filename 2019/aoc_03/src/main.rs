const INPUT: &str = include_str!("input.txt");

#[derive(Debug, Clone, Copy)]
struct Point {
    x: i32,
    y: i32,
    d: i32
}

impl PartialEq for Point {
    fn eq(&self, other: &Point) -> bool {
        self.x == other.x && self.y == other.y
    }
}

#[derive(Debug, Clone)]
struct Wire {
    pts: Vec<Point>
}

impl Wire {
    fn from_csv(csv: &str) -> Wire {
        let mut pts = vec![Point {x: 0, y: 0, d: 0}];
        let mut current_pt = pts[0];
        for mv in csv.split(',') {
            let (dir, n) = mv.split_at(1);
            let new_d = n.parse::<i32>().unwrap();
            let mut new_pt = current_pt;
            new_pt.d += new_d;
            match dir {
                "U" => new_pt.y += new_d,
                "D" => new_pt.y -= new_d,
                "R" => new_pt.x += new_d,
                "L" => new_pt.x -= new_d,
                _ => panic!("Invalid direction!")
            }
            pts.append(&mut get_pts_between(&current_pt, &new_pt));
            pts.push(new_pt);
            current_pt = new_pt;
        }

        Wire {
            pts
        }
    }

    // This is a dumb way to accomplish this. Instead of generating
    // every point along the wire and iterating through all of them,
    // use a Line structure and do some basic math to find intersections.
    // Jeez...
    fn get_intersections(&self, other: &Wire) -> Vec<Point> {
        let mut ret = Vec::new();
        for pt in &self.pts {
            for other_pt in &other.pts {
                if pt == other_pt {
                    ret.push(Point {
                        x: pt.x,
                        y: pt.y,
                        d: pt.d + other_pt.d
                    });
                }
            }
        }
        ret
    }
}

fn get_md(a: Point, b: Point) -> i32 {
    (a.x - b.x).abs() + (a.y - b.y).abs()
}

fn get_pts_between(a: &Point, b: &Point) -> Vec<Point> {
    let mut ret = Vec::new();
    if a.x == b.x && a.y != b.y {
        if a.y < b.y {
            for (i, y) in ((a.y + 1)..b.y).enumerate() {
                ret.push(Point {
                    x: a.x,
                    y,
                    d: a.d + (i as i32) + 1
                });
            }
        } else {
            for (i, y) in ((b.y + 1)..a.y).enumerate() {
                ret.push(Point {
                    x: a.x,
                    y,
                    d: a.d + (i as i32) + 1
                });
            }
        }
    } else if a.y == b.y && a.x != b.x {
        if a.x < b.x {
            for (i, x) in ((a.x + 1)..b.x).enumerate() {
                ret.push(Point {
                    x,
                    y: a.y,
                    d: a.d + (i as i32) + 1
                });
            }
        } else {
            for (i, x) in ((b.x + 1)..a.x).enumerate() {
                ret.push(Point {
                    x,
                    y: a.y,
                    d: a.d + (i as i32) + 1
                });
            }
        }
    } else {
        panic!("Can't calc pts between diag lines");
    }
    ret
}

fn main() {
    let wire_strs: Vec<&str> = INPUT.lines().collect();
    let mut wires: Vec::<Wire> = Vec::new();

    for wire in wire_strs {
        wires.push(Wire::from_csv(wire));
    }

    let wires_cpy = wires.clone();

    let mut smallest_md = 1_000_000;
    let mut smallest_d = 1_000_000;
    for (i, wire_i) in wires.iter().enumerate() {
        for wire_j in wires_cpy.iter().skip(i + 1) {
            let isections = wire_i.get_intersections(&wire_j);
            for isec in isections {
                // Pt 1
                let md = get_md(Point { x: 0, y: 0, d: 0 }, isec);
                if md < smallest_md && md > 0 {
                    smallest_md = md;
                }

                // Pt 2
                if isec.d < smallest_d && isec.d > 0 {
                    smallest_d = isec.d;
                }
            }
        }
    }

    println!("The closest wire intersection has a Manhattan Distance of {}", smallest_md);
    println!("The shortest intersection distance is {}", smallest_d);
}

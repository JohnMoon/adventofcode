const INPUT: &str = include_str!("input.txt");

// Fuel required for pt. 1
//fn get_fuel_required(mod_mass: f32) -> u32 {
//    ((mod_mass / 3.0).trunc() - 2.0) as u32
//}

// Fuel required for pt. 2
fn get_fuel_required(mod_mass: f32, acc: u32) -> u32 {
    let fuel = (mod_mass / 3.0).trunc() - 2.0;
    if fuel > 0.0 {
        get_fuel_required(fuel, acc + (fuel as u32))
    } else {
        acc
    }
}

fn main() {
    let input: Vec<&str> = INPUT.lines().collect();
    let sum = input.iter()
        .fold(0, |acc, mass| acc + get_fuel_required(mass.parse::<f32>().unwrap(), 0));
    println!("The sum fuel required is {}.", sum);
}

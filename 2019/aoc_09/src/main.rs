const INPUT: &str = include_str!("input.txt");
//const INPUT: &str = include_str!("test_input.txt");

#[derive(Clone, Debug)]
struct Intcode {
    prog: Vec<i64>,
    pos: usize,
    rel: usize
}

#[derive(Debug)]
enum ParamMode {
    POS,
    IMM,
    REL
}

impl ParamMode {
    fn from_char(c: char) -> ParamMode {
        match c {
            '0' => ParamMode::POS,
            '1' => ParamMode::IMM,
            '2' => ParamMode::REL,
            _ => panic!("Invalid parameter mode!")
        }
    }
}

impl Intcode {
    fn get_index(&mut self, index_i: usize) -> usize {
        // Reverse the op chars
        let op_chars: Vec<char> = self.prog[self.pos].to_string().chars().rev().collect();
        let mode = {
            if index_i + 2 < op_chars.len() {
                ParamMode::from_char(op_chars[index_i + 2])
            } else {
                ParamMode::POS
            }
        };

        let index = match mode {
            ParamMode::POS => self.prog[(self.pos + 1 + index_i) as usize] as usize,
            ParamMode::REL => ((self.rel as i64) + self.prog[(self.pos + 1 + index_i) as usize]) as usize,
            ParamMode::IMM => panic!("Cannot store value in immediate!")
        };

        // Add zeros to the end of the program if we need to
        while index >= self.prog.len() {
            self.prog.push(0);
        }
        index
    }

    fn get_values(&mut self, n_params: usize) -> Vec<i64> {
        let mut ret = Vec::new();

        // Reverse the op chars
        let op_chars: Vec<char> = self.prog[self.pos].to_string().chars().rev().collect();
        let count = op_chars.len();
        for p in 0..n_params {
            let mode = {
                if count > p + 2 {
                    ParamMode::from_char(op_chars[p + 2])
                } else {
                    ParamMode::POS
                }
            };

            let index = match mode {
                ParamMode::POS => self.prog[(self.pos + 1 + p) as usize] as usize,
                ParamMode::IMM => (self.pos + 1 + p) as usize,
                ParamMode::REL => ((self.rel as i64) + self.prog[(self.pos + 1 + p) as usize]) as usize
            };

            // Add zeros to the end of the program if we need to
            while index >= self.prog.len() {
                self.prog.push(0);
            }

            let val = self.prog[index];
            ret.push(val);
        }
        ret
    }

    // Addition takes two params and stores result at the address in 3
    fn add(&mut self) {
        let values = self.get_values(2);
        let index = self.get_index(2);
        // Add zeros to the end of the program if we need to
        while index >= self.prog.len() {
            self.prog.push(0);
        }

        self.prog[index] = values[0] + values[1];
        self.pos += 4;
    }

    // Multiplication takes two params and stores result at the address in 3
    fn mult(&mut self) {
        let values = self.get_values(2);
        let index = self.get_index(2);
        // Add zeros to the end of the program if we need to
        while index >= self.prog.len() {
            self.prog.push(0);
        }

        self.prog[index] = values[0] * values[1];
        self.pos += 4;
    }

    // input op takes only one parameter - the input destination
    // it can only be a positional mode parameter
    fn input(&mut self, input: i64) {
        let index = self.get_index(0);
        // Add zeros to the end of the program if we need to
        while index >= self.prog.len() {
            self.prog.push(0);
        }

        self.prog[index] = input;
        self.pos += 2;
    }

    // output op takes only one parameter - the output location
    fn output(&mut self) -> i64 {
        let values = self.get_values(1);
        self.pos += 2;
        values[0]
    }

    // if first param is non-zero, set pos to value of second param
    fn jump_if_true(&mut self) {
        let values = self.get_values(2);
        if values[0] != 0 {
            self.pos = values[1] as usize;
        } else {
            self.pos += 3;
        }
    }

    // if first param is zero, set pos to value of second param
    fn jump_if_false(&mut self) {
        let values = self.get_values(2);
        if values[0] == 0 {
            self.pos = values[1] as usize;
        } else {
            self.pos += 3;
        }
    }

    // if first param is < second param, store 1 in third param, else store 0
    fn lt(&mut self) {
        // Add zeros to the end of the program if we need to
        let values = self.get_values(2);
        let index = self.get_index(2);
        while index >= self.prog.len() {
            self.prog.push(0);
        }

        if values[0] < values[1] {
            self.prog[index] = 1;
        } else {
            self.prog[index] = 0;
        }
        self.pos += 4;
    }

    // if first param is == second param, store 1 in third param, else store 0
    fn eq(&mut self) {
        let values = self.get_values(2);
        let index = self.get_index(2);
        // Add zeros to the end of the program if we need to
        while index >= self.prog.len() {
            self.prog.push(0);
        }

        if values[0] == values[1] {
            self.prog[index] = 1;
        } else {
            self.prog[index] = 0;
        }
        self.pos += 4;
    }

    // adjust the relative base by the value of the one param
    fn adj_rel_base(&mut self) {
        let values = self.get_values(1);
        self.rel += values[0] as usize;
        self.pos += 2;
    }

    fn run(&mut self, mut input: Vec<i64>) -> Option<i64> {
        let mut op = self.prog[self.pos] % 100;
        let mut out = None;
        while op != 99 {
            match op {
                1 => self.add(),
                2 => self.mult(),
                3 => {
                    self.input(*input.get(0).expect("error - no input given!"));
                    input.remove(0);
                }
                4 => out = Some(self.output()),
                5 => self.jump_if_true(),
                6 => self.jump_if_false(),
                7 => self.lt(),
                8 => self.eq(),
                9 => self.adj_rel_base(),
                _ => panic!("Unsupported op code!")
            };
            if let Some(s) = out {
                println!("{}", s);
                out = None;
            }
            op = self.prog[self.pos] % 100;
        }
        None
    }
}

fn main() {
    let mut _input: Vec<&str> = INPUT.split(',').collect::<Vec<_>>()
        .iter().map(|&s| s.trim()).collect::<Vec<_>>();

    let input = _input.iter()
        .filter(|s| s.parse::<i64>().is_ok())
        .map(|&s| s.parse::<i64>().unwrap())
        .collect::<Vec<_>>();

    let mut boost = Intcode {
        prog: input.clone(),
        pos: 0,
        rel: 0
    };

    println!("Running part 1:");
    boost.run(vec![1]);

    println!("\nRunning part 2:");
    boost = Intcode {
        prog: input,
        pos: 0,
        rel: 0
    };
    boost.run(vec![2]);
}

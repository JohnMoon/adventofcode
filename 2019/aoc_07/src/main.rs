const INPUT: &str = include_str!("input.txt");

#[derive(Clone, Debug)]
struct Intcode {
    prog: Vec<i32>,
    pos: usize,
}

#[derive(Clone, Debug)]
struct Amp {
    prog: Intcode,
}

enum ParamMode {
    POS,
    IMM
}

impl ParamMode {
    fn from_char(c: char) -> ParamMode {
        match c {
            '0' => ParamMode::POS,
            '1' => ParamMode::IMM,
            _ => panic!("Invalid parameter mode!")
        }
    }
}

impl Intcode {
    fn get_values(&self, n_params: usize) -> Vec<i32> {
        let mut ret = Vec::new();

        // Reverse the op chars
        let op_chars: Vec<char> = self.prog[self.pos].to_string().chars().rev().collect();
        let count = op_chars.len();
        for p in 0..n_params {
            let mode = {
                if count > p + 2 {
                    ParamMode::from_char(op_chars[p + 2])
                } else {
                    ParamMode::POS
                }
            };

            let val = match mode {
                ParamMode::POS => self.prog[self.prog[(self.pos + 1 + p) as usize] as usize],
                ParamMode::IMM => self.prog[(self.pos + 1 + p) as usize]
            };
            ret.push(val);
        }
        ret
    }

    // Addition takes two params and stores result at the address in 3
    fn add(&mut self) {
        let values = self.get_values(2);
        let index = self.prog[(self.pos + 3) as usize] as usize;
        self.prog[index] = values[0] + values[1];
        self.pos += 4;
    }

    // Multiplication takes two params and stores result at the address in 3
    fn mult(&mut self) {
        let values = self.get_values(2);
        let index = self.prog[(self.pos + 3) as usize] as usize;
        self.prog[index] = values[0] * values[1];
        self.pos += 4;
    }

    // input op takes only one parameter - the input destination
    // it can only be a positional mode parameter
    fn input(&mut self, input: i32) {
        let index = self.prog[(self.pos + 1) as usize] as usize;
        self.prog[index] = input;
        self.pos += 2;
    }

    // output op takes only one parameter - the output location
    fn output(&mut self) -> i32 {
        let values = self.get_values(1);
        self.pos += 2;
        values[0]
    }

    // if first param is non-zero, set pos to value of second param
    fn jump_if_true(&mut self) {
        let values = self.get_values(2);
        if values[0] != 0 {
            self.pos = values[1] as usize;
        } else {
            self.pos += 3;
        }
    }

    // if first param is zero, set pos to value of second param
    fn jump_if_false(&mut self) {
        let values = self.get_values(2);
        if values[0] == 0 {
            self.pos = values[1] as usize;
        } else {
            self.pos += 3;
        }
    }

    // if first param is < second param, store 1 in third param, else store 0
    fn lt(&mut self) {
        let values = self.get_values(2);
        let index = self.prog[(self.pos + 3) as usize] as usize;
        if values[0] < values[1] {
            self.prog[index] = 1;
        } else {
            self.prog[index] = 0;
        }
        self.pos += 4;
    }

    // if first param is == second param, store 1 in third param, else store 0
    fn eq(&mut self) {
        let values = self.get_values(2);
        let index = self.prog[(self.pos + 3) as usize] as usize;
        if values[0] == values[1] {
            self.prog[index] = 1;
        } else {
            self.prog[index] = 0;
        }
        self.pos += 4;
    }

    fn run(&mut self, mut input: Vec<i32>) -> Option<i32> {
        let mut op = self.prog[self.pos] % 100;
        let mut out = None;
        while op != 99 {
            match op {
                1 => self.add(),
                2 => self.mult(),
                3 => {
                    self.input(*input.get(0).expect("error - no input given!"));
                    input.remove(0);
                }
                4 => out = Some(self.output()),
                5 => self.jump_if_true(),
                6 => self.jump_if_false(),
                7 => self.lt(),
                8 => self.eq(),
                _ => panic!("Unsupported op code!")
            };
            if out.is_some() {
                return out;
            }
            op = self.prog[self.pos] % 100;
        }
        None
    }
}

impl Amp {
    fn run(&mut self, phase: Option<i32>, input_sig: i32) -> Option<i32> {
        let input = {
            if let Some(p) = phase {
                vec![p, input_sig]
            } else {
                vec![input_sig]
            }
        };
        self.prog.run(input)
    }
}

fn run_sequence(mut amps: Vec<Amp>, phases: &[i32]) -> i32 {
    let mut input = 0;
    for (i, amp) in amps.iter_mut().enumerate() {
        if let Some(output) = amp.run(Some(phases[i]), input) {
            input = output;
        }
    }
    input
}

fn run_sequence_feedback(mut amps: Vec<Amp>, phases: &[i32]) -> i32 {
    let mut input = 0;
    let mut first = true;
    loop {
        for (i, amp) in amps.iter_mut().enumerate() {
            let phase = if first {
                Some(phases[i])
            } else {
                None
            };

            if let Some(output) = amp.run(phase, input) {
                input = output;
            } else {
                return input;
            }
        }
        first = false;
    }
}

fn main() {
    let _input: Vec<&str> = INPUT.split(',').collect::<Vec<_>>()
        .iter().map(|&s| s.trim()).collect::<Vec<_>>();

    let input = _input.iter()
        .filter(|s| s.parse::<i32>().is_ok())
        .map(|&s| s.parse::<i32>().unwrap())
        .collect::<Vec<_>>();

    let mut amps = Vec::new();
    for _ in 0..5 {
        amps.push(Amp {
            prog: Intcode {
                prog: input.clone(),
                pos: 0
            },
        });
    }

    let mut largest = 0;
    for a in 0..5 {
        for b in 0..5 {
            for c in 0..5 {
                for d in 0..5 {
                    for e in 0..5 {
                        let seq = [a, b, c, d, e];
                        if (1..seq.len()).any(|i| seq[i..].contains(&seq[i - 1])) {
                            continue;
                        }
                        let out = run_sequence(amps.clone(), &[a, b, c, d, e]);
                        if out > largest {
                            largest = out;
                        }
                    }
                }
            }
        }
    }

    println!("Pt1: Largest amplifier chain output is {}", largest);

    // With feedback
    largest = 0;
    for a in 5..10 {
        for b in 5..10 {
            for c in 5..10 {
                for d in 5..10 {
                    for e in 5..10 {
                        let seq = [a, b, c, d, e];
                        if (1..seq.len()).any(|i| seq[i..].contains(&seq[i - 1])) {
                            continue;
                        }
                        let out = run_sequence_feedback(amps.clone(), &[a, b, c, d, e]);
                        if out > largest {
                            largest = out;
                        }
                    }
                }
            }
        }
    }

    println!("Pt2: Largest amplifier chain output (with feedback loop) is {}", largest);
}

const INPUT: &str = include_str!("input.txt");
const WIDTH: usize = 25;
const HEIGHT: usize = 6;

fn main() {
    let digs: Vec<char> = INPUT.chars().collect();
    let mut fewest_zeros: Option<(usize, &[char])> = None;
    for layer in digs.chunks_exact(WIDTH * HEIGHT) {
        let count = layer.iter().filter(|x| **x == '0').count();
        if let Some(f) = fewest_zeros {
            if count < f.0 {
                fewest_zeros = Some((count, layer));
            }
        } else {
            fewest_zeros = Some((count, layer));
        }
    }

    let layer = fewest_zeros.unwrap().1;

    let n_ones = layer.iter().filter(|x| **x == '1').count();
    let n_twos = layer.iter().filter(|x| **x == '2').count();

    println!("Number of ones in layer ({}) * number of twos in layer ({}) is {}",
             n_ones, n_twos, n_ones * n_twos);

    let mut img = [['.'; HEIGHT]; WIDTH];
    for layer in digs.chunks_exact(WIDTH * HEIGHT) {
        for (y, line) in layer.chunks_exact(WIDTH).enumerate() {
            for (x, c) in line.iter().enumerate() {
                if *c != '2' && img[x][y] == '.' {
                    if *c == '1' {
                        img[x][y] = 'X';
                    } else {
                        img[x][y] = ' ';
                    }
                }
            }
        }
    }

    for y in 0..HEIGHT {
        for i in img.iter().take(WIDTH) {
            print!("{}", i[y]);
        }
        println!();
    }
}

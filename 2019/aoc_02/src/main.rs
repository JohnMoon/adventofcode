const INPUT: &str = include_str!("input.txt");

struct Intcode {
    prog: Vec<usize>,
    pos: usize,
}

impl Intcode {
    fn run(&mut self) {
        let mut op = self.prog[self.pos];
        while op != 99 {
            let a = self.prog[self.prog[self.pos + 1]];
            let b = self.prog[self.prog[self.pos + 2]];

            let val = match op {
                1 => a + b,
                2 => a * b,
                _ => panic!("Unsupported op code!")
            };

            let index = self.prog[self.pos + 3];
            self.prog[index] = val;
            self.pos += 4;
            op = self.prog[self.pos];
        }
    }
}

fn main() {
    let _input: Vec<&str> = INPUT.split(',').collect();
    let input = _input.iter()
        .filter(|s| s.parse::<usize>().is_ok())
        .map(|&s| s.parse::<usize>().unwrap())
        .collect::<Vec<_>>();

    let mut pt1_prog = Intcode {
        prog: input.clone(),
        pos: 0
    };

    // Manually restore program state
    pt1_prog.prog[1] = 12;
    pt1_prog.prog[2] = 2;

    pt1_prog.run();
    println!("Part 1: position 0 after running program: {}", pt1_prog.prog[0]);

    let pt2_target = 19_690_720;
    let mut pt2_prog: Intcode;
    for a in {0..99} {
        for b in {0..99} {
            pt2_prog = Intcode {
                prog: input.clone(),
                pos: 0
            };
            pt2_prog.prog[1] = a;
            pt2_prog.prog[2] = b;
            pt2_prog.run();
            if pt2_prog.prog[0] == pt2_target {
                println!("\nPart 2: {} and {} produce {}", a, b, pt2_target);
                println!("100 * {} + {} = {}", a, b, 100 * a + b);
                return;
            }
        }
    }
}
